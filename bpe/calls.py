#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCallsToCall(self, addr):
    code = b"\xE8"
    if self.exe.cache.calls is not None:
        offsets = self.exe.cache.calls
    else:
        offsets = self.exe.codes(code, -1)
        self.exe.cache.calls = offsets
    if offsets is False:
        return False
    offset1 = len(code)
    offset2 = offset1 + 4
    data = []
    getAddr = self.getAddr
    rawVaDiff = self.exe.codeSection.rawVaDiff
    addr = addr + rawVaDiff  # vaToRaw almost
    for offset in offsets:
        tmpAddr = getAddr(offset, offset1, offset2)
        if addr == tmpAddr:
            data.append(offset)
    if len(data) == 0:
        return False
    return data

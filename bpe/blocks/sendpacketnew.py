#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A54h
            b"\x51"                      # 5  push ecx
            b"\x66\x89\x03"              # 6  mov [ebx], ax
            b"\x8D\x43\xAB"              # 9  lea eax, [ebx+4]
            b"\x6A\x04"                  # 12 push 4
            b"\x50"                      # 14 push eax
            b"\xFF\xD6"                  # 15 call esi
            b"\x0F\xBF\x43\xAB"          # 17 movsx eax, word ptr [ebx+2]
            b"\x50"                      # 21 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call new3
            b"\x0F\xBF\x4B\xAB"          # 27 movsx ecx, word ptr [ebx+2]
            b"\x6A\x08"                  # 31 push 8
            b"\x53"                      # 33 push ebx
            b"\x8B\xF8"                  # 34 mov edi, eax
            b"\x51"                      # 36 push ecx
            b"\x57"                      # 37 push edi
            b"\xFF\xD6"                  # 38 call esi
            b"\x8B\x45\xAB"              # 40 mov eax, [ebp+var_4]
            b"\xFF\x75\xAB"              # 43 push [ebp+var_8]
            b"\x8B\x48\xAB"              # 46 mov ecx, [eax+24h]
            b"\x03\x48\xAB"              # 49 add ecx, [eax+2Ch]
            b"\x0F\xBF\x43\xAB"          # 52 movsx eax, word ptr [ebx+2]
            b"\x51"                      # 56 push ecx
            b"\x50"                      # 57 push eax
            b"\x8D\x47\xAB"              # 58 lea eax, [edi+8]
            b"\x50"                      # 61 push eax
            b"\xFF\xD6"                  # 62 call esi
            b"\x0F\xBF\x43\xAB"          # 64 movsx eax, word ptr [ebx+2]
            b"\x83\xC4\x38"              # 68 add esp, 38h
            b"\x57"                      # 71 push edi
            b"\x50"                      # 72 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 0  mov [ebp+var_10], 96Eh
            b"\xFF\x75\xAB"              # 7  push [ebp+var_10]
            b"\x8D\x45\xAB"              # 10 lea eax, [ebp+var_C]
            b"\x50"                      # 13 push eax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+var_10]
            b"\x50"                      # 17 push eax
            b"\x6A\x00"                  # 18 push 0
            b"\x8D\x4D\xAB"              # 20 lea ecx, [ebp+buf]
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_51EB20
            b"\x8B\x45\xAB"              # 28 mov eax, [ebp+var_24]
            b"\xFF\x75\xAB"              # 31 push [ebp+var_10]
            b"\x2B\xC7"                  # 34 sub eax, edi
            b"\xD1\xF8"                  # 36 sar eax, 1
            b"\x8D\x4D\xAB"              # 38 lea ecx, [ebp+buf]
            b"\x8D\x04\xAB"              # 41 lea eax, [edi+eax*2]
            b"\x50"                      # 44 push eax
            b"\x57"                      # 45 push edi
            b"\xFF\x75\xAB"              # 46 push [ebp+var_18]
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call sub_51EB20
            b"\x8B\x75\xAB"              # 54 mov esi, [ebp+buf]
            b"\x8B\x4D\xAB"              # 57 mov ecx, [ebp+var_18]
            b"\x8B\xC1"                  # 60 mov eax, ecx
            b"\x2B\xCE"                  # 62 sub ecx, esi
            b"\x2B\xC6"                  # 64 sub eax, esi
            b"\x56"                      # 66 push esi
            b"\x51"                      # 67 push ecx
            b"\x66\x89\x46\xAB"          # 68 mov [esi+2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 77 mov ecx, eax
            b"\xE8"                      # 79 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 79,
            "packetId": 3,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9ACh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_210], a
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, g_session.m_account_id
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_20C], eax
            b"\x8D\x41\xAB"              # 23 lea eax, [ecx+8]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_20E], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 33 lea eax, [ebp+var_248]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 39 cmovnb eax, [ebp+var_248]
            b"\x51"                      # 46 push ecx
            b"\x50"                      # 47 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 48 lea eax, [ebp+var_208]
            b"\x50"                      # 54 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 55 call j_memcpy
            b"\x83\xC4\x0C"              # 60 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 63 lea eax, [ebp+var_210]
            b"\x50"                      # 69 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 70 movsx eax, [ebp+var_20E]
            b"\x50"                      # 77 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 78 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 83 mov ecx, eax
            b"\xE8"                      # 85 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 85,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 79,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A16h
            b"\x6A\xFF"                  # 5  push 0FFFFFFFFh
            b"\x0F\x57\xC0"              # 7  xorps xmm0, xmm0
            b"\x66\x89\x45\xAB"          # 10 mov word ptr [ebp+buf], ax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+var_1E]
            b"\x6A\x18"                  # 17 push 18h
            b"\x50"                      # 19 push eax
            b"\x66\x0F\xD6\x45\xAB"      # 20 movq qword ptr [ebp+var_1E], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 25 movq [ebp+var_16], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 30 movq [ebp+var_E], xmm0
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 35 call _snprintf_s
            b"\x83\xC4\x10"              # 41 add esp, 10h
            b"\x8D\x45\xAB"              # 44 lea eax, [ebp+buf]
            b"\x50"                      # 47 push eax
            b"\x6A\x1A"                  # 48 push 1Ah
            b"\xE8\xAB\xAB\xAB\xAB"      # 50 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 55 mov ecx, eax
            b"\xE8"                      # 57 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 57,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 51,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9FBh
            b"\x8D\x3C\x4D\xAB\x00\x00\x00"  # 5  lea edi, ds:8[ecx*2]
            b"\x6A\x00"                  # 12 push 0
            b"\x50"                      # 14 push eax
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_214], d
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_20C], ecx
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 28 mov word ptr [ebp+var_214+2],
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call j_memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 40 mov eax, [ebp+var_214]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 46 mov [ebp-114h], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 52 mov eax, [ebp+var_20C]
            b"\x03\xC0"                  # 58 add eax, eax
            b"\x50"                      # 60 push eax
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 61 mov [ebp+var_110], esi
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 67 mov esi, [ebp+var_204]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 73 lea eax, [ebp+var_10C]
            b"\x56"                      # 79 push esi
            b"\x50"                      # 80 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 81 call j_memcpy
            b"\x83\xC4\x18"              # 86 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 89 lea eax, [ebp+buf]
            b"\x50"                      # 95 push eax
            b"\x0F\xBF\xC7"              # 96 movsx eax, di
            b"\x50"                      # 99 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 105 mov ecx, eax
            b"\xE8"                      # 107 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A77h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+var_24], ax
            b"\x8D\x45\xAB"              # 9  lea eax, [ebp+var_24]
            b"\x0F\x57\xC0"              # 12 xorps xmm0, xmm0
            b"\x50"                      # 15 push eax
            b"\x66\x0F\xD6\x45\xAB"      # 16 movq qword ptr [ebp+var_24+2], xm
            b"\x6A\x0F"                  # 21 push 0Fh
            b"\xC7\x45\xAB\x00\x00\x00\x00"  # 23 mov dword ptr [ebp+var_1C+2],
            b"\xC6\x45\xAB\x00"          # 30 mov [ebp+var_1C+6], 0
            b"\xC6\x45\xAB\x01"          # 34 mov [ebp+var_24+2], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 43 mov ecx, eax
            b"\xE8"                      # 45 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 45,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 39,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A77h
            b"\x66\x89\x4D\xAB"          # 5  mov word ptr [ebp+var_24], cx
            b"\xC6\x45\xAB\x00"          # 9  mov [ebp+var_24+2], 0
            b"\xF3\x0F\x10\x40\xAB"      # 13 movss xmm0, dword ptr [eax+34h]
            b"\xF3\x0F\x11\x45\xAB"      # 18 movss dword ptr [ebp+var_24+3], x
            b"\xF3\x0F\x10\x40\xAB"      # 23 movss xmm0, dword ptr [eax+30h]
            b"\xF3\x0F\x11\x45\xAB"      # 28 movss dword ptr [ebp+var_24+7], x
            b"\xF3\x0F\x10\x40\xAB"      # 33 movss xmm0, dword ptr [eax+2Ch]
            b"\x6A\x00"                  # 38 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 40 push offset aRange
            b"\x8B\xCF"                  # 45 mov ecx, edi
            b"\xF3\x0F\x11\x45\xAB"      # 47 movss dword ptr [ebp+var_1C+3], x
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call sub_451CF0
            b"\x83\xF8\xFF"              # 57 cmp eax, 0FFFFFFFFh
            b"\x74\xAB"                  # 60 jz short loc_A06D87
            b"\x83\xC7\xAB"              # 62 add edi, 18h
            b"\x83\x7F\xAB\xAB"          # 65 cmp dword ptr [edi+14h], 10h
            b"\x72\x02"                  # 69 jb short loc_A06D63
            b"\x8B\x3F"                  # 71 mov edi, [edi]
            b"\x57"                      # 73 push edi
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 74 call atof
            b"\xD9\x5D\xAB"              # 80 fstp dword ptr [ebp+var_24+3]
            b"\x83\xC4\x04"              # 83 add esp, 4
            b"\x8D\x45\xAB"              # 86 lea eax, [ebp+var_24]
            b"\x50"                      # 89 push eax
            b"\x6A\x0F"                  # 90 push 0Fh
            b"\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 97 mov ecx, eax
            b"\xE8"                      # 99 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 99,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 93,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1D5h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x8D\x46\xAB"              # 8  lea eax, [esi+8]
            b"\x56"                      # 11 push esi
            b"\x66\x89\x47\x02"          # 12 mov [edi+2], ax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 16 lea ecx, [ebp+var_170]
            b"\x51"                      # 22 push ecx
            b"\x83\xC0\xAB"              # 23 add eax, 0FFFFFFF8h
            b"\x50"                      # 26 push eax
            b"\x8D\x47\xAB"              # 27 lea eax, [edi+8]
            b"\x50"                      # 30 push eax
            b"\x89\x5F\xAB"              # 31 mov [edi+4], ebx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 34 call memcpy_s
            b"\x0F\xB7\x47\xAB"          # 40 movzx eax, word ptr [edi+2]
            b"\x83\xC4\xAB"              # 44 add esp, 10h
            b"\x57"                      # 47 push edi
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0C9h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF74],
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_DF74+2]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 19 mov ecx, [ebp+var_DF74]
            b"\x33\xF6"                  # 25 xor esi, esi
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+buf], ecx
            b"\x85\xC0"                  # 33 test eax, eax
            b"\x7E\xAB"                  # 35 jle short loc_A1498B
            b"\x8D\x9D\xAB\xAB\xAB\xAB"  # 37 lea ebx, [ebp+var_AF0C]
            b"\x8B\xF8"                  # 43 mov edi, eax
            b"\x8D\x49\xAB"              # 45 lea ecx, [ecx+0]
            b"\x56"                      # 48 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 49 lea eax, [ebp+var_D440]
            b"\x50"                      # 55 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 56 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 61 call sub_AD2FA0
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 66 mov [ebp+var_4], 5
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 73 movzx eax, [ebp+var_D430]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 80 mov word ptr [ebp+var_DF74+2]
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 87 movzx eax, [ebp+var_D438]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 94 mov word ptr [ebp+var_DF74],
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 101 mov eax, [ebp+var_DF74]
            b"\x89\x03"                  # 107 mov [ebx], eax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 109 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 116 lea ecx, [ebp+var_D440]
            b"\xE8\xAB\xAB\xAB\xAB"      # 122 call sub_45A240
            b"\x46"                      # 127 inc esi
            b"\x8D\x5B\xAB"              # 128 lea ebx, [ebx+4]
            b"\x3B\xF7"                  # 131 cmp esi, edi
            b"\x7C\xAB"                  # 133 jl short loc_A14920
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 135 mov eax, [ebp+var_DF6C]
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 141 mov edi, [ebp+var_DF64]
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 147 mov edx, [ebp+var_BD7C]
            b"\x85\xC0"                  # 153 test eax, eax
            b"\x74\xAB"                  # 155 jz short loc_A149AD
            b"\x83\xBF\xAB\xAB\xAB\x00\x00"  # 157 cmp dword ptr [edi+260h], 0
            b"\x75\xAB"                  # 164 jnz short loc_A149AD
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 166 lea eax, [ebp+buf]
            b"\x50"                      # 172 push eax
            b"\x0F\xBF\xC2"              # 173 movsx eax, dx
            b"\x50"                      # 176 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 177 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 182 mov ecx, eax
            b"\xE8"                      # 184 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 184,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 178,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0C8h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF74],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_DF74+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 19 mov eax, [ebp+var_DF74]
            b"\x33\xDB"                  # 25 xor ebx, ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+var_B710], eax
            b"\x85\xD2"                  # 33 test edx, edx
            b"\x0F\x8E\xAB\xAB\x00\x00"  # 35 jle loc_A14BEC
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 41 lea esi, [ebp+var_B70C]
            b"\x8B\xFA"                  # 47 mov edi, edx
            b"\x8D\x64\x24\xAB"          # 49 lea esp, [esp+0]
            b"\x53"                      # 53 push ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 54 lea eax, [ebp+var_D150]
            b"\x50"                      # 60 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 61 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call sub_AD2BE0
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 71 mov [ebp+var_4], 7
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 78 movzx eax, [ebp+var_D140]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 85 cmp [ebp+var_D110], 10h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 92 mov word ptr [ebp+var_DF7C+4]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 99 lea eax, [ebp+var_D124]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 105 cmovnb eax, [ebp+var_D124]
            b"\x50"                      # 112 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 113 call atoi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 119 mov [ebp+var_DF7C+6], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 125 mov eax, [ebp+var_DF7C+4]
            b"\x89\x06"                  # 131 mov [esi], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 133 movzx eax, word ptr [ebp+var
            b"\x83\xC4\x04"              # 140 add esp, 4
            b"\x66\x89\x46\xAB"          # 143 mov [esi+4], ax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 147 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 154 lea ecx, [ebp+var_D150]
            b"\xE8\xAB\xAB\xAB\xAB"      # 160 call sub_45A240
            b"\x43"                      # 165 inc ebx
            b"\x8D\x76\xAB"              # 166 lea esi, [esi+6]
            b"\x3B\xDF"                  # 169 cmp ebx, edi
            b"\x7C\xAB"                  # 171 jl short loc_A14B60
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 173 mov edx, [ebp+var_DF6C]
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 179 mov edi, [ebp+var_DF64]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 185 mov ecx, [ebp+var_BD7C]
            b"\x85\xD2"                  # 191 test edx, edx
            b"\x74\xAB"                  # 193 jz short loc_A14C0E
            b"\x83\xBF\xAB\xAB\xAB\x00\x00"  # 195 cmp dword ptr [edi+260h], 0
            b"\x75\xAB"                  # 202 jnz short loc_A14C0E
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 204 lea eax, [ebp+var_B710]
            b"\x50"                      # 210 push eax
            b"\x0F\xBF\xC1"              # 211 movsx eax, cx
            b"\x50"                      # 214 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 215 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 220 mov ecx, eax
            b"\xE8"                      # 222 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 222,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 216,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9D4h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_E018], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_E018]
            b"\x50"                      # 18 push eax
            b"\x6A\xAB"                  # 19 push 2
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0AAAh
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF74],
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_DF74+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 19 mov eax, [ebp+var_DF74]
            b"\x33\xDB"                  # 25 xor ebx, ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+var_6F10], eax
            b"\x85\xD2"                  # 33 test edx, edx
            b"\x0F\x8E\xAB\xAB\x00\x00"  # 35 jle loc_A14F90
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 41 lea esi, [ebp+var_6F0C]
            b"\x8B\xFA"                  # 47 mov edi, edx
            b"\x8D\x9B\xAB\xAB\x00\x00"  # 49 lea ebx, [ebx+0]
            b"\x53"                      # 55 push ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 56 lea eax, [ebp+var_CCF0]
            b"\x50"                      # 62 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 63 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_AD2BE0
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 73 mov [ebp+var_4], 8
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 80 mov eax, [ebp+var_CCE0]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 86 cmp [ebp+var_CCB0], 10h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 93 mov [ebp+var_BD7C], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 99 lea eax, [ebp+var_CCC4]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 105 cmovnb eax, [ebp+var_CCC4]
            b"\x50"                      # 112 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 113 call atoi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 119 mov dword ptr [ebp+var_BD84+4],
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 125 movq xmm0, [ebp+var_BD84
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 133 mov ax, [ebp+var_CCE8]
            b"\x66\x0F\xD6\x06"          # 140 movq qword ptr [esi], xmm0
            b"\x83\xC4\x04"              # 144 add esp, 4
            b"\x66\x89\x46\xAB"          # 147 mov [esi+8], ax
            b"\xC7\x45\xFC\xFF\xFF\xFF\xFF"  # 151 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 158 lea ecx, [ebp+var_CCF0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 164 call sub_45A240
            b"\x43"                      # 169 inc ebx
            b"\x8D\x76\xAB"              # 170 lea esi, [esi+0Ah]
            b"\x3B\xDF"                  # 173 cmp ebx, edi
            b"\x7C\xAB"                  # 175 jl short loc_A14EC0
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 177 mov edi, [ebp+var_DF64]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 183 cmp dword ptr [edi+260h], 0
            b"\x75\xAB"                  # 190 jnz short loc_A14F64
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 192 lea eax, [ebp+var_6F10]
            b"\x50"                      # 198 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 199 mov eax, [ebp+var_DF6C]
            b"\x98"                      # 205 cwd cwde
            b"\x50"                      # 206 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 207 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 212 mov ecx, eax
            b"\xE8"                      # 214 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 214,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 208,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9D6h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF74],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_DF74+2]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 19 mov ecx, [ebp+var_DF74]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_6310], ecx
            b"\x85\xC0"                  # 31 test eax, eax
            b"\x0F\x8E\xAB\xAB\x00\x00"  # 33 jle loc_A15105
            b"\x8D\x9D\xAB\xAB\xAB\xAB"  # 39 lea ebx, [ebp+var_630C]
            b"\x33\xFF"                  # 45 xor edi, edi
            b"\x90"                      # 47 no nop
            b"\x57"                      # 48 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 49 lea eax, [ebp+var_CDD0]
            b"\x50"                      # 55 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 56 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 61 call sub_AD2BE0
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 66 mov [ebp+var_4], 9
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 73 cmp [ebp+var_CD90], 10h
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 80 mov esi, [ebp+var_CDC0]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 86 lea eax, [ebp+var_CDA4]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 92 cmovnb eax, [ebp+var_CDA4]
            b"\x50"                      # 99 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 100 call atoi
            b"\x89\x03"                  # 106 mov [ebx], eax
            b"\x83\xC4\x04"              # 108 add esp, 4
            b"\x89\x73\xAB"              # 111 mov [ebx+4], esi
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 114 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 121 lea ecx, [ebp+var_CDD0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 127 call sub_45A240
            b"\x47"                      # 132 inc edi
            b"\x8D\x5B\xAB"              # 133 lea ebx, [ebx+8]
            b"\x3B\xBD\xAB\xAB\xAB\xAB"  # 136 cmp edi, [ebp+var_BD7C]
            b"\x7C\xAB"                  # 142 jl short loc_A15050
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 144 mov edi, [ebp+var_DF64]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 150 cmp dword ptr [edi+260h], 0
            b"\x75\xAB"                  # 157 jnz short loc_A150DA
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 159 lea eax, [ebp+var_6310]
            b"\x50"                      # 165 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 166 mov eax, [ebp+var_DF6C]
            b"\x98"                      # 172 cwd cwde
            b"\x50"                      # 173 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 179 mov ecx, eax
            b"\xE8"                      # 181 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 19Ch
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF64],
            b"\x8D\x77\xAB"              # 12 lea esi, [edi+4]
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_DF64+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 22 mov eax, [ebp+var_DF64]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 28 mov dword ptr [ebp+var_5710], eax
            b"\x57"                      # 34 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 35 lea eax, [ebp+var_E54]
            b"\x50"                      # 41 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 42 lea eax, [ebp+var_570C]
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call j_memcpy
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_5710]
            b"\x50"                      # 63 push eax
            b"\x0F\xB7\xC6"              # 64 movzx eax, si
            b"\x50"                      # 67 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 73 mov ecx, eax
            b"\xE8"                      # 75 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 75,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 69,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2DBh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF64],
            b"\x8D\x77\xAB"              # 12 lea esi, [edi+5]
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_DF64+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 22 mov eax, [ebp+var_DF64]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 28 mov dword ptr [ebp+var_6710], eax
            b"\x8D\x47\xAB"              # 34 lea eax, [edi+1]
            b"\x50"                      # 37 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_190C]
            b"\x50"                      # 44 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 45 lea eax, [ebp+var_670C]
            b"\x50"                      # 51 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call j_memcpy
            b"\x83\xC4\x0C"              # 57 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 60 lea eax, [ebp+var_6710]
            b"\x50"                      # 66 push eax
            b"\x0F\xB7\xC6"              # 67 movzx eax, si
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0F3h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 8  lea eax, [ebp+var_171C]
            b"\x56"                      # 14 push esi
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 28 call memcpy_s
            b"\x83\xC4\x10"              # 34 add esp, 10h
            b"\x0F\xB7\x47\xAB"          # 37 movzx eax, word ptr [edi+2]
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0D5h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_BD84],
            b"\x0F\xB7\x43\xAB"          # 12 movzx eax, word ptr [ebx+10h]
            b"\x66\x83\xC0\xAB"          # 16 add ax, 0Fh
            b"\x0F\x57\xC0"              # 20 xorps xmm0, xmm0
            b"\x8B\xCE"                  # 23 mov ecx, esi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var_BD84+2]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 32 movq [ebp+var_BD84+7], xm
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call sub_44B040
            b"\x50"                      # 45 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 46 lea eax, [ebp+var_BD84+7]
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call j_memcpy
            b"\x0F\xB7\x43\xAB"          # 58 movzx eax, word ptr [ebx+38h]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 62 mov word ptr [ebp+var_BD84+4]
            b"\x8A\x43\xAB"              # 69 mov al, [ebx+30h]
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 72 mov byte ptr [ebp+var_BD84+6], al
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 78 mov eax, [ebp+var_BD7C]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 84 movq xmm0, [ebp+var_BD84]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 92 mov [ebp+var_4308], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 98 movzx eax, [ebp+var_BD78]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 105 mov [ebp+var_4304], ax
            b"\x8A\x85\xAB\xAB\xAB\xAB"  # 112 mov al, [ebp+var_BD76]
            b"\x83\xC4\x0C"              # 118 add esp, 0Ch
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 121 movq qword ptr [ebp+var_
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 129 mov [ebp+var_4302], al
            b"\xFF\x73\xAB"              # 135 push dword ptr [ebx+10h]
            b"\x8B\xCB"                  # 138 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 140 call sub_44B040
            b"\x50"                      # 145 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 146 lea eax, [ebp+var_4301]
            b"\x50"                      # 152 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 153 call j_memcpy
            b"\x83\xC4\x0C"              # 158 add esp, 0Ch
            b"\x8B\xCF"                  # 161 mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 163 call sub_A099F0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 168 lea eax, [ebp+var_4310]
            b"\x50"                      # 174 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 175 movsx eax, word ptr [ebp+var
            b"\x50"                      # 182 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 183 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 188 mov ecx, eax
            b"\xE8"                      # 190 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 190,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 184,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1B2h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_3F10],
            b"\x8D\x0C\xAB\xAB\xAB\x00\x00"  # 12 lea ecx, ds:55h[eax*8]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_BD7C], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_3F0E], cx
            b"\x75\xAB"                  # 32 jnz short loc_A19974
            b"\x6A\xAB"                  # 34 push 4Fh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp+var_3F0C]
            b"\x53"                      # 42 push ebx
            b"\x50"                      # 43 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 44 call strncpy
            b"\x83\xC4\x0C"              # 50 add esp, 0Ch
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 53 mov [ebp+var_3EBD], 1
            b"\xEB\xAB"                  # 62 jmp short loc_A19982
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_3F0C], 0
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 71 mov byte ptr [ebp+var_3EBD+1]
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 78 cmp dword_10B2A14, 0
            b"\x75\xAB"                  # 85 jnz short loc_A1999C
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 87 lea eax, [ebp+var_3F0C]
            b"\x50"                      # 93 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 94 mov ecx, (offset g_session+4E18h)
            b"\xE8\xAB\xAB\xAB\xAB"      # 99 call sub_4513B0
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 104 mov ebx, [ebp+var_BD7C]
            b"\x33\xFF"                  # 110 xor edi, edi
            b"\x85\xDB"                  # 112 test ebx, ebx
            b"\x7E\xAB"                  # 114 jle short loc_A19A06
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 116 lea esi, [ebp+var_3EB7]
            b"\x8B\xFF"                  # 122 mov edi, edi
            b"\x57"                      # 124 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 125 lea eax, [ebp+var_D360]
            b"\x50"                      # 131 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 132 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 137 call sub_AD0B50
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 142 mov [ebp+var_4], 36h
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 149 mov eax, [ebp+var_D358]
            b"\x85\xC0"                  # 155 test eax, eax
            b"\x74\xAB"                  # 157 jz short loc_A199ED
            b"\x66\x89\x46\xAB"          # 159 mov [esi-4], ax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 163 mov ax, [ebp+var_D350]
            b"\x66\x89\x46\xAB"          # 170 mov [esi-2], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 174 mov eax, [ebp+var_D348]
            b"\x89\x06"                  # 180 mov [esi], eax
            b"\x83\xC6\xAB"              # 182 add esi, 8
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 185 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 192 lea ecx, [ebp+var_D360]
            b"\xE8\xAB\xAB\xAB\xAB"      # 198 call sub_45A240
            b"\x47"                      # 203 inc edi
            b"\x3B\xFB"                  # 204 cmp edi, ebx
            b"\x7C\xAB"                  # 206 jl short loc_A199B0
            b"\x85\xDB"                  # 208 test ebx, ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 210 lea eax, [ebp+var_3F10]
            b"\x50"                      # 216 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 217 movsx eax, [ebp+var_3F0E]
            b"\x50"                      # 224 push eax
            b"\x75\xAB"                  # 225 jnz short loc_A19A1E
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 227 mov byte ptr [ebp+var_3EBD+1
            b"\xE8\xAB\xAB\xAB\xAB"      # 234 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 239 mov ecx, eax
            b"\xE8"                      # 241 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 241,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 235,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 801h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF7C],
            b"\x8D\x0C\xAB\xAB\xAB\x00\x00"  # 12 lea ecx, ds:0Ch[eax*4]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_BD7C], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var_DF7C+2]
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_DF7C+4], ebx
            b"\x81\xF9\xAB\xAB\xAB\x00"  # 38 cmp ecx, 800h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 44 ja loc_A16084
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 50 mov ebx, [ebp+var_BD7C]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 56 movq xmm0, qword ptr [ebp
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 64 mov eax, [ebp+var_DF6C]
            b"\x33\xF6"                  # 70 xor esi, esi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 72 movq qword ptr [ebp+var_8
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 80 mov [ebp+var_8708], eax
            b"\x85\xDB"                  # 86 test ebx, ebx
            b"\x7E\xAB"                  # 88 jle short loc_A19B13
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 90 lea edi, [ebp+var_8704]
            b"\x90"                      # 96 no nop
            b"\x56"                      # 97 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 98 lea eax, [ebp+var_D520]
            b"\x50"                      # 104 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 105 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 110 call sub_AD16D0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 115 mov [ebp+var_4], 37h
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 122 mov ecx, [ebp+var_D518]
            b"\x85\xC9"                  # 128 test ecx, ecx
            b"\x74\xAB"                  # 130 jz short loc_A19AF0
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 132 mov ax, [ebp+var_D510]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 139 mov word ptr [ebp+var_DF64],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 146 mov word ptr [ebp+var_DF64+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 153 mov eax, [ebp+var_DF64]
            b"\x89\x07"                  # 159 mov [edi], eax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 161 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 168 lea ecx, [ebp+var_D520]
            b"\xE8\xAB\xAB\xAB\xAB"      # 174 call sub_45A240
            b"\x46"                      # 179 inc esi
            b"\x83\xC7\x04"              # 180 add edi, 4
            b"\x3B\xF3"                  # 183 cmp esi, ebx
            b"\x7C\xAB"                  # 185 jl short loc_A19AB0
            b"\x8D\x0C\xAB\xAB\xAB\xAB\xAB"  # 187 lea ecx, ds:0Ch[ebx*4]
            b"\x85\xDB"                  # 194 test ebx, ebx
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 196 jz loc_A16084
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 202 lea eax, [ebp+var_8710]
            b"\x50"                      # 208 push eax
            b"\x0F\xBF\xC1"              # 209 movsx eax, cx
            b"\x50"                      # 212 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 213 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 218 mov ecx, eax
            b"\xE8"                      # 220 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 220,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 214,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 153h
            b"\x8D\x71\xAB"              # 5  lea esi, [ecx+4]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 8  mov word ptr [ebp+var_DF64],
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_DF64+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 22 mov eax, [ebp+var_DF64]
            b"\x51"                      # 28 push ecx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 29 mov dword ptr [ebp+var_9710], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 35 lea eax, [ebp+var_970C]
            b"\x53"                      # 41 push ebx
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call j_memcpy
            b"\x83\xC4\x0C"              # 48 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp+var_9710]
            b"\x50"                      # 57 push eax
            b"\x0F\xBF\xC6"              # 58 movsx eax, si
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 155h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF80],
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_108F960
            b"\x8D\x04\xAB"              # 17 lea eax, [eax+eax*2]
            b"\x8D\x1C\xAB\xAB\xAB\xAB\x00"  # 20 lea ebx, ds:4[eax*4]
            b"\xB8\xAB\xAB\xAB\x00"      # 27 mov eax, 800h
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov word ptr [ebp+var_DF80+2]
            b"\x66\x3B\xC3"              # 39 cmp ax, bx
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 42 jl loc_A16084
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 48 mov eax, [ebp+var_DF80]
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 54 mov edi, _dword_108F95C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov dword ptr [ebp+var_A710], eax
            b"\x8B\x07"                  # 66 mov eax, [edi]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 68 mov [ebp+var_DF64], eax
            b"\x3B\xC7"                  # 74 cmp eax, edi
            b"\x74\xAB"                  # 76 jz short loc_A19D6C
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 78 lea esi, [ebp+var_A70C]
            b"\xEB\x06"                  # 84 jmp short loc_A19D30
            b"\xAB\xAB\xAB\xAB\xAB\xAB"  # 86 align 10h
            b"\x8B\x48\xAB"              # 92 mov ecx, [eax+14h]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 95 mov dword ptr [ebp+var_BD84], ecx
            b"\x8B\x48\xAB"              # 101 mov ecx, [eax+18h]
            b"\x8B\x40\xAB"              # 104 mov eax, [eax+1Ch]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 107 mov dword ptr [ebp+var_BD84+4],
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 113 movq xmm0, [ebp+var_BD84
            b"\x66\x0F\xD6\x06"          # 121 movq qword ptr [esi], xmm0
            b"\x89\x46\xAB"              # 125 mov [esi+8], eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 128 lea ecx, [ebp+var_DF64]
            b"\x8D\x76\x0C"              # 134 lea esi, [esi+0Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 137 call sub_690080
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 142 mov eax, [ebp+var_DF64]
            b"\x3B\xC7"                  # 148 cmp eax, edi
            b"\x75\xAB"                  # 150 jnz short loc_A19D30
            b"\xB9\xAB\xAB\xAB\xAB"      # 152 mov ecx, offset _dword_108F95C
            b"\xE8\xAB\xAB\xAB\xAB"      # 157 call sub_69F9B0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 162 lea eax, [ebp+var_A710]
            b"\x50"                      # 168 push eax
            b"\x0F\xBF\xC3"              # 169 movsx eax, bx
            b"\x50"                      # 172 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 173 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 178 mov ecx, eax
            b"\xE8"                      # 180 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 180,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 174,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov dword ptr [eb
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 17,
            "packetId": (6, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9CEh
            b"\x6A\xFF"                  # 5  push 0FFFFFFFFh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var_CA00],
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_C9FE]
            b"\x6A\xAB"                  # 20 push 64h
            b"\x50"                      # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call _snprintf_s
            b"\x83\xC4\x20"              # 29 add esp, 20h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 32 lea eax, [ebp+var_CA00]
            b"\x50"                      # 38 push eax
            b"\x6A\xAB"                  # 39 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 98Dh
            b"\x68\xAB\xAB\x00\x00"      # 5  push 3FFh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 10 mov word ptr [ebp+var_DF64],
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 17 lea eax, [ebp+var_5310+1]
            b"\x6A\xAB"                  # 23 push 0
            b"\x8D\x77\xAB"              # 25 lea esi, [edi+5]
            b"\x50"                      # 28 push eax
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 29 mov word ptr [ebp+var_DF64+2]
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call j_memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 41 mov eax, [ebp+var_DF64]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 47 mov dword ptr [ebp+var_5310], eax
            b"\x8D\x47\xAB"              # 53 lea eax, [edi+1]
            b"\x50"                      # 56 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_1814]
            b"\x50"                      # 63 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 64 lea eax, [ebp+var_530C]
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call j_memcpy
            b"\x83\xC4\x18"              # 76 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 79 lea eax, [ebp+var_5310]
            b"\x50"                      # 85 push eax
            b"\x0F\xBF\xC6"              # 86 movsx eax, si
            b"\x50"                      # 89 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 90 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 95 mov ecx, eax
            b"\xE8"                      # 97 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 97,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 91,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 886h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\x8D\x45\xAB"              # 9  lea eax, [ebp+buf]
            b"\x50"                      # 12 push eax
            b"\x6A\xAB"                  # 13 push 2
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9ABh
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 9  mov eax, g_session.m_account_id
            b"\x89\x45\xAB"              # 14 mov [ebp+var_6], eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+buf]
            b"\x50"                      # 20 push eax
            b"\x6A\xAB"                  # 21 push 6
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xBB\xAB\xAB\x00\x00"      # 0  mov ebx, 9A1h
            b"\x8D\x45\xAB"              # 5  lea eax, [ebp+buf]
            b"\x50"                      # 8  push eax
            b"\x6A\xAB"                  # 9  push 2
            b"\x66\x89\x5D\xAB"          # 11 mov word ptr [ebp+buf], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0ACFh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+buf], ax
            b"\x8D\x83\xAB\xAB\xAB\x00"  # 12 lea eax, [ebx+178h]
            b"\x50"                      # 18 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 19 push offset aS
            b"\x6A\xFF"                  # 24 push 0FFFFFFFFh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 26 lea eax, [ebp+var_6F2]
            b"\x6A\xAB"                  # 32 push 19h
            b"\x50"                      # 34 push eax
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_6F6]
            b"\xFF\xD6"                  # 45 call esi
            b"\x8D\x83\xAB\xAB\xAB\x00"  # 47 lea eax, [ebx+138h]
            b"\x50"                      # 53 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 54 push offset aS
            b"\x0F\x57\xC0"              # 59 xorps xmm0, xmm0
            b"\x6A\xFF"                  # 62 push 0FFFFFFFFh
            b"\x8D\x45\xAB"              # 64 lea eax, [ebp+var_34]
            b"\x6A\xAB"                  # 67 push 21h
            b"\x50"                      # 69 push eax
            b"\xC6\x45\xAB\x00"          # 70 mov [ebp+var_34], 0
            b"\x66\x0F\xD6\x45\xAB"      # 74 movq [ebp+var_33], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 79 movq [ebp+var_2B], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 84 movq [ebp+var_23], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 89 movq [ebp+var_1B], xmm0
            b"\xFF\xD6"                  # 94 call esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 96 lea eax, [ebp+var_6D9]
            b"\x50"                      # 102 push eax
            b"\x8D\x45\xAB"              # 103 lea eax, [ebp+var_34]
            b"\x50"                      # 106 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 107 call sub_46FA10
            b"\x8D\x83\xAB\xAB\xAB\x00"  # 112 lea eax, [ebx+1D0h]
            b"\x50"                      # 118 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 119 push offset aS
            b"\x6A\xFF"                  # 124 push 0FFFFFFFFh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 126 lea eax, [ebp+var_6B9]
            b"\x6A\xAB"                  # 132 push 5
            b"\x50"                      # 134 push eax
            b"\xFF\xD6"                  # 135 call esi
            b"\x83\xC4\x50"              # 137 add esp, 50h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 140 lea eax, [ebp+buf]
            b"\x50"                      # 146 push eax
            b"\x6A\xAB"                  # 147 push 44h
            b"\xE8\xAB\xAB\xAB\xAB"      # 149 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 154 mov ecx, eax
            b"\xE8"                      # 156 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 156,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 150,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9A3h
            b"\x66\x89\x06"              # 5  mov [esi], ax
            b"\x8B\x83\xAB\xAB\xAB\xAB"  # 8  mov eax, [ebx+69D8h]
            b"\x89\x46\xAB"              # 14 mov [esi+4], eax
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 17 lea eax, [ebx+1B8h]
            b"\x50"                      # 23 push eax
            b"\x6A\xFF"                  # 24 push 0FFFFFFFFh
            b"\x8D\x46\xAB"              # 26 lea eax, [esi+8]
            b"\x6A\xAB"                  # 29 push 0Ah
            b"\x50"                      # 31 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 32 call _snprintf_s
            b"\x83\xC4\x10"              # 38 add esp, 10h
            b"\xB8\xAB\xAB\xAB\x00"      # 41 mov eax, 0Eh
            b"\x56"                      # 46 push esi
            b"\x50"                      # 47 push eax
            b"\x66\x89\x46\xAB"          # 48 mov [esi+2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 57 mov ecx, eax
            b"\xE8"                      # 59 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2018-11-28
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\x00\x00"  # 0  mov dword ptr [eb
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 10 mov [ebp+packet+2], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 17 lea eax, [ebp+packet]
            b"\x50"                      # 23 push eax
            b"\x6A\x04"                  # 24 push 4
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 33,
            "packetId": 6,
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2017-11-01
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9FBh
            b"\x8D\x3C\xAB\xAB\xAB\xAB\x00"  # 5  lea edi, ds:6[ecx*2]
            b"\x6A\xAB"                  # 12 push 0
            b"\x50"                      # 14 push eax
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 15 mov [ebp+packet.packet_id], d
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov dword ptr [ebp+packet.next.am
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 28 mov [ebp+packet.packet_len],
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call j_memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 40 mov eax, dword ptr [ebp+packet.pa
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 46 mov [ebp-114h], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 52 mov eax, dword ptr [ebp+packet.ne
            b"\x03\xC0"                  # 58 add eax, eax
            b"\x50"                      # 60 push eax
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 61 mov [ebp+var_110], si
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 68 mov esi, [ebp+src]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 74 lea eax, [ebp+dst]
            b"\x56"                      # 80 push esi
            b"\x50"                      # 81 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 82 call j_memcpy
            b"\x83\xC4\x18"              # 87 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 90 lea eax, [ebp+var_114]
            b"\x50"                      # 96 push eax
            b"\x0F\xBF\xC7"              # 97 movsx eax, di
            b"\x50"                      # 100 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 101 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 106 mov ecx, eax
            b"\xE8"                      # 108 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 108,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 102,
        }
    ],
    # 2017-11-01
    [
        (
            b"\xBB\xAB\xAB\x00\x00"      # 0  mov ebx, 9A1h
            b"\xEB\x03"                  # 5  jmp short loc_A50B70
            b"\xAB\xAB\xAB"              # 7  align 10h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 10 lea eax, [ebp+buf]
            b"\x50"                      # 16 push eax
            b"\x6A\xAB"                  # 17 push 2
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 19 mov [ebp+buf], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 33,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 187h
            b"\x6A\xAB"                  # 5  push 6
            b"\x66\x89\x4D\xAB"          # 7  mov word ptr [ebp+buf], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xC7\x45\xF0\xAB\xAB\x00\x00"  # 0  mov [ebp+Src], 96Eh
            b"\x8B\x4D\xAB"              # 7  mov ecx, [ebp+Src]
            b"\x51"                      # 10 push ecx
            b"\x8D\x55\xAB"              # 11 lea edx, [ebp+var_C]
            b"\x52"                      # 14 push edx
            b"\x8D\x4D\xAB"              # 15 lea ecx, [ebp+Src]
            b"\x51"                      # 18 push ecx
            b"\x33\xC0"                  # 19 xor eax, eax
            b"\x50"                      # 21 push eax
            b"\x8D\x4D\xAB"              # 22 lea ecx, [ebp+buf]
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_45F030
            b"\x8B\x7D\xAB"              # 30 mov edi, [ebp+var_20]
            b"\x8B\x55\xAB"              # 33 mov edx, [ebp+var_1C]
            b"\x8B\x4D\xAB"              # 36 mov ecx, [ebp+Src]
            b"\x2B\xD7"                  # 39 sub edx, edi
            b"\xD1\xFA"                  # 41 sar edx, 1
            b"\x51"                      # 43 push ecx
            b"\x8D\x04\xAB"              # 44 lea eax, [edi+edx*2]
            b"\x8B\x55\xAB"              # 47 mov edx, [ebp+var_2C]
            b"\x50"                      # 50 push eax
            b"\x57"                      # 51 push edi
            b"\x52"                      # 52 push edx
            b"\x8D\x4D\xAB"              # 53 lea ecx, [ebp+buf]
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call sub_45F030
            b"\x8B\x75\xAB"              # 61 mov esi, [ebp+buf]
            b"\x8B\x45\xAB"              # 64 mov eax, [ebp+var_2C]
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\x2B\xC6"                  # 69 sub eax, esi
            b"\x2B\xCE"                  # 71 sub ecx, esi
            b"\x56"                      # 73 push esi
            b"\x50"                      # 74 push eax
            b"\x66\x89\x4E\xAB"          # 75 mov [esi+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 79 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 84 mov ecx, eax
            b"\xE8"                      # 86 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 86,
            "packetId": 3,
            "retOffset": 0,
        },
        {
            "instanceR": 80,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9ACh
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+buf], cx
            b"\x8D\x48\xAB"              # 12 lea ecx, [eax+8]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_20E], cx
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 22 mov ecx, [ebp+Src]
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 28 mov [ebp+var_20C], edx
            b"\x73\x06"                  # 34 jnb short loc_4A129A
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 36 lea ecx, [ebp+Src]
            b"\x50"                      # 42 push eax
            b"\x51"                      # 43 push ecx
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 44 lea edx, [ebp+Dst]
            b"\x52"                      # 50 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 51 call memcpy
            b"\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 56 movsx ecx, [ebp+var_20E]
            b"\x83\xC4\x0C"              # 63 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 66 lea eax, [ebp+buf]
            b"\x50"                      # 72 push eax
            b"\x51"                      # 73 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 74 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 79 mov ecx, eax
            b"\xE8"                      # 81 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 81,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 75,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 2A7h
            b"\x66\x89\x0E"              # 5  mov [esi], cx
            b"\x8B\x55\xAB"              # 8  mov edx, [ebp+var_8]
            b"\x83\xC2\xAB"              # 11 add edx, 0Ah
            b"\x66\x89\x56\xAB"          # 14 mov [esi+2], dx
            b"\x66\x8B\x45\xAB"          # 18 mov ax, word ptr [ebp+var_8]
            b"\x66\x89\x46\xAB"          # 22 mov [esi+4], ax
            b"\x8B\x4F\xAB"              # 26 mov ecx, [edi+6]
            b"\x89\x4E\xAB"              # 29 mov [esi+6], ecx
            b"\x0F\xB7\x55\xAB"          # 32 movzx edx, word ptr [ebp+var_8]
            b"\x52"                      # 36 push edx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 37 lea eax, [ebp+Src]
            b"\x50"                      # 43 push eax
            b"\x8D\x4E\xAB"              # 44 lea ecx, [esi+0Ah]
            b"\x51"                      # 47 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call memcpy
            b"\x0F\xBF\x56\xAB"          # 53 movsx edx, word ptr [esi+2]
            b"\x83\xC4\xAB"              # 57 add esp, 0Ch
            b"\x56"                      # 60 push esi
            b"\x52"                      # 61 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0C9h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov [ebp-1064h], dx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov [ebp-1062h], cx
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 19 mov ecx, [ebp-1064h]
            b"\x33\xF6"                  # 25 xor esi, esi
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 27 mov [ebp+Dst], ecx
            b"\x85\xC0"                  # 33 test eax, eax
            b"\x7E\x61"                  # 35 jle short loc_7FF6EF
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 37 lea edi, [ebp+var_C5C]
            b"\x56"                      # 43 push esi
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 44 lea edx, [ebp+var_D8]
            b"\x52"                      # 50 push edx
            b"\xB9\xAB\xAB\xAB\xAB"      # 51 mov ecx, offset dword_CDA5A8
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call sub_88DD90
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 61 mov [ebp+var_4], 5
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 68 mov ax, word ptr [ebp+var_C8]
            b"\x66\x8B\x8D\xAB\xAB\xAB\xAB"  # 75 mov cx, word ptr [ebp+var_D0]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 82 mov [ebp-1062h], ax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 89 mov [ebp-1064h], cx
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 96 mov edx, [ebp-1064h]
            b"\x89\x17"                  # 102 mov [edi], edx
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 104 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 111 lea ecx, [ebp+var_D8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 117 call sub_45DFA0
            b"\x46"                      # 122 inc esi
            b"\x83\xC7\xAB"              # 123 add edi, 4
            b"\x3B\xB5\xAB\xAB\xAB\xAB"  # 126 cmp esi, [ebp+nNumberOfBytesToRe
            b"\x7C\xAB"                  # 132 jl short loc_7FF694
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 134 cmp [ebp+nNumberOfBytesToRea
            b"\x74\x24"                  # 141 jz short loc_7FF71C
            b"\x83\xBB\xAB\xAB\xAB\x00\xAB"  # 143 cmp dword ptr [ebx+2E4h], 0
            b"\x75\x1B"                  # 150 jnz short loc_7FF71C
            b"\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 152 movsx ecx, word ptr [ebp+Sou
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 159 lea eax, [ebp+Dst]
            b"\x50"                      # 165 push eax
            b"\x51"                      # 166 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 167 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 172 mov ecx, eax
            b"\xE8"                      # 174 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 174,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 168,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9D4h
            b"\x6A\xAB"                  # 5  push 2
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var_13BC],
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 19Ch
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+nNumberOfBy
            b"\x56"                      # 12 push esi
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 13 lea ecx, [ebp+var_354]
            b"\x8D\x7E\xAB"              # 19 lea edi, [esi+4]
            b"\x51"                      # 22 push ecx
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 23 lea edx, [ebp+var_85C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 29 mov word ptr [ebp+nNumberOfBy
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 36 mov eax, [ebp+nNumberOfBytesToRea
            b"\x52"                      # 42 push edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 43 mov dword ptr [ebp+var_860], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call memcpy
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_860]
            b"\x0F\xB7\xCF"              # 63 movzx ecx, di
            b"\x50"                      # 66 push eax
            b"\x51"                      # 67 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 73 mov ecx, eax
            b"\xE8"                      # 75 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 75,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 69,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 17Eh
            b"\x46"                      # 5  inc esi
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 6  mov [ebp-1064h], cx
            b"\x56"                      # 13 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_354]
            b"\x50"                      # 20 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 21 lea ecx, [ebp+var_85C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 27 mov [ebp-1062h], di
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 34 mov edx, [ebp-1064h]
            b"\x51"                      # 40 push ecx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 41 mov dword ptr [ebp+var_860], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call memcpy
            b"\x83\xC4\x0C"              # 52 add esp, 0Ch
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 55 lea edx, [ebp+var_860]
            b"\x0F\xB7\xC7"              # 61 movzx eax, di
            b"\x52"                      # 64 push edx
            b"\x50"                      # 65 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 71 mov ecx, eax
            b"\xE8"                      # 73 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 108h
            b"\x46"                      # 5  inc esi
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 6  mov [ebp-1064h], dx
            b"\x56"                      # 13 push esi
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 14 lea ecx, [ebp+var_354]
            b"\x51"                      # 20 push ecx
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 21 lea edx, [ebp+var_85C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 27 mov [ebp-1062h], di
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 34 mov eax, [ebp-1064h]
            b"\x52"                      # 40 push edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 41 mov dword ptr [ebp+var_860], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call memcpy
            b"\x83\xC4\x0C"              # 52 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 55 lea eax, [ebp+var_860]
            b"\x0F\xB7\xCF"              # 61 movzx ecx, di
            b"\x50"                      # 64 push eax
            b"\x51"                      # 65 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 71 mov ecx, eax
            b"\xE8"                      # 73 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9CEh
            b"\x6A\xAB"                  # 5  push 64h
            b"\x51"                      # 7  push ecx
            b"\x66\x89\x55\xAB"          # 8  mov word ptr [ebp+var_78], dx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call ds:_snprintf_s
            b"\x83\xC4\x20"              # 18 add esp, 20h
            b"\x8D\x55\xAB"              # 21 lea edx, [ebp+var_78]
            b"\x52"                      # 24 push edx
            b"\x6A\x66"                  # 25 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0F3h
            b"\x46"                      # 5  inc esi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 6  mov [ebp-1064h], ax
            b"\x56"                      # 13 push esi
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 14 lea edx, [ebp+var_354]
            b"\x52"                      # 20 push edx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 21 lea eax, [ebp+var_85C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 27 mov [ebp-1062h], di
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 34 mov ecx, [ebp-1064h]
            b"\x50"                      # 40 push eax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 41 mov dword ptr [ebp+var_860], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call memcpy
            b"\x83\xC4\x0C"              # 52 add esp, 0Ch
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 55 lea ecx, [ebp+var_860]
            b"\x0F\xB7\xD7"              # 61 movzx edx, di
            b"\x51"                      # 64 push ecx
            b"\x52"                      # 65 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 71 mov ecx, eax
            b"\xE8"                      # 73 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 98Dh
            b"\x6A\xAB"                  # 5  push 0
            b"\x8D\x7E\xAB"              # 7  lea edi, [esi+5]
            b"\x52"                      # 10 push edx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp-1064h], cx
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 18 mov [ebp-1062h], di
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 30 mov eax, [ebp-1064h]
            b"\x46"                      # 36 inc esi
            b"\x56"                      # 37 push esi
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 38 lea ecx, [ebp+var_354]
            b"\x51"                      # 44 push ecx
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 45 lea edx, [ebp+var_85C]
            b"\x52"                      # 51 push edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov dword ptr [ebp+var_860], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call memcpy
            b"\x83\xC4\x18"              # 63 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 66 lea eax, [ebp+var_860]
            b"\x0F\xBF\xCF"              # 72 movsx ecx, di
            b"\x50"                      # 75 push eax
            b"\x51"                      # 76 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 77 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 82 mov ecx, eax
            b"\xE8"                      # 84 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 84,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 78,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 22Dh
            b"\x6A\xAB"                  # 5  push 2
            b"\x66\x89\x55\xAB"          # 7  mov word ptr [ebp+Src], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 934h
            b"\x6A\xAB"                  # 5  push 2
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9D0h
            b"\x83\xC4\x10"              # 5  add esp, 10h
            b"\x66\x89\x06"              # 8  mov [esi], ax
            b"\x0F\xB7\xC7"              # 11 movzx eax, di
            b"\x56"                      # 14 push esi
            b"\x50"                      # 15 push eax
            b"\x66\x89\x7E\xAB"          # 16 mov [esi+2], di
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A16h
            b"\x6A\xFF"                  # 5  push 0FFFFFFFFh
            b"\x0F\x57\xC0"              # 7  xorps xmm0, xmm0
            b"\x66\x89\x45\xAB"          # 10 mov word ptr [ebp+buf], ax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+buf+2]
            b"\x6A\xAB"                  # 17 push 18h
            b"\x50"                      # 19 push eax
            b"\x66\x0F\xD6\x45\xAB"      # 20 movq qword ptr [ebp+buf+2], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 25 movq [ebp+var_16], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 30 movq [ebp+var_E], xmm0
            b"\x90"                      # 35 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call near ptr 361F75Dh
            b"\x83\xC4\x10"              # 41 add esp, 10h
            b"\x8D\x45\xAB"              # 44 lea eax, [ebp+buf]
            b"\x50"                      # 47 push eax
            b"\x6A\xAB"                  # 48 push 1Ah
            b"\xE8\xAB\xAB\xAB\xAB"      # 50 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 55 mov ecx, eax
            b"\xE8"                      # 57 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 57,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 51,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1D5h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x8D\x46\xAB"              # 8  lea eax, [esi+8]
            b"\x56"                      # 11 push esi
            b"\x66\x89\x47\xAB"          # 12 mov [edi+2], ax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 16 lea ecx, [ebp+var_170]
            b"\x51"                      # 22 push ecx
            b"\x83\xC0\xAB"              # 23 add eax, 0FFFFFFF8h
            b"\x50"                      # 26 push eax
            b"\x8D\x47\xAB"              # 27 lea eax, [edi+8]
            b"\x50"                      # 30 push eax
            b"\x89\x5F\xAB"              # 31 mov [edi+4], ebx
            b"\x90"                      # 34 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call near ptr 35AE380h
            b"\x0F\xB7\x47\xAB"          # 40 movzx eax, word ptr [edi+2]
            b"\x83\xC4\x10"              # 44 add esp, 10h
            b"\x57"                      # 47 push edi
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0F3h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x56"                      # 8  push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+var_4C8]
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call near ptr 35AE380h
            b"\x90"                      # 33 no nop
            b"\x83\xC4\x10"              # 34 add esp, 10h
            b"\x0F\xB7\x47\xAB"          # 37 movzx eax, word ptr [edi+2]
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 17Eh
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x56"                      # 8  push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+var_4C8]
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\x90"                      # 28 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call near ptr 35AE380h
            b"\x0F\xB7\x47\xAB"          # 34 movzx eax, word ptr [edi+2]
            b"\x83\xC4\x10"              # 38 add esp, 10h
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 108h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x56"                      # 8  push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+var_4C8]
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call near ptr 35AE380h
            b"\x90"                      # 33 no nop
            b"\x0F\xB7\x47\xAB"          # 34 movzx eax, word ptr [edi+2]
            b"\x83\xC4\x10"              # 38 add esp, 10h
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9CEh
            b"\x6A\xFF"                  # 5  push 0FFFFFFFFh
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+var_78], ax
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_76]
            b"\x6A\xAB"                  # 14 push 64h
            b"\x50"                      # 16 push eax
            b"\x90"                      # 17 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call near ptr 361F75Dh
            b"\x83\xC4\x20"              # 23 add esp, 20h
            b"\x8D\x45\xAB"              # 26 lea eax, [ebp+var_78]
            b"\x50"                      # 29 push eax
            b"\x6A\xAB"                  # 30 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 37 mov ecx, eax
            b"\xE8"                      # 39 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 33,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A16h
            b"\x6A\xAB"                  # 5  push 18h
            b"\x51"                      # 7  push ecx
            b"\x66\x89\x45\xAB"          # 8  mov word ptr [ebp+buf], ax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call _snprintf_s
            b"\x83\xC4\x10"              # 18 add esp, 10h
            b"\x8D\x55\xAB"              # 21 lea edx, [ebp+buf]
            b"\x52"                      # 24 push edx
            b"\x6A\xAB"                  # 25 push 1Ah
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9FBh
            b"\x8D\x54\x36\xAB"          # 5  lea edx, [esi+esi+6]
            b"\x6A\xAB"                  # 9  push 0
            b"\x50"                      # 11 push eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_200], c
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 19 mov word ptr [ebp+var_200+2],
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call j_memset
            b"\x66\x8B\x87\xAB\xAB\xAB\x00"  # 31 mov ax, [edi+0A8h]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 38 mov ecx, dword ptr [ebp+var_200]
            b"\x8D\x14\x36"              # 44 lea edx, [esi+esi]
            b"\x52"                      # 47 push edx
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 48 mov [ebp+var_110], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 55 lea eax, [ebp+var_10E]
            b"\x53"                      # 61 push ebx
            b"\x50"                      # 62 push eax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 63 mov [ebp-114h], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 69 call j_memcpy
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 74 movsx edx, word ptr [ebp+var_
            b"\x83\xC4\x18"              # 81 add esp, 18h
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 84 lea ecx, [ebp+buf]
            b"\x51"                      # 90 push ecx
            b"\x52"                      # 91 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 97 mov ecx, eax
            b"\xE8"                      # 99 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 99,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 93,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2DBh
            b"\x8D\x7E\xAB"              # 5  lea edi, [esi+5]
            b"\x46"                      # 8  inc esi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 9  mov [ebp-1064h], ax
            b"\x56"                      # 16 push esi
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 17 lea edx, [ebp+var_354]
            b"\x52"                      # 23 push edx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 24 lea eax, [ebp+var_85C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 30 mov [ebp-1062h], di
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 37 mov ecx, [ebp-1064h]
            b"\x50"                      # 43 push eax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 44 mov dword ptr [ebp+var_860], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 50 call j_memcpy
            b"\x0F\xB7\xD7"              # 55 movzx edx, di
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 58 lea ecx, [ebp+var_860]
            b"\x83\xC4\x0C"              # 64 add esp, 0Ch
            b"\x51"                      # 67 push ecx
            b"\x52"                      # 68 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 69 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 74 mov ecx, eax
            b"\xE8"                      # 76 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 76,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 70,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 96h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_10A4],
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 12 mov ecx, dword ptr [ebp+anonymous
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_1098], ecx
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 24 mov ecx, [ebp-1074h]
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 30 mov dword ptr [ebp+var_1098+4], e
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 36 mov edx, [ebp-1064h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 42 mov dword ptr [ebp+var_10A4+8], e
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 48 mov eax, [ebp-1078h]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 54 mov [ebp-108Ch], ecx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov [ebp-1090h], eax
            b"\x42"                      # 66 inc edx
            b"\x83\xC3\xAB"              # 67 add ebx, 1Dh
            b"\x52"                      # 70 push edx
            b"\xB9\xAB\xAB\xAB\x00"      # 71 mov ecx, 7
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 76 lea esi, [ebp+var_10A4]
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 82 lea edi, [ebp+var_860]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 88 lea eax, [ebp+var_244]
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 94 mov word ptr [ebp+var_10A4+2]
            b"\xF3\xA5"                  # 101 rep movsd
            b"\x50"                      # 103 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 104 lea ecx, [ebp+var_844]
            b"\x51"                      # 110 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 111 call j_memcpy
            b"\x83\xC4\x0C"              # 116 add esp, 0Ch
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 119 lea edx, [ebp+var_860]
            b"\x0F\xB7\xC3"              # 125 movzx eax, bx
            b"\x52"                      # 128 push edx
            b"\x50"                      # 129 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 130 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 135 mov ecx, eax
            b"\xE8"                      # 137 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 137,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 131,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 155h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-1064h], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, _dword_C49BF8
            b"\x8D\x1C\xAB"              # 17 lea ebx, [eax+eax*2]
            b"\x8D\x1C\x9D\xAB\xAB\xAB\xAB"  # 20 lea ebx, ds:4[ebx*4]
            b"\xB9\xAB\xAB\xAB\xAB"      # 27 mov ecx, 800h
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov [ebp-1062h], bx
            b"\x66\x3B\xCB"              # 39 cmp cx, bx
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 42 jl loc_81DCB0
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 48 mov edx, [ebp-1064h]
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 54 mov edi, _dword_C49BF4
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 60 mov dword ptr [ebp+buf], edx
            b"\x8B\x07"                  # 66 mov eax, [edi]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 68 mov [ebp-1064h], eax
            b"\x3B\xC7"                  # 74 cmp eax, edi
            b"\x74\x34"                  # 76 jz short loc_821549
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 78 lea esi, [ebp+var_C5C]
            b"\xEB\x03"                  # 84 jmp short loc_821520
            b"\xAB\xAB\xAB"              # 86 align 10h
            b"\x8B\x48\xAB"              # 89 mov ecx, [eax+10h]
            b"\x8B\x50\xAB"              # 92 mov edx, [eax+14h]
            b"\x8B\x40\xAB"              # 95 mov eax, [eax+18h]
            b"\x89\x0E"                  # 98 mov [esi], ecx
            b"\x89\x56\xAB"              # 100 mov [esi+4], edx
            b"\x89\x46\xAB"              # 103 mov [esi+8], eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 106 lea ecx, [ebp-1064h]
            b"\x83\xC6\x0C"              # 112 add esi, 0Ch
            b"\xE8\xAB\xAB\xAB\xAB"      # 115 call sub_54CBE0
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 120 mov eax, [ebp-1064h]
            b"\x3B\xC7"                  # 126 cmp eax, edi
            b"\x75\xD7"                  # 128 jnz short loc_821520
            b"\xB9\xAB\xAB\xAB\xAB"      # 130 mov ecx, offset _dword_C49BF0
            b"\xE8\xAB\xAB\xAB\xAB"      # 135 call sub_552740
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 140 lea eax, [ebp+buf]
            b"\x0F\xBF\xCB"              # 146 movsx ecx, bx
            b"\x50"                      # 149 push eax
            b"\x51"                      # 150 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 151 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 156 mov ecx, eax
            b"\xE8"                      # 158 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 158,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 152,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9CEh
            b"\x6A\xAB"                  # 5  push 64h
            b"\x50"                      # 7  push eax
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 8  mov word ptr [ebp+var_1CC], d
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 15 call _snprintf_s
            b"\x83\xC4\x20"              # 21 add esp, 20h
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 24 lea ecx, [ebp+var_1CC]
            b"\x51"                      # 30 push ecx
            b"\x6A\xAB"                  # 31 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 98Dh
            b"\x6A\xAB"                  # 5  push 0
            b"\x8D\x7E\xAB"              # 7  lea edi, [esi+5]
            b"\x51"                      # 10 push ecx
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 11 mov [ebp-1064h], ax
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 18 mov [ebp-1062h], di
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call j_memset
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 30 mov edx, [ebp-1064h]
            b"\x46"                      # 36 inc esi
            b"\x56"                      # 37 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_354]
            b"\x50"                      # 44 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 45 lea ecx, [ebp+var_85C]
            b"\x51"                      # 51 push ecx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 52 mov dword ptr [ebp+var_860], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call j_memcpy
            b"\x83\xC4\x18"              # 63 add esp, 18h
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 66 lea edx, [ebp+var_860]
            b"\x0F\xBF\xC7"              # 72 movsx eax, di
            b"\x52"                      # 75 push edx
            b"\x50"                      # 76 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 77 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 82 mov ecx, eax
            b"\xE8"                      # 84 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 84,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 78,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 860h
            b"\x8D\x4D\xAB"              # 5  lea ecx, [ebp+buf]
            b"\x51"                      # 8  push ecx
            b"\x6A\xAB"                  # 9  push 2
            b"\x66\x89\x45\xAB"          # 11 mov word ptr [ebp+buf], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 187h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_6], 0
            b"\x6A\xAB"                  # 12 push 6
            b"\x66\x89\x4D\xAB"          # 14 mov word ptr [ebp+buf], cx
            b"\x89\x55\xAB"              # 18 mov [ebp+var_6], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2014-09-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9A1h
            b"\x6A\xAB"                  # 5  push 2
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+buf], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2016-01-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0F3h
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x56"                      # 8  push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+var_3D0]
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 28 call memcpy_s
            b"\x83\xC4\x10"              # 34 add esp, 10h
            b"\x0F\xB7\x47\xAB"          # 37 movzx eax, word ptr [edi+2]
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2016-01-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 17Eh
            b"\x66\x89\x07"              # 5  mov [edi], ax
            b"\x56"                      # 8  push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+var_3D0]
            b"\x50"                      # 15 push eax
            b"\x8D\x43\xAB"              # 16 lea eax, [ebx-4]
            b"\x50"                      # 19 push eax
            b"\x8D\x47\xAB"              # 20 lea eax, [edi+4]
            b"\x50"                      # 23 push eax
            b"\x66\x89\x5F\xAB"          # 24 mov [edi+2], bx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 28 call memcpy_s
            b"\x0F\xB7\x47\xAB"          # 34 movzx eax, word ptr [edi+2]
            b"\x83\xC4\x10"              # 38 add esp, 10h
            b"\x57"                      # 41 push edi
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2016-01-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9CEh
            b"\x6A\xAB"                  # 5  push 0FFFFFFFFh
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+var_78], ax
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_76]
            b"\x6A\xAB"                  # 14 push 64h
            b"\x50"                      # 16 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 17 call _snprintf_s
            b"\x83\xC4\x20"              # 23 add esp, 20h
            b"\x8D\x45\xAB"              # 26 lea eax, [ebp+var_78]
            b"\x50"                      # 29 push eax
            b"\x6A\xAB"                  # 30 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 37 mov ecx, eax
            b"\xE8"                      # 39 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 33,
        }
    ],
    # 2015-04-08
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2A7h
            b"\x66\x89\x06"              # 5  mov [esi], ax
            b"\x8B\x45\xAB"              # 8  mov eax, [ebp+var_8]
            b"\x83\xC0\xAB"              # 11 add eax, 0Ah
            b"\x66\x89\x46\xAB"          # 14 mov [esi+2], ax
            b"\x66\x8B\x45\xAB"          # 18 mov ax, word ptr [ebp+var_8]
            b"\x66\x89\x46\xAB"          # 22 mov [esi+4], ax
            b"\x8B\x47\xAB"              # 26 mov eax, [edi+6]
            b"\x89\x46\xAB"              # 29 mov [esi+6], eax
            b"\x0F\xB7\x45\xAB"          # 32 movzx eax, word ptr [ebp+var_8]
            b"\x50"                      # 36 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 37 lea eax, [ebp+var_260]
            b"\x50"                      # 43 push eax
            b"\x8D\x46\xAB"              # 44 lea eax, [esi+0Ah]
            b"\x50"                      # 47 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call j_memcpy
            b"\x0F\xBF\x46\xAB"          # 53 movsx eax, word ptr [esi+2]
            b"\x83\xC4\x0C"              # 57 add esp, 0Ch
            b"\x56"                      # 60 push esi
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2015-04-08
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 91Eh
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ebp+anonymous_0+2]
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 11 mov ebx, dword ptr [ebp+var_10A0+
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 17 mov [ebp+anonymous_1], cx
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 24 mov word ptr [ebp+anonymous_0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 31 movq xmm0, qword ptr [ebp
            b"\x33\xF6"                  # 39 xor esi, esi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 41 movq qword ptr [ebp+buf],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 49 mov [ebp+var_D90]
            b"\x85\xDB"                  # 59 test ebx, ebx
            b"\x7E\xAB"                  # 61 jle short loc_878D50
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 63 lea edi, [ebp+var_D8C]
            b"\x8D\xA4\x24\x00\x00\x00\x00"  # 69 lea esp, [esp+0]
            b"\x56"                      # 76 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 77 lea eax, [ebp+var_130]
            b"\x50"                      # 83 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 84 mov ecx, offset dword_E15578
            b"\xE8\xAB\xAB\xAB\xAB"      # 89 call sub_920670
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 94 mov [ebp+var_4], 6
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 101 movzx eax, word ptr [ebp+var
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 108 mov word ptr [ebp+var_10A4],
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 115 movzx eax, word ptr [ebp+var
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 122 mov word ptr [ebp+var_10A4+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 129 mov eax, dword ptr [ebp+var_10A4
            b"\x89\x07"                  # 135 mov [edi], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 137 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 144 lea ecx, [ebp+var_130]
            b"\xE8\xAB\xAB\xAB\xAB"      # 150 call sub_480A40
            b"\x46"                      # 155 inc esi
            b"\x8D\x7F\xAB"              # 156 lea edi, [edi+4]
            b"\x3B\xF3"                  # 159 cmp esi, ebx
            b"\x7C\xAB"                  # 161 jl short loc_878CF0
            b"\x8D\x04\x9D\xAB\xAB\xAB\xAB"  # 163 lea eax, ds:0Ch[ebx*4]
            b"\x85\xDB"                  # 170 test ebx, ebx
            b"\x74\xAB"                  # 172 jz short loc_878D67
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 174 lea ecx, [ebp+buf]
            b"\x51"                      # 180 push ecx
            b"\x98"                      # 181 cwd cwde
            b"\x50"                      # 182 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 183 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 188 mov ecx, eax
            b"\xE8"                      # 190 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 190,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 184,
        }
    ],
    # 2015-04-08
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1D5h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_998], a
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 12 mov dword ptr [ebp+var_998+4], eb
            b"\x8D\x51\xAB"              # 18 lea edx, [ecx+1]
            b"\x90"                      # 21 no nop
            b"\x8A\x01"                  # 22 mov al, [ecx]
            b"\x41"                      # 24 inc ecx
            b"\x84\xC0"                  # 25 test al, al
            b"\x75\xAB"                  # 27 jnz short loc_87D540
            b"\x2B\xCA"                  # 29 sub ecx, edx
            b"\x8D\x41\xAB"              # 31 lea eax, [ecx+9]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 34 mov word ptr [ebp+var_998+2],
            b"\x8D\x41\xAB"              # 41 lea eax, [ecx+1]
            b"\x50"                      # 44 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 45 lea eax, [ebp+var_E98]
            b"\x50"                      # 51 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 52 lea eax, [ebp+var_990]
            b"\x50"                      # 58 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call j_memcpy
            b"\x83\xC4\x0C"              # 64 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 67 lea eax, [ebp+var_998]
            b"\x50"                      # 73 push eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 74 movzx eax, word ptr [ebp+var_
            b"\x50"                      # 81 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 82 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 87 mov ecx, eax
            b"\xE8"                      # 89 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 89,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 83,
        }
    ],
    # 2018-12-12
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0C9h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DC5C],
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_DC5C+2]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 19 mov ecx, [ebp+var_DC5C]
            b"\x33\xF6"                  # 25 xor esi, esi
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+buf], ecx
            b"\x85\xC0"                  # 33 test eax, eax
            b"\x7E\x73"                  # 35 jle short loc_A0F66C
            b"\x8D\x9D\xAB\xAB\xAB\xAB"  # 37 lea ebx, [ebp+var_B400]
            b"\x8B\xF8"                  # 43 mov edi, eax
            b"\x56"                      # 45 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 46 lea eax, [ebp+var_D128]
            b"\x50"                      # 52 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 53 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call CSession_sub_ACAF70
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 63 mov [ebp+var_4], 5
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 70 movzx eax, [ebp+var_D118]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 77 mov word ptr [ebp+var_DC5C+2]
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 84 movzx eax, [ebp+var_D120]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 91 mov word ptr [ebp+var_DC5C],
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 98 mov eax, [ebp+var_DC5C]
            b"\x89\x03"                  # 104 mov [ebx], eax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 106 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 113 lea ecx, [ebp+var_D128]
            b"\xE8\xAB\xAB\xAB\xAB"      # 119 call sub_459C30
            b"\x46"                      # 124 inc esi
            b"\x8D\x5B\xAB"              # 125 lea ebx, [ebx+4]
            b"\x3B\xF7"                  # 128 cmp esi, edi
            b"\x7C\xA9"                  # 130 jl short loc_A0F601
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 132 mov eax, [ebp+var_DC58+4]
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 138 mov edi, [ebp+var_DC4C]
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 144 mov edx, [ebp+var_BA70]
            b"\x85\xC0"                  # 150 test eax, eax
            b"\x74\x20"                  # 152 jz short loc_A0F68E
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 154 cmp dword ptr [edi+254h], 0
            b"\x75\x17"                  # 161 jnz short loc_A0F68E
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 163 lea eax, [ebp+buf]
            b"\x50"                      # 169 push eax
            b"\x0F\xBF\xC2"              # 170 movsx eax, dx
            b"\x50"                      # 173 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 179 mov ecx, eax
            b"\xE8"                      # 181 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2018-12-12
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 7E4h
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_DC64+4], ebx
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 11 mov ebx, [ebp+var_BA70]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 17 mov word ptr [ebp+var_DC64],
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 24 mov word ptr [ebp+var_DC64+2]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 31 movq xmm0, qword ptr [ebp
            b"\x33\xF6"                  # 39 xor esi, esi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 41 movq qword ptr [ebp+var_7
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 49 mov dword ptr [eb
            b"\x85\xDB"                  # 59 test ebx, ebx
            b"\x7E\x6C"                  # 61 jle short loc_A0F790
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 63 lea edi, [ebp+var_7404+0Ch]
            b"\x8D\x9B\xAB\xAB\xAB\xAB"  # 69 lea ebx, [ebx+0]
            b"\x56"                      # 75 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 76 lea eax, [ebp+var_D3C8]
            b"\x50"                      # 82 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 83 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 88 call CSession_sub_ACAF70
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 93 mov [ebp+var_4], 6
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 100 movzx eax, [ebp+var_D3C0]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 107 mov word ptr [ebp+var_DC4C],
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 114 movzx eax, [ebp+var_D3B8]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 121 mov word ptr [ebp+var_DC4C+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 128 mov eax, [ebp+var_DC4C]
            b"\x89\x07"                  # 134 mov [edi], eax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 136 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 143 lea ecx, [ebp+var_D3C8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 149 call sub_459C30
            b"\x46"                      # 154 inc esi
            b"\x8D\x7F\xAB"              # 155 lea edi, [edi+4]
            b"\x3B\xF3"                  # 158 cmp esi, ebx
            b"\x7C\xA9"                  # 160 jl short loc_A0F730
            b"\x8D\x04\x9D\xAB\xAB\xAB\x00"  # 162 lea eax, ds:0Ch[ebx*4]
            b"\x85\xDB"                  # 169 test ebx, ebx
            b"\x74\x15"                  # 171 jz short loc_A0F7A7
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 173 lea ecx, [ebp+var_7404]
            b"\x51"                      # 179 push ecx
            b"\x98"                      # 180 cwd cwde
            b"\x50"                      # 181 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 182 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 187 mov ecx, eax
            b"\xE8"                      # 189 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 189,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 183,
        }
    ],
    # 2018-12-19
    [
        (
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 0  lea eax, [ebp+var_BDB0]
            b"\x50"                      # 6  push eax
            b"\x6A\x0A"                  # 7  push 0Ah
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 9  mov dword ptr [eb
            b"\xBE\xAB\xAB\x00\x00"      # 19 mov esi, 7F4h
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 29 mov ecx, eax
            b"\xE8"                      # 31 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 31,
            "packetId": (15, 2),
            "retOffset": 9,
        },
        {
            "instanceR": 25,
        }
    ],
    # 2018-12-19
    [
        (
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 0  lea eax, [ebp+var_E018]
            b"\x50"                      # 6  push eax
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\x00\x00"  # 7  mov dword ptr [eb
            b"\x6A\xAB"                  # 17 push 4
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 19 mov word ptr [ebp+var_E018+2]
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 33,
            "packetId": 13,
            "retOffset": 7,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 155h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF80],
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_1094AE0
            b"\x8D\x04\x40"              # 17 lea eax, [eax+eax*2]
            b"\x8D\x1C\x85\xAB\xAB\x00\x00"  # 20 lea ebx, ds:4[eax*4]
            b"\xB8\xAB\xAB\xAB\x00"      # 27 mov eax, 800h
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov word ptr [ebp+var_DF80+2]
            b"\x66\x3B\xC3"              # 39 cmp ax, bx
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 42 jl loc_A18C34
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 48 mov eax, [ebp+var_DF80]
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 54 mov edi, dword_1094ADC
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov dword ptr [ebp+var_8F10], eax
            b"\x8B\x07"                  # 66 mov eax, [edi]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 68 mov [ebp+var_DF64], eax
            b"\x3B\xC7"                  # 74 cmp eax, edi
            b"\x74\x47"                  # 76 jz short loc_A1C99C
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 78 lea esi, [ebp+var_8F0C]
            b"\xEB\x03"                  # 84 jmp short loc_A1C960
            b"\xAB\xAB\xAB"              # 86 align 10h
            b"\x8B\x48\xAB"              # 89 mov ecx, [eax+14h]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 92 mov dword ptr [ebp+var_BD84], ecx
            b"\x8B\x48\xAB"              # 98 mov ecx, [eax+18h]
            b"\x8B\x40\xAB"              # 101 mov eax, [eax+1Ch]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 104 mov dword ptr [ebp+var_BD84+4],
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 110 movq xmm0, [ebp+var_BD84
            b"\x66\x0F\xD6\x06"          # 118 movq qword ptr [esi], xmm0
            b"\x89\x46\xAB"              # 122 mov [esi+8], eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 125 lea ecx, [ebp+var_DF64]
            b"\x8D\x76\xAB"              # 131 lea esi, [esi+0Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 134 call sub_692DD0
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 139 mov eax, [ebp+var_DF64]
            b"\x3B\xC7"                  # 145 cmp eax, edi
            b"\x75\xC4"                  # 147 jnz short loc_A1C960
            b"\xB9\xAB\xAB\xAB\xAB"      # 149 mov ecx, offset dword_1094ADC
            b"\xE8\xAB\xAB\xAB\xAB"      # 154 call sub_6A2710
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 159 lea eax, [ebp+var_8F10]
            b"\x50"                      # 165 push eax
            b"\x0F\xBF\xC3"              # 166 movsx eax, bx
            b"\x50"                      # 169 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 170 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 175 mov ecx, eax
            b"\xE8"                      # 177 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 177,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 171,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 0  mov [ebp+buf], 0B11h
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+buf+2], ax
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+buf]
            b"\x50"                      # 14 push eax
            b"\x6A\xAB"                  # 15 push 4
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 24,
            "packetId": 3,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1B2h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_3F10],
            b"\x8D\x0C\xC5\xAB\xAB\xAB\x00"  # 12 lea ecx, ds:55h[eax*8]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_BD7C], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var_3F10+2]
            b"\x75\x1E"                  # 32 jnz short loc_A1C5AA
            b"\x6A\xAB"                  # 34 push 4Fh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp+var_3F0C]
            b"\x53"                      # 42 push ebx
            b"\x50"                      # 43 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 44 call strncpy
            b"\x83\xC4\x0C"              # 50 add esp, 0Ch
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 53 mov [ebp+var_3EBD], 1
            b"\xEB\x0E"                  # 62 jmp short loc_A1C5B8
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 64 mov [ebp+var_3F0C], 0
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 71 mov byte ptr [ebp+var_3EBD+1]
            b"\x83\x3D\xAB\xAB\xAB\xAB\x00"  # 78 cmp dword_10B7B94, 0
            b"\x75\x11"                  # 85 jnz short loc_A1C5D2
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 87 lea eax, [ebp+var_3F0C]
            b"\x50"                      # 93 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 94 mov ecx, (offset g_session+4E20h)
            b"\xE8\xAB\xAB\xAB\xAB"      # 99 call sub_4513A0
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 104 mov ebx, [ebp+var_BD7C]
            b"\x33\xFF"                  # 110 xor edi, edi
            b"\x85\xDB"                  # 112 test ebx, ebx
            b"\x7E\x5C"                  # 114 jle short loc_A1C63A
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 116 lea esi, [ebp+var_3EB7]
            b"\x57"                      # 122 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 123 lea eax, [ebp+var_D280]
            b"\x50"                      # 129 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 130 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 135 call CSession_sub_AD2AC0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 140 mov [ebp+var_4], 36h
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 147 mov eax, [ebp+var_D278]
            b"\x85\xC0"                  # 153 test eax, eax
            b"\x74\x1A"                  # 155 jz short loc_A1C621
            b"\x66\x89\x46\xAB"          # 157 mov [esi-4], ax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 161 mov ax, [ebp+var_D270]
            b"\x66\x89\x46\xAB"          # 168 mov [esi-2], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 172 mov eax, [ebp+var_D268]
            b"\x89\x06"                  # 178 mov [esi], eax
            b"\x83\xC6\x08"              # 180 add esi, 8
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 183 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 190 lea ecx, [ebp+var_D280]
            b"\xE8\xAB\xAB\xAB\xAB"      # 196 call sub_45C3F0
            b"\x47"                      # 201 inc edi
            b"\x3B\xFB"                  # 202 cmp edi, ebx
            b"\x7C\xAC"                  # 204 jl short loc_A1C5E4
            b"\x85\xDB"                  # 206 test ebx, ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 208 lea eax, [ebp+var_3F10]
            b"\x50"                      # 214 push eax
            b"\x0F\xBF\x85\xF2\xC0\xFF\xFF"  # 215 movsx eax, word ptr [ebp+var
            b"\x50"                      # 222 push eax
            b"\x75\x07"                  # 223 jnz short loc_A1C652
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 225 mov byte ptr [ebp+var_3EBD+1
            b"\xE8\xAB\xAB\xAB\xAB"      # 232 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 237 mov ecx, eax
            b"\xE8"                      # 239 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 239,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 233,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 801h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF7C],
            b"\x8D\x0C\x85\xAB\xAB\xAB\x00"  # 12 lea ecx, ds:0Ch[eax*4]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_BD7C], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var_DF7C+2]
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_DF7C+4], ebx
            b"\x81\xF9\xAB\xAB\xAB\x00"  # 38 cmp ecx, 800h
            b"\x0F\x87\x7F\xC5\xFF\xFF"  # 44 ja loc_A18C34
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 50 mov ebx, [ebp+var_BD7C]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 56 movq xmm0, qword ptr [ebp
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 64 mov eax, [ebp+var_DF70+4]
            b"\x33\xF6"                  # 70 xor esi, esi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 72 movq qword ptr [ebp+var_9
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 80 mov [ebp+var_9708], eax
            b"\x85\xDB"                  # 86 test ebx, ebx
            b"\x7E\x69"                  # 88 jle short loc_A1C746
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 90 lea edi, [ebp+var_9704]
            b"\x56"                      # 96 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 97 lea eax, [ebp+var_D6E0]
            b"\x50"                      # 103 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 104 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 109 call CSession_sub_AD3640
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 114 mov [ebp+var_4], 37h
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 121 mov ecx, [ebp+var_D6D8]
            b"\x85\xC9"                  # 127 test ecx, ecx
            b"\x74\x1D"                  # 129 jz short loc_A1C723
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 131 mov ax, [ebp+var_D6D0]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 138 mov word ptr [ebp+var_DF64],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 145 mov word ptr [ebp+var_DF64+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 152 mov eax, [ebp+var_DF64]
            b"\x89\x07"                  # 158 mov [edi], eax
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 160 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 167 lea ecx, [ebp+var_D6E0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 173 call sub_45C3F0
            b"\x46"                      # 178 inc esi
            b"\x83\xC7\xAB"              # 179 add edi, 4
            b"\x3B\xF3"                  # 182 cmp esi, ebx
            b"\x7C\xA6"                  # 184 jl short loc_A1C6E3
            b"\x8D\x0C\x9D\xAB\xAB\xAB\x00"  # 186 lea ecx, ds:0Ch[ebx*4]
            b"\x85\xDB"                  # 193 test ebx, ebx
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 195 jz loc_A18C34
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 201 lea eax, [ebp+var_9710]
            b"\x50"                      # 207 push eax
            b"\x0F\xBF\xC1"              # 208 movsx eax, cx
            b"\x50"                      # 211 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 212 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 217 mov ecx, eax
            b"\xE8"                      # 219 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 219,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 213,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 161h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF80],
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, _dword_1094AE8
            b"\x8D\x04\x80"              # 17 lea eax, [eax+eax*4]
            b"\x8D\x1C\xC5\xAB\xAB\xAB\x00"  # 20 lea ebx, ds:4[eax*8]
            b"\xB8\xAB\xAB\xAB\x00"      # 27 mov eax, 800h
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_BD7C], ebx
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 38 mov word ptr [ebp+var_DF80+2]
            b"\x66\x3B\xC3"              # 45 cmp ax, bx
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 48 jl loc_A18C34
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 54 mov eax, [ebp+var_DF80]
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 60 mov ecx, _dword_1094AE4
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 66 mov dword ptr [ebp+var_AF10], eax
            b"\x8B\x01"                  # 72 mov eax, [ecx]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 74 mov [ebp+var_DF64], eax
            b"\x3B\xC1"                  # 80 cmp eax, ecx
            b"\x0F\x84\xD9\x00\x00\x00"  # 82 jz loc_A1CB02
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 88 mov edi, strncpy
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 94 lea esi, [ebp+var_AF0C]
            b"\xBB\xAB\xAB\xAB\x00"      # 100 mov ebx, 1
            b"\x8D\x9B\xAB\xAB\xAB\x00"  # 105 lea ebx, [ebx+0]
            b"\x8B\x48\xAB"              # 111 mov ecx, [eax+18h]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 114 mov dword ptr [ebp+var_BFFC], ec
            b"\x33\xC9"                  # 120 xor ecx, ecx
            b"\x39\x48\xAB"              # 122 cmp [eax+34h], ecx
            b"\x0F\x45\xCB"              # 125 cmovnz ecx, ebx
            b"\x83\x78\xAB\x00"          # 128 cmp dword ptr [eax+38h], 0
            b"\x74\x03"                  # 132 jz short loc_A1CA5A
            b"\x83\xC9\xAB"              # 134 or ecx, 10h
            b"\x83\x78\xAB\x00"          # 137 cmp dword ptr [eax+40h], 0
            b"\x74\x06"                  # 141 jz short loc_A1CA66
            b"\x81\xC9\xAB\xAB\xAB\x00"  # 143 or ecx, 100h
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 149 mov dword ptr [ebp+var_BFFC+4],
            b"\x8B\x48\xAB"              # 155 mov ecx, [eax+14h]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 158 mov dword ptr [ebp+var_BFF4], ec
            b"\x8B\x48\xAB"              # 164 mov ecx, [eax+3Ch]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 167 mov dword ptr [ebp+var_BFF4+4],
            b"\x6A\xAB"                  # 173 push 17h
            b"\x8D\x48\xAB"              # 175 lea ecx, [eax+1Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 178 call sub_44B030
            b"\x50"                      # 183 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 184 lea eax, [ebp+var_BFEC]
            b"\x50"                      # 190 push eax
            b"\xFF\xD7"                  # 191 call edi
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 193 movq xmm0, [ebp+var_BFFC
            b"\x66\x0F\xD6\x06"          # 201 movq qword ptr [esi], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 205 movq xmm0, [ebp+var_BFF4
            b"\x66\x0F\xD6\x46\xAB"      # 213 movq qword ptr [esi+8], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 218 movq xmm0, [ebp+var_BFEC
            b"\x66\x0F\xD6\x46\xAB"      # 226 movq qword ptr [esi+10h], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 231 movq xmm0, [ebp+var_BFE4
            b"\x66\x0F\xD6\x46\xAB"      # 239 movq qword ptr [esi+18h], xmm0
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 244 mov [ebp+var_BFD5], 0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 251 movq xmm0, qword ptr [eb
            b"\x66\x0F\xD6\x46\xAB"      # 259 movq qword ptr [esi+20h], xmm0
            b"\x83\xC4\x0C"              # 264 add esp, 0Ch
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 267 lea ecx, [ebp+var_DF64]
            b"\x83\xC6\x28"              # 273 add esi, 28h
            b"\xE8\xAB\xAB\xAB\xAB"      # 276 call sub_692D80
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 281 mov eax, [ebp+var_DF64]
            b"\x3B\x05\xAB\xAB\xAB\xAB"  # 287 cmp eax, _dword_1094AE4
            b"\x0F\x85\x44\xFF\xFF\xFF"  # 293 jnz loc_A1CA40
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 299 mov ebx, [ebp+var_BD7C]
            b"\xB9\xAB\xAB\xAB\xAB"      # 305 mov ecx, offset _dword_1094AE4
            b"\xE8\xAB\xAB\xAB\xAB"      # 310 call sub_6A26E0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 315 lea eax, [ebp+var_AF10]
            b"\x50"                      # 321 push eax
            b"\x0F\xBF\xC3"              # 322 movsx eax, bx
            b"\x50"                      # 325 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 326 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 331 mov ecx, eax
            b"\xE8"                      # 333 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 333,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 327,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A6Eh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_DF04], ax
            b"\x8B\x03"                  # 12 mov eax, [ebx]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_DEC4], eax
            b"\x8B\x43\xAB"              # 20 mov eax, [ebx+4Ch]
            b"\x99"                      # 23 cd cdq
            b"\x8D\x4B\xAB"              # 24 lea ecx, [ebx+4]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+var_DED0], eax
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 33 mov dword ptr [ebp+var_DED0+4], e
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call sub_44B030
            b"\x8B\x35\xAB\xAB\xAB\xAB"  # 44 mov esi, strcpy_s
            b"\x50"                      # 50 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp+var_DF00]
            b"\x6A\xAB"                  # 57 push 18h
            b"\x50"                      # 59 push eax
            b"\xFF\xD6"                  # 60 call esi
            b"\x83\xC4\x0C"              # 62 add esp, 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 65 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 70 call CSession_sub_AF60C0
            b"\x50"                      # 75 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 76 lea eax, [ebp+var_DEE8]
            b"\x6A\xAB"                  # 82 push 18h
            b"\x50"                      # 84 push eax
            b"\xFF\xD6"                  # 85 call esi
            b"\x8B\x43\xAB"              # 87 mov eax, [ebx+2Ch]
            b"\x40"                      # 90 inc eax
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 91 mov [ebp+var_DEC8], ax
            b"\x8B\x43\xAB"              # 98 mov eax, [ebx+44h]
            b"\x40"                      # 101 inc eax
            b"\x83\xC4\x0C"              # 102 add esp, 0Ch
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 105 lea ecx, [ebp+var_DF7C]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 111 mov [ebp+var_DEC6], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 118 call sub_45BCF0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 123 mov [ebp+var_4], 3Fh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 130 lea eax, [ebp+var_DEC0]
            b"\x50"                      # 136 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 137 lea eax, [ebp+var_DF04]
            b"\x50"                      # 143 push eax
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 144 push [ebp+var_DF7C+4]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 150 lea eax, [ebp+var_BD7C]
            b"\x50"                      # 156 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 157 lea ecx, [ebp+var_DF7C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 163 call sub_521A40
            b"\x8D\x4B\xAB"              # 168 lea ecx, [ebx+1Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 171 call sub_44B030
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 176 movsx edx, [ebp+var_DEC8]
            b"\x03\xC2"                  # 183 add eax, edx
            b"\x50"                      # 185 push eax
            b"\x8D\x4B\xAB"              # 186 lea ecx, [ebx+1Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 189 call sub_44B030
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 194 lea ecx, [ebp+var_DF7C]
            b"\x50"                      # 200 push eax
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 201 push [ebp+var_DF7C+4]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 207 lea eax, [ebp+var_BD7C]
            b"\x50"                      # 213 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 214 call sub_6FE9E0
            b"\x8D\x4B\xAB"              # 219 lea ecx, [ebx+34h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 222 call sub_44B030
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 227 movsx edx, [ebp+var_DEC6]
            b"\x03\xC2"                  # 234 add eax, edx
            b"\x50"                      # 236 push eax
            b"\x8D\x4B\xAB"              # 237 lea ecx, [ebx+34h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 240 call sub_44B030
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 245 lea ecx, [ebp+var_DF7C]
            b"\x50"                      # 251 push eax
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 252 push [ebp+var_DF7C+4]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 258 lea eax, [ebp+var_BD7C]
            b"\x50"                      # 264 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 265 call sub_6FE9E0
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 270 mov eax, [ebp+var_DF7C]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 276 mov ecx, [ebp+var_DF7C+4]
            b"\x2B\xC8"                  # 282 sub ecx, eax
            b"\x50"                      # 284 push eax
            b"\x66\x89\x48\xAB"          # 285 mov [eax+2], cx
            b"\x0F\xBF\xC1"              # 289 movsx eax, cx
            b"\x50"                      # 292 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 293 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 298 mov ecx, eax
            b"\xE8"                      # 300 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 300,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 294,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 811h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_2C08],
            b"\x8D\x0C\x80"              # 12 lea ecx, [eax+eax*4]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_BD7C], eax
            b"\x8D\x0C\x4D\xAB\xAB\xAB\x00"  # 21 lea ecx, ds:59h[ecx*2]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 28 mov word ptr [ebp+var_2C08+2]
            b"\x75\x2F"                  # 35 jnz short loc_A1E83E
            b"\x6A\xAB"                  # 37 push 4Fh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 39 lea eax, [ebp+var_2C00+1]
            b"\x53"                      # 45 push ebx
            b"\x50"                      # 46 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 47 call strncpy
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 53 mov eax, [ebp+var_DF74]
            b"\x83\xC4\x0C"              # 59 add esp, 0Ch
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 62 mov [ebp+var_2BB0], 0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 69 mov [ebp+var_2C04], eax
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 75 mov byte ptr [ebp+var_2C00],
            b"\xEB\x13"                  # 82 jmp short loc_A1E851
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 84 mov word ptr [ebp+var
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 93 mov [ebp+var_2C04
            b"\x83\x3D\xAB\xAB\xAB\xAB\x00"  # 103 cmp dword_10B7B94, 0
            b"\x75\x11"                  # 110 jnz short loc_A1E86B
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 112 lea eax, [ebp+var_2C00+1]
            b"\x50"                      # 118 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 119 mov ecx, (offset g_session+4E20h
            b"\xE8\xAB\xAB\xAB\xAB"      # 124 call sub_4513A0
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 129 mov ebx, [ebp+var_BD7C]
            b"\x33\xFF"                  # 135 xor edi, edi
            b"\x85\xDB"                  # 137 test ebx, ebx
            b"\x0F\x8E\x82\x00\x00\x00"  # 139 jle loc_A1E8FD
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 145 lea esi, [ebp+var_2BAB]
            b"\x57"                      # 151 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 152 lea eax, [ebp+var_C6F8]
            b"\x50"                      # 158 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 159 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 164 call CSession_sub_AD2AC0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 169 mov [ebp+var_4], 41h
            b"\x83\xBD\xAB\xAB\xAB\xAB\x00"  # 176 cmp [ebp+var_C6F0], 0
            b"\x74\x41"                  # 183 jz short loc_A1E8E4
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 185 cmp [ebp+var_C6B8], 10h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 192 lea eax, [ebp+var_C6CC]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 198 cmovnb eax, [ebp+var_C6CC]
            b"\x50"                      # 205 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 206 call atoi
            b"\x89\x46\xAB"              # 212 mov [esi-4], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 215 movzx eax, [ebp+var_C6E8]
            b"\x66\x89\x06"              # 222 mov [esi], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 225 mov eax, [ebp+var_C6E0]
            b"\x89\x46\xAB"              # 231 mov [esi+2], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 234 movzx eax, [ebp+var_C6E8]
            b"\x83\xC4\x04"              # 241 add esp, 4
            b"\x66\x89\x06"              # 244 mov [esi], ax
            b"\x83\xC6\xAB"              # 247 add esi, 0Ah
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 250 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 257 lea ecx, [ebp+var_C6F8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 263 call sub_45C3F0
            b"\x47"                      # 268 inc edi
            b"\x3B\xFB"                  # 269 cmp edi, ebx
            b"\x7C\x86"                  # 271 jl short loc_A1E881
            b"\x85\xDB"                  # 273 test ebx, ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 275 lea eax, [ebp+var_2C08]
            b"\x50"                      # 281 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 282 movsx eax, word ptr [ebp+var
            b"\x50"                      # 289 push eax
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 290 jnz loc_A1FED1
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 296 mov byte ptr [ebp+var_2C00],
            b"\xE8\xAB\xAB\xAB\xAB"      # 303 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 308 mov ecx, eax
            b"\xE8"                      # 310 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 310,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 304,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A7Fh
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_4F10],
            b"\x8D\x0C\xC5\xAB\xAB\xAB\x00"  # 12 lea ecx, ds:58h[eax*8]
            b"\x6A\xAB"                  # 19 push 4Fh
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 21 mov [ebp+var_BD7C], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 27 lea eax, [ebp+var_4F0C]
            b"\x53"                      # 33 push ebx
            b"\x50"                      # 34 push eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 35 mov word ptr [ebp+var_4F10+2]
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 42 call strncpy
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 48 mov ecx, dword ptr g_windowMgr+2D
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 57 mov [ebp+var_4EBD], 0
            b"\x85\xC9"                  # 64 test ecx, ecx
            b"\x74\x13"                  # 66 jz short loc_A1F6BD
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 68 lea eax, [ebp+var_4EBA]
            b"\x50"                      # 74 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 75 lea eax, [ebp+var_4EBC]
            b"\x50"                      # 81 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 82 call sub_9F65A0
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 87 mov ebx, [ebp+var_BD7C]
            b"\x33\xFF"                  # 93 xor edi, edi
            b"\x85\xDB"                  # 95 test ebx, ebx
            b"\x7E\x5B"                  # 97 jle short loc_A1F724
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 99 lea esi, [ebp+var_4EB4]
            b"\x90"                      # 105 no nop
            b"\x57"                      # 106 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 107 lea eax, [ebp+var_D150]
            b"\x50"                      # 113 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 114 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 119 call CSession_sub_AD2AC0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 124 mov [ebp+var_4], 5Ah
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 131 mov eax, [ebp+var_D148]
            b"\x85\xC0"                  # 137 test eax, eax
            b"\x74\x1A"                  # 139 jz short loc_A1F70D
            b"\x66\x89\x46\xAB"          # 141 mov [esi-4], ax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 145 mov ax, [ebp+var_D140]
            b"\x66\x89\x46\xAB"          # 152 mov [esi-2], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 156 mov eax, [ebp+var_D138]
            b"\x89\x06"                  # 162 mov [esi], eax
            b"\x83\xC6\x08"              # 164 add esi, 8
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 167 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAb\xAB\xAB"  # 174 lea ecx, [ebp+var_D150]
            b"\xE8\xAB\xAB\xAB\xAB"      # 180 call sub_45C3F0
            b"\x47"                      # 185 inc edi
            b"\x3B\xFB"                  # 186 cmp edi, ebx
            b"\x7C\xAC"                  # 188 jl short loc_A1F6D0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 190 lea eax, [ebp+var_4F10]
            b"\x50"                      # 196 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 197 movsx eax, word ptr [ebp+var
            b"\x50"                      # 204 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 205 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 210 mov ecx, eax
            b"\xE8"                      # 212 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 212,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 206,
        }
    ],
    # 2018-12-19
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A92h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_3710],
            b"\x8D\x0C\x80"              # 12 lea ecx, [eax+eax*4]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_BD7C], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 21 mov eax, [ebp+var_DF70+4]
            b"\x6A\xAB"                  # 27 push 4Fh
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 29 mov [ebp+var_370C], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 35 lea eax, [ebp+var_3704]
            b"\x8D\x0C\x4D\xAB\xAB\xAB\x00"  # 41 lea ecx, ds:60h[ecx*2]
            b"\x53"                      # 48 push ebx
            b"\x50"                      # 49 push eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 50 mov word ptr [ebp+var_3710+2]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_3708
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 67 call strncpy
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 73 mov ecx, dword ptr g_windowMgr+3C
            b"\x83\xC4\x0C"              # 79 add esp, 0Ch
            b"\xC6\x85\xAB\xAB\xAB\xAB\x00"  # 82 mov [ebp+var_36B5], 0
            b"\x85\xC9"                  # 89 test ecx, ecx
            b"\x74\x13"                  # 91 jz short loc_A1F7F3
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 93 lea eax, [ebp+var_36B2]
            b"\x50"                      # 99 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 100 lea eax, [ebp+var_36B4]
            b"\x50"                      # 106 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 107 call sub_9F65A0
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 112 cmp dword_10B7B94, 0
            b"\x75\x11"                  # 119 jnz short loc_A1F80D
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 121 lea eax, [ebp+var_3704]
            b"\x50"                      # 127 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 128 mov ecx, (offset g_session+4E20h
            b"\xE8\xAB\xAB\xAB\xAB"      # 133 call sub_4513A0
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 138 mov ebx, [ebp+var_BD7C]
            b"\x33\xFF"                  # 144 xor edi, edi
            b"\x85\xDB"                  # 146 test ebx, ebx
            b"\x0F\x8E\x76\x00\x00\x00"  # 148 jle loc_A1F893
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 154 lea esi, [ebp+var_36AA]
            b"\x57"                      # 160 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 161 lea eax, [ebp+var_C8B8]
            b"\x50"                      # 167 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 168 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 173 call CSession_sub_AD2AC0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 178 mov [ebp+var_4], 5Bh
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 185 cmp [ebp+var_C8B0], 0
            b"\x74\x37"                  # 192 jz short loc_A1F87C
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 194 cmp [ebp+var_C878], 10h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 201 lea eax, [ebp+var_C88C]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 207 cmovnb eax, [ebp+var_C88C]
            b"\x50"                      # 214 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 215 call atoi
            b"\x89\x46\xAB"              # 221 mov [esi-6], eax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 224 mov ax, [ebp+var_C8A8]
            b"\x66\x89\x46\xAB"          # 231 mov [esi-2], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 235 mov eax, [ebp+var_C8A0]
            b"\x83\xC4\x04"              # 241 add esp, 4
            b"\x89\x06"                  # 244 mov [esi], eax
            b"\x83\xC6\xAB"              # 246 add esi, 0Ah
            b"\xC7\x45\xAB\xFF\xFF\xFF\xFF"  # 249 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 256 lea ecx, [ebp+var_C8B8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 262 call sub_45C3F0
            b"\x47"                      # 267 inc edi
            b"\x3B\xFB"                  # 268 cmp edi, ebx
            b"\x7C\x90"                  # 270 jl short loc_A1F823
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 272 lea eax, [ebp+var_3710]
            b"\x50"                      # 278 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 279 movsx eax, word ptr [ebp+var
            b"\x50"                      # 286 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 287 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 292 mov ecx, eax
            b"\xE8"                      # 294 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 294,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 288,
        }
    ],
    # 2018-12-26
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0AAAh
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_E550],
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_E550+2]
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 19 mov eax, [ebp+var_E550]
            b"\x33\xDB"                  # 25 xor ebx, ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov [ebp+Dst], eax
            b"\x85\xD2"                  # 33 test edx, edx
            b"\x0F\x8E\xAB\xAB\xAB\x00"  # 35 jle loc_A16948
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 41 lea esi, [ebp+var_6800]
            b"\x8B\xFA"                  # 47 mov edi, edx
            b"\x53"                      # 49 push ebx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 50 lea eax, [ebp+var_D1D8]
            b"\x50"                      # 56 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 57 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CSession_sub_AD2750
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 8
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 74 mov eax, [ebp+var_D1C8]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 80 cmp [ebp+var_D198], 10h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 87 mov [ebp+var_C270], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 93 lea eax, [ebp+var_D1AC]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 99 cmovnb eax, [ebp+var_D1AC]
            b"\x50"                      # 106 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 107 call ds:atoi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 113 mov dword ptr [ebp+var_C278+4],
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 119 movq xmm0, [ebp+var_C278
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 127 mov ax, [ebp+var_D1D0]
            b"\x66\x0F\xD6\x06"          # 134 movq qword ptr [esi], xmm0
            b"\x83\xC4\x04"              # 138 add esp, 4
            b"\x66\x89\x46\xAB"          # 141 mov [esi+8], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 145 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 152 lea ecx, [ebp+var_D1D8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 158 call sub_459C20
            b"\x43"                      # 163 inc ebx
            b"\x8D\x76\xAB"              # 164 lea esi, [esi+0Ah]
            b"\x3B\xDF"                  # 167 cmp ebx, edi
            b"\x7C\x86"                  # 169 jl short loc_A16878
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 171 mov edi, [ebp+var_E540]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 177 cmp dword ptr [edi+254h], 0
            b"\x75\x1B"                  # 184 jnz short loc_A1691C
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 186 lea eax, [ebp+Dst]
            b"\x50"                      # 192 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 193 mov eax, [ebp+var_E54C+4]
            b"\x98"                      # 199 cwd cwde
            b"\x50"                      # 200 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 201 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 206 mov ecx, eax
            b"\xE8"                      # 208 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 208,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 202,
        }
    ],
    # 2018-12-26
    [
        (
            b"\xBA\xAB\xAB\xAB\x00"      # 0  mov edx, 9D6h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_E550],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_E550+2]
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 19 mov ecx, [ebp+var_E550]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_7404], ecx
            b"\x85\xC0"                  # 31 test eax, eax
            b"\x0F\x8E\xAB\xAB\xAB\xAB"  # 33 jle loc_A16ABC
            b"\x8D\x9D\xAB\xAB\xAB\xAB"  # 39 lea ebx, [ebp+var_7400]
            b"\x33\xFF"                  # 45 xor edi, edi
            b"\x57"                      # 47 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 48 lea eax, [ebp+var_D398]
            b"\x50"                      # 54 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 55 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call CSession_sub_AD2750
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 65 mov [ebp+var_4], 9
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 72 cmp [ebp+var_D358], 10h
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 79 mov esi, [ebp+var_D388]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+var_D36C]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 91 cmovnb eax, [ebp+var_D36C]
            b"\x50"                      # 98 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 99 call ds:atoi
            b"\x89\x03"                  # 105 mov [ebx], eax
            b"\x83\xC4\x04"              # 107 add esp, 4
            b"\x89\x73\xAB"              # 110 mov [ebx+4], esi
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 113 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 120 lea ecx, [ebp+var_D398]
            b"\xE8\xAB\xAB\xAB\xAB"      # 126 call sub_459C20
            b"\x47"                      # 131 inc edi
            b"\x8D\x5B\xAB"              # 132 lea ebx, [ebx+8]
            b"\x3B\xBD\xAB\xAB\xAB\xAB"  # 135 cmp edi, [ebp+var_C270]
            b"\x7C\xA0"                  # 141 jl short loc_A16A07
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 143 mov edi, [ebp+var_E540]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 149 cmp dword ptr [edi+254h], 0
            b"\x75\x1B"                  # 156 jnz short loc_A16A91
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 158 lea eax, [ebp+var_7404]
            b"\x50"                      # 164 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 165 mov eax, [ebp+var_E54C+4]
            b"\x98"                      # 171 cwd cwde
            b"\x50"                      # 172 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 173 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 178 mov ecx, eax
            b"\xE8"                      # 180 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 180,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 174,
        }
    ],
    # 2018-12-26
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0B0Fh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+Src], ax
            b"\x6A\xAB"                  # 12 push 4
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+Src]
            b"\x50"                      # 20 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 21 lea eax, [ebp+var_6004]
            b"\x68\xAB\xAB\xAB\xAB"      # 27 push 800h
            b"\x50"                      # 32 push eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 33 mov [ebp+var_E5F6], cx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 40 call ds:memcpy_s
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 46 mov ecx, g_CBarterMarketItemMgr
            b"\x83\xC4\x10"              # 52 add esp, 10h
            b"\xE8\xAB\xAB\xAB\xAB"      # 55 call CBarterMarketItemMgr_getList
            b"\x8B\x08"                  # 60 mov ecx, [eax]
            b"\x8B\x31"                  # 62 mov esi, [ecx]
            b"\x3B\xF1"                  # 64 cmp esi, ecx
            b"\x0F\x84\xB5\x00\x00\x00"  # 66 jz loc_A16C45
            b"\x8D\x9D\xAB\xAB\xAB\xAB"  # 72 lea ebx, [ebp+var_6000]
            b"\x8B\xF8"                  # 78 mov edi, eax
            b"\xEB\x06"                  # 80 jmp short loc_A16BA0
            b"\xAB\xAB\xAB\xAB\xAB\xAB"  # 82 align 10h
            b"\x8D\x46\xAB"              # 88 lea eax, [esi+8]
            b"\x50"                      # 91 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 92 lea ecx, [ebp+var_D77C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 98 call sub_463950
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 103 mov [ebp+var_4], 0Ah
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 110 mov eax, [ebp+var_D76C]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 116 lea ecx, [ebp+var_D750]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 122 mov dword ptr [ebp+var_C278+4],
            b"\xE8\xAB\xAB\xAB\xAB"      # 128 call sub_44B2C0
            b"\x50"                      # 133 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 134 call ds:atoi
            b"\x83\xC4\x04"              # 140 add esp, 4
            b"\xB9\xAB\xAB\xAB\xAB"      # 143 mov ecx, offset g_session
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 148 push [ebp+var_D698]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 154 mov dword ptr [ebp+var_C278], ea
            b"\xE8\xAB\xAB\xAB\xAB"      # 160 call CSession_sub_ACF3E0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 165 movq xmm0, [ebp+var_C278
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 173 mov word ptr [ebp+var_C270],
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 180 mov eax, [ebp+var_D68C]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 186 mov [ebp+var_C270+2], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 192 mov eax, [ebp+var_C270]
            b"\x66\x0F\xD6\x03"          # 198 movq qword ptr [ebx], xmm0
            b"\x89\x43\xAB"              # 202 mov [ebx+8], eax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 205 mov ax, [ebp+var_C26C]
            b"\x66\x89\x43\xAB"          # 212 mov [ebx+0Ch], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 216 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 223 lea ecx, [ebp+var_D77C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 229 call sub_463FC0
            b"\x8B\x36"                  # 234 mov esi, [esi]
            b"\x8D\x5B\xAB"              # 236 lea ebx, [ebx+0Eh]
            b"\x3B\x37"                  # 239 cmp esi, [edi]
            b"\x0F\x85\x61\xFF\xFF\xFF"  # 241 jnz loc_A16BA0
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 247 mov edi, [ebp+var_E540]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 253 cmp [ebp+var_E550], 0
            b"\x0F\x8E\xAB\xAB\xAB\xAB"  # 260 jle loc_A17BE6
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 266 cmp dword ptr [edi+254h], 0
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 273 jnz loc_A17BE6
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 279 lea eax, [ebp+var_6004]
            b"\x50"                      # 285 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 286 movsx eax, [ebp+var_E5F6]
            b"\x50"                      # 293 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 294 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 299 mov ecx, eax
            b"\xE8"                      # 301 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 301,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 295,
        }
    ],
    # 2019-01-09
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 819h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_DF7C],
            b"\x8D\x0C\xC5\xAB\xAB\xAB\xAB"  # 12 lea ecx, ds:0Ch[eax*8]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_BD7C], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var_DF7C+2]
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_DF7C+4], ebx
            b"\x81\xF9\xAB\xAB\xAB\xAB"  # 38 cmp ecx, 800h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 44 ja loc_A19014
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 50 mov ebx, [ebp+var_BD7C]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 56 movq xmm0, qword ptr [ebp
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 64 mov eax, [ebp+var_DF70+4]
            b"\x33\xF6"                  # 70 xor esi, esi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 72 movq qword ptr [ebp+var_B
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 80 mov [ebp+var_B708], eax
            b"\x85\xDB"                  # 86 test ebx, ebx
            b"\x0F\x8E\x83\x00\x00\x00"  # 88 jle loc_A1EE8E
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 94 lea edi, [ebp+var_B704]
            b"\x56"                      # 100 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 101 lea eax, [ebp+var_D360]
            b"\x50"                      # 107 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 108 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 113 call CSession_sub_AD3B50
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 118 mov [ebp+var_4], 42h
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 125 mov eax, [ebp+var_D358]
            b"\x85\xC0"                  # 131 test eax, eax
            b"\x74\x37"                  # 133 jz short loc_A1EE6B
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 135 lea ecx, [ebp+var_D360]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 141 mov word ptr [ebp+var_BD84+4
            b"\xE8\xAB\xAB\xAB\xAB"      # 148 call sub_50F7E0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 153 mov dword ptr [ebp+var_BD84+6],
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 159 mov ax, [ebp+var_D350]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 166 mov word ptr [ebp+var_BD7C+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 173 mov eax, dword ptr [ebp+var_BD84
            b"\x89\x07"                  # 179 mov [edi], eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 181 mov eax, [ebp+var_BD7C]
            b"\x89\x47\xAB"              # 187 mov [edi+4], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 190 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 197 lea ecx, [ebp+var_D360]
            b"\xE8\xAB\xAB\xAB\xAB"      # 203 call sub_45C3F0
            b"\x46"                      # 208 inc esi
            b"\x83\xC7\xAB"              # 209 add edi, 8
            b"\x3B\xF3"                  # 212 cmp esi, ebx
            b"\x7C\x8C"                  # 214 jl short loc_A1EE11
            b"\x8D\x0C\xDD\xAB\xAB\xAB\xAB"  # 216 lea ecx, ds:0Ch[ebx*8]
            b"\x85\xDB"                  # 223 test ebx, ebx
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 225 jz loc_A19014
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 231 lea eax, [ebp+var_B710]
            b"\x50"                      # 237 push eax
            b"\x0F\xBF\xC1"              # 238 movsx eax, cx
            b"\x50"                      # 241 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 242 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 247 mov ecx, eax
            b"\xE8"                      # 249 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 249,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 243,
        }
    ],
    # 2019-01-09
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 0  mov dword ptr [ebp+buf], 0B11
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+buf+2], ax
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+buf]
            b"\x50"                      # 14 push eax
            b"\x6A\xAB"                  # 15 push 4
            b"\xC7\x86\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 17 mov dword ptr [es
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 34,
            "packetId": 3,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 368h
            b"\x89\x5D\xAB"              # 5  mov [ebp+Src+2], ebx
            b"\x66\x89\x45\xAB"          # 8  mov word ptr [ebp+Src], ax
            b"\x8D\x45\xAB"              # 12 lea eax, [ebp+Src]
            b"\x50"                      # 15 push eax
            b"\x6A\xAB"                  # 16 push 6
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A16h
            b"\x0F\x57\xC0"              # 5  xorps xmm0, xmm0
            b"\x6A\xFF"                  # 8  push 0FFFFFFFFh
            b"\x66\x89\x45\xAB"          # 10 mov [ebp+Src], ax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+var_1E]
            b"\x6A\xAB"                  # 17 push 18h
            b"\x50"                      # 19 push eax
            b"\x66\x0F\x13\x45\xAB"      # 20 movlpd [ebp+var_1E], xmm0
            b"\x66\x0F\x13\x45\xAB"      # 25 movlpd [ebp+var_16], xmm0
            b"\x66\x0F\x13\x45\xAB"      # 30 movlpd [ebp+var_E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call sub_48CE90
            b"\x83\xC4\x10"              # 40 add esp, 10h
            b"\x8D\x45\xAB"              # 43 lea eax, [ebp+Src]
            b"\x50"                      # 46 push eax
            b"\x6A\xAB"                  # 47 push 1Ah
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 9FBh
            b"\x8B\xB0\xAB\xAB\xAB\x00"  # 5  mov esi, [eax+0B0h]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 11 lea eax, [ebp+Dst]
            b"\xD1\xFB"                  # 17 sar ebx, 1
            b"\x68\xAB\xAB\xAB\x00"      # 19 push 0FCh
            b"\x6A\x00"                  # 24 push 0
            b"\x50"                      # 26 push eax
            b"\x8D\x3C\x5D\xAB\x00\x00\x00"  # 27 lea edi, ds:8[ebx*2]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 34 mov word ptr [ebp+var_214], c
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 41 mov word ptr [ebp+var_214+2],
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 53 mov eax, [ebp+var_214]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 59 mov [ebp+var_114], eax
            b"\x8D\x04\xAB"              # 65 lea eax, [ebx+ebx]
            b"\x50"                      # 68 push eax
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 69 mov [ebp+var_110], esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 75 lea eax, [ebp+Dst]
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 81 mov esi, [ebp+Src]
            b"\x56"                      # 87 push esi
            b"\x50"                      # 88 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 89 call memcpy
            b"\x83\xC4\x18"              # 94 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 97 lea eax, [ebp+var_114]
            b"\x50"                      # 103 push eax
            b"\x0F\xBF\xC7"              # 104 movsx eax, di
            b"\x50"                      # 107 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 108 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 113 mov ecx, eax
            b"\xE8"                      # 115 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 115,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 109,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A77h
            b"\xC7\x45\xAB\x00\x00\x00\x00"  # 5  mov dword ptr [ebp+anonymous_
            b"\x66\x89\x45\xAB"          # 12 mov [ebp-24h], ax
            b"\x0F\x57\xC0"              # 16 xorps xmm0, xmm0
            b"\x8D\x45\xAB"              # 19 lea eax, [ebp-24h]
            b"\x66\x0F\x13\x45\xAB"      # 22 movlpd [ebp+anonymous_0], xmm0
            b"\x50"                      # 27 push eax
            b"\x6A\xAB"                  # 28 push 0Fh
            b"\xC6\x45\xAB\xAB"          # 30 mov byte ptr [ebp-22h], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 34 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 39 mov ecx, eax
            b"\xE8"                      # 41 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 41,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 35,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0A77h
            b"\xC6\x45\xAB\xAB"          # 5  mov byte ptr [ebp-22h], 0
            b"\x66\x89\x4D\xAB"          # 9  mov [ebp-24h], cx
            b"\x8B\xCA"                  # 13 mov ecx, edx
            b"\xF3\x0F\x10\x40\xAB"      # 15 movss xmm0, dword ptr [eax+34h]
            b"\xF3\x0F\x11\x45\xAB"      # 20 movss dword ptr [ebp+anonymous_0]
            b"\xF3\x0F\x10\x40\xAB"      # 25 movss xmm0, dword ptr [eax+30h]
            b"\xF3\x0F\x11\x45\xAB"      # 30 movss dword ptr [ebp+anonymous_0+
            b"\xF3\x0F\x10\x40\xAB"      # 35 movss xmm0, dword ptr [eax+2Ch]
            b"\x6A\x00"                  # 40 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 42 push offset aRange
            b"\xF3\x0F\x11\x45\xAB"      # 47 movss dword ptr [ebp+anonymous_0+
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call sub_48B480
            b"\x83\xF8\xAB"              # 57 cmp eax, 0FFFFFFFFh
            b"\x74\xAB"                  # 60 jz short loc_9BC7E8
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 62 mov eax, [ebp+String]
            b"\x83\xC0\xAB"              # 68 add eax, 18h
            b"\x83\x78\xAB\xAB"          # 71 cmp dword ptr [eax+14h], 10h
            b"\x72\x02"                  # 75 jb short loc_9BC7C4
            b"\x8B\x00"                  # 77 mov eax, [eax]
            b"\x50"                      # 79 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 80 call ds:atof
            b"\x83\xC4\x04"              # 86 add esp, 4
            b"\x8D\x45\xAB"              # 89 lea eax, [ebp-24h]
            b"\xD9\x5D\xAB"              # 92 fstp dword ptr [ebp+anonymous_0]
            b"\x50"                      # 95 push eax
            b"\x6A\xAB"                  # 96 push 0Fh
            b"\xE8\xAB\xAB\xAB\xAB"      # 98 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 103 mov ecx, eax
            b"\xE8"                      # 105 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 105,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 99,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 1D5h
            b"\x66\x89\x4E\xAB"          # 5  mov [esi+2], cx
            b"\x83\xC1\xAB"              # 9  add ecx, 0FFFFFFF8h
            b"\x66\x89\x06"              # 12 mov [esi], ax
            b"\x8D\x46\xAB"              # 15 lea eax, [esi+8]
            b"\x85\xFF"                  # 18 test edi, edi
            b"\x74\xAB"                  # 20 jz short loc_9C4945
            b"\x85\xC0"                  # 22 test eax, eax
            b"\x75\xAB"                  # 24 jnz short loc_9C4910
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 26 call ds:__imp__errno
            b"\xC7\x00\xAB\xAB\xAB\x00"  # 32 mov dword ptr [eax], 16h
            b"\xEB\xAB"                  # 38 jmp short loc_9C493F
            b"\x3B\xCF"                  # 40 cmp ecx, edi
            b"\x72\xAB"                  # 42 jb short loc_9C4927
            b"\x57"                      # 44 push edi
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 45 lea ecx, [ebp+Dst]
            b"\x51"                      # 51 push ecx
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call memcpy
            b"\x83\xC4\x0C"              # 58 add esp, 0Ch
            b"\xEB\xAB"                  # 61 jmp short loc_9C4945
            b"\x51"                      # 63 push ecx
            b"\x6A\x00"                  # 64 push 0
            b"\x50"                      # 66 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 67 call memset
            b"\x83\xC4\x0C"              # 72 add esp, 0Ch
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 75 call ds:__imp__errno
            b"\xC7\x00\xAB\xAB\xAB\x00"  # 81 mov dword ptr [eax], 22h
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 87 call ds:__imp__invalid_parameter_
            b"\x0F\xB7\x46\xAB"          # 93 movzx eax, word ptr [esi+2]
            b"\x56"                      # 97 push esi
            b"\x50"                      # 98 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 99 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 104 mov ecx, eax
            b"\xE8"                      # 106 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 106,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 100,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 19Ch
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0E9B8h], ax
            b"\x8D\x77\xAB"              # 12 lea esi, [edi+4]
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 15 mov [ebp-0E9B6h], si
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 22 mov eax, [ebp-0E9B8h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 28 mov [ebp-5F10h], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 34 lea eax, [ebp+var_E54]
            b"\x57"                      # 40 push edi
            b"\x50"                      # 41 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 42 lea eax, [ebp-5F0Ch]
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call memcpy
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp-5F10h]
            b"\x50"                      # 63 push eax
            b"\x0F\xB7\xC6"              # 64 movzx eax, si
            b"\x50"                      # 67 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 73 mov ecx, eax
            b"\xE8"                      # 75 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 75,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 69,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 2DBh
            b"\x8D\x77\xAB"              # 5  lea esi, [edi+5]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 8  mov [ebp-0E9B8h], ax
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 15 mov [ebp-0E9B6h], si
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 22 mov eax, [ebp-0E9B8h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 28 mov [ebp-5B10h], eax
            b"\x8D\x47\xAB"              # 34 lea eax, [edi+1]
            b"\x50"                      # 37 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_11BC]
            b"\x50"                      # 44 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 45 lea eax, [ebp-5B0Ch]
            b"\x50"                      # 51 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call memcpy
            b"\x83\xC4\x0C"              # 57 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 60 lea eax, [ebp-5B10h]
            b"\x50"                      # 66 push eax
            b"\x0F\xB7\xC6"              # 67 movzx eax, si
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov dword ptr [eb
            b"\x50"                      # 10 push eax
            b"\x6A\x04"                  # 11 push 4
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 20,
            "packetId": (6, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 153h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0E9B8h], ax
            b"\x83\xC6\xAB"              # 12 add esi, 4
            b"\x51"                      # 15 push ecx
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 16 mov [ebp-0E9B6h], si
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 23 mov eax, [ebp-0E9B8h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 29 mov [ebp-0AF10h], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 35 lea eax, [ebp-0AF0Ch]
            b"\x52"                      # 41 push edx
            b"\x50"                      # 42 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call memcpy
            b"\x83\xC4\x0C"              # 48 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp-0AF10h]
            b"\x50"                      # 57 push eax
            b"\x0F\xBF\xC6"              # 58 movsx eax, si
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 9CEh
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push offset aS
            b"\x6A\xFF"                  # 10 push 0FFFFFFFFh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp-0D19Ch], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 19 lea eax, [ebp-0D19Ah]
            b"\x6A\xAB"                  # 25 push 64h
            b"\x50"                      # 27 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call sub_48CE90
            b"\x83\xC4\x20"              # 33 add esp, 20h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp-0D19Ch]
            b"\x50"                      # 42 push eax
            b"\x6A\xAB"                  # 43 push 66h
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 98Dh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0E9B8h], ax
            b"\x8D\x46\xAB"              # 12 lea eax, [esi+5]
            b"\x0F\xB7\xF0"              # 15 movzx esi, ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 18 lea eax, [ebp-530Ch]
            b"\x68\xAB\xAB\xAB\x00"      # 24 push 3FCh
            b"\x6A\x00"                  # 29 push 0
            b"\x50"                      # 31 push eax
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 32 mov [ebp-0E9B6h], si
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call memset
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 44 mov eax, [ebp-0E9B8h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 50 mov [ebp-5310h], eax
            b"\x8D\x47\xAB"              # 56 lea eax, [edi+1]
            b"\x50"                      # 59 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 60 lea eax, [ebp+var_10C4]
            b"\x50"                      # 66 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 67 lea eax, [ebp-530Ch]
            b"\x50"                      # 73 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 74 call memcpy
            b"\x83\xC4\x18"              # 79 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 82 lea eax, [ebp-5310h]
            b"\x50"                      # 88 push eax
            b"\x0F\xBF\xC6"              # 89 movsx eax, si
            b"\x50"                      # 92 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 93 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 98 mov ecx, eax
            b"\xE8"                      # 100 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 100,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 94,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xBB\xAB\xAB\xAB\x00"      # 0  mov ebx, 9A1h
            b"\x8D\x45\xAB"              # 5  lea eax, [ebp+Src]
            b"\x66\x89\x5D\xAB"          # 8  mov word ptr [ebp+Src], bx
            b"\x50"                      # 12 push eax
            b"\x6A\xAB"                  # 13 push 2
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0ACFh
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_6F6]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_6F8], ax
            b"\x8D\x87\xAB\xAB\xAB\x00"  # 22 lea eax, [edi+178h]
            b"\x50"                      # 28 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 29 push offset aS
            b"\x6A\xAB"                  # 34 push 0FFFFFFFFh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp+var_6F2]
            b"\x6A\xAB"                  # 42 push 19h
            b"\x50"                      # 44 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call sub_48CE90
            b"\x8D\x87\xAB\xAB\xAB\x00"  # 50 lea eax, [edi+138h]
            b"\xC6\x45\xAB\xAB"          # 56 mov [ebp+var_54], 0
            b"\x50"                      # 60 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 61 push offset aS
            b"\x6A\xAB"                  # 66 push 0FFFFFFFFh
            b"\x0F\x57\xC0"              # 68 xorps xmm0, xmm0
            b"\x8D\x45\xAB"              # 71 lea eax, [ebp+var_74]
            b"\x6A\xAB"                  # 74 push 21h
            b"\x50"                      # 76 push eax
            b"\x0F\x11\x45\xAB"          # 77 movups [ebp+var_74], xmm0
            b"\x0F\x11\x45\xAB"          # 81 movups [ebp+var_64], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 85 call sub_48CE90
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 90 lea eax, [ebp+var_6D9]
            b"\x50"                      # 96 push eax
            b"\x8D\x45\xAB"              # 97 lea eax, [ebp+var_74]
            b"\x50"                      # 100 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 101 call sub_4AAE40
            b"\x8D\x87\xAB\xAB\xAB\x00"  # 106 lea eax, [edi+1D0h]
            b"\x50"                      # 112 push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 113 push offset aS
            b"\x6A\xAB"                  # 118 push 0FFFFFFFFh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 120 lea eax, [ebp+var_6B9]
            b"\x6A\xAB"                  # 126 push 5
            b"\x50"                      # 128 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 129 call sub_48CE90
            b"\x83\xC4\x50"              # 134 add esp, 50h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 137 lea eax, [ebp+var_6F8]
            b"\x50"                      # 143 push eax
            b"\x6A\xAB"                  # 144 push 44h
            b"\xE8\xAB\xAB\xAB\xAB"      # 146 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 151 mov ecx, eax
            b"\xE8"                      # 153 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 153,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 147,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9A1h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 5  lea eax, [ebp+var_7E4]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_7E4], di
            b"\x50"                      # 18 push eax
            b"\x6A\xAB"                  # 19 push 2
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 9A3h
            b"\x66\x89\x06"              # 5  mov [esi], ax
            b"\x8B\x83\xAB\xAB\xAB\x00"  # 8  mov eax, [ebx+69D8h]
            b"\x89\x46\xAB"              # 14 mov [esi+4], eax
            b"\x8D\x83\xAB\xAB\xAB\x00"  # 17 lea eax, [ebx+1B8h]
            b"\x50"                      # 23 push eax
            b"\x6A\xAB"                  # 24 push 0FFFFFFFFh
            b"\x8D\x46\xAB"              # 26 lea eax, [esi+8]
            b"\x6A\xAB"                  # 29 push 0Ah
            b"\x50"                      # 31 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call sub_48CE90
            b"\x83\xC4\x10"              # 37 add esp, 10h
            b"\xB8\xAB\xAB\xAB\x00"      # 40 mov eax, 0Eh
            b"\x66\x89\x46\xAB"          # 45 mov [esi+2], ax
            b"\x56"                      # 49 push esi
            b"\x50"                      # 50 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 51 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 56 mov ecx, eax
            b"\xE8"                      # 58 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 58,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 52,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x8D\x45\xAB"              # 0  lea eax, [ebp+Src]
            b"\xC7\x83\xAB\xAB\xAB\x00\xAB\xAB\xAB\xAB"  # 3  mov dword ptr [eb
            b"\x50"                      # 13 push eax
            b"\x6A\xAB"                  # 14 push 4
            b"\xC7\x45\xF8\xAB\xAB\xAB\xAB"  # 16 mov [ebp+Src], 7F40B11h
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 30,
            "packetId": (19, 2),
            "retOffset": 16,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x8D\x45\xAB"              # 0  lea eax, [ebp+Src]
            b"\xC7\x45\xC4\xAB\xAB\xAB\xAB"  # 3  mov [ebp+Src], 7F40B11h
            b"\x50"                      # 10 push eax
            b"\x6A\xAB"                  # 11 push 4
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 20,
            "packetId": (6, 2),
            "retOffset": 3,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A77h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\x00"  # 5  mov dword ptr [eb
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_C68], ax
            b"\x0F\x57\xC0"              # 22 xorps xmm0, xmm0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp+var_C68]
            b"\x66\x0F\x13\x85\xAB\xAB\xAB\xAB"  # 31 movlpd qword ptr [ebp+var
            b"\x50"                      # 39 push eax
            b"\x6A\x0F"                  # 40 push 0Fh
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 42 mov [ebp+var_C66], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0A77h
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_C66], 0
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_C68], cx
            b"\x8B\xCA"                  # 19 mov ecx, edx
            b"\xF3\x0F\x10\x40\xAB"      # 21 movss xmm0, dword ptr [eax+34h]
            b"\xF3\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 26 movss dword ptr [ebp+var_
            b"\xF3\x0F\x10\x40\xAB"      # 34 movss xmm0, dword ptr [eax+30h]
            b"\xF3\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 39 movss dword ptr [ebp+var_
            b"\xF3\x0F\x10\x40\xAB"      # 47 movss xmm0, dword ptr [eax+2Ch]
            b"\x6A\xAB"                  # 52 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 54 push offset aRange
            b"\xF3\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 59 movss dword ptr [ebp+var_
            b"\xE8\xAB\xAB\xAB\xAB"      # 67 call sub_48B790
            b"\x83\xF8\xAB"              # 72 cmp eax, 0FFFFFFFFh
            b"\x74\xAB"                  # 75 jz short loc_8CA6B7
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 77 mov eax, dword ptr [ebp+Src+2]
            b"\x83\xC0\x18"              # 83 add eax, 18h
            b"\x83\x78\xAB\xAB"          # 86 cmp dword ptr [eax+14h], 10h
            b"\x72\x02"                  # 90 jb short loc_8CA68D
            b"\x8B\x00"                  # 92 mov eax, [eax]
            b"\x50"                      # 94 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 95 call ds:atof
            b"\x83\xC4\x04"              # 101 add esp, 4
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 104 lea eax, [ebp+var_C68]
            b"\xD9\x9D\xAB\xAB\xAB\xAB"  # 110 fstp dword ptr [ebp+var_C65]
            b"\x50"                      # 116 push eax
            b"\x6A\xAB"                  # 117 push 0Fh
            b"\xE8\xAB\xAB\xAB\xAB"      # 119 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 124 mov ecx, eax
            b"\xE8"                      # 126 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 126,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 120,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0F3h
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 5  lea ecx, [ebp-1A04h]
            b"\x66\x89\x07"              # 11 mov [edi], ax
            b"\x8D\x46\xAB"              # 14 lea eax, [esi+4]
            b"\x56"                      # 17 push esi
            b"\x66\x89\x47\xAB"          # 18 mov [edi+2], ax
            b"\x83\xC0\xAB"              # 22 add eax, 0FFFFFFFCh
            b"\x51"                      # 25 push ecx
            b"\x50"                      # 26 push eax
            b"\x8D\x47\xAB"              # 27 lea eax, [edi+4]
            b"\x50"                      # 30 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 31 call sub_8DFF40
            b"\x83\xC4\x10"              # 36 add esp, 10h
            b"\x0F\xB7\x47\xAB"          # 39 movzx eax, word ptr [edi+2]
            b"\x57"                      # 43 push edi
            b"\x50"                      # 44 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A54h
            b"\x66\x89\x4F\xAB"          # 5  mov [edi+2], cx
            b"\x8D\x4B\xAB"              # 9  lea ecx, [ebx+28h]
            b"\x51"                      # 12 push ecx
            b"\x66\x89\x07"              # 13 mov [edi], ax
            b"\x8D\x47\xAB"              # 16 lea eax, [edi+4]
            b"\x6A\xAB"                  # 19 push 4
            b"\x50"                      # 21 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call copy_buffer_sub_4AE100
            b"\x0F\xBF\x47\xAB"          # 27 movsx eax, word ptr [edi+2]
            b"\x50"                      # 31 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call new3
            b"\x0F\xBF\x4F\xAB"          # 37 movsx ecx, word ptr [edi+2]
            b"\x8B\xF0"                  # 41 mov esi, eax
            b"\x6A\xAB"                  # 43 push 8
            b"\x57"                      # 45 push edi
            b"\x51"                      # 46 push ecx
            b"\x56"                      # 47 push esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call copy_buffer_sub_4AE100
            b"\xFF\x75\xAB"              # 53 push [ebp+Size]
            b"\x8B\x4B\xAB"              # 56 mov ecx, [ebx+24h]
            b"\x03\x4B\xAB"              # 59 add ecx, [ebx+2Ch]
            b"\x0F\xBF\x47\xAB"          # 62 movsx eax, word ptr [edi+2]
            b"\x51"                      # 66 push ecx
            b"\x50"                      # 67 push eax
            b"\x8D\x46\xAB"              # 68 lea eax, [esi+8]
            b"\x50"                      # 71 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call copy_buffer_sub_4AE100
            b"\x0F\xBF\x47\xAB"          # 77 movsx eax, word ptr [edi+2]
            b"\x83\xC4\x38"              # 81 add esp, 38h
            b"\x56"                      # 84 push esi
            b"\x50"                      # 85 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 86 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 91 mov ecx, eax
            b"\xE8"                      # 93 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 93,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 87,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 0  mov [ebp+Src], 96Eh
            b"\x50"                      # 7  push eax
            b"\x8D\x45\xAB"              # 8  lea eax, [ebp+Src]
            b"\x50"                      # 11 push eax
            b"\xFF\x75\xAB"              # 12 push [ebp+var_1C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call sub_528640
            b"\xFF\x75\xAB"              # 20 push [ebp+var_14]
            b"\x8D\x04\xAB"              # 23 lea eax, [esi+ebx*2]
            b"\x50"                      # 26 push eax
            b"\x56"                      # 27 push esi
            b"\xFF\x75\xAB"              # 28 push [ebp+var_1C]
            b"\x8D\x4D\xAB"              # 31 lea ecx, [ebp+var_20]
            b"\xE8\xAB\xAB\xAB\xAB"      # 34 call sub_528640
            b"\x8B\x4D\xAB"              # 39 mov ecx, [ebp+var_20]
            b"\x8B\x45\xAB"              # 42 mov eax, [ebp+var_1C]
            b"\x2B\xC1"                  # 45 sub eax, ecx
            b"\x51"                      # 47 push ecx
            b"\x66\x89\x41\xAB"          # 48 mov [ecx+2], ax
            b"\x8B\x45\xAB"              # 52 mov eax, [ebp+var_1C]
            b"\x2B\x45\xAB"              # 55 sub eax, [ebp+var_20]
            b"\x50"                      # 58 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 66,
            "packetId": 3,
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 808h
            b"\xC7\x45\xAB\x00\x00\x00\x00"  # 5  mov [ebp+var_A], 0
            b"\x33\xD2"                  # 12 xor edx, edx
            b"\x66\x89\x45\xAB"          # 14 mov [ebp+Src], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_12], 0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_12+4],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_A], 0FFFFFFFFh
            b"\x8B\x06"                  # 39 mov eax, [esi]
            b"\x89\x55\xAB"              # 41 mov [ebp+var_18], edx
            b"\x89\x45\xAB"              # 44 mov [ebp+var_1C], eax
            b"\x3B\xC6"                  # 47 cmp eax, esi
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 49 jz loc_64CCBF
            b"\x57"                      # 55 push edi
            b"\x8D\x7D\xAB"              # 56 lea edi, [ebp+var_12]
            b"\x81\xC3\xAB\xAB\xAB\x00"  # 59 add ebx, 98h
            b"\x8B\x0B"                  # 65 mov ecx, [ebx]
            b"\x85\xC9"                  # 67 test ecx, ecx
            b"\x74\x17"                  # 69 jz short loc_64CC53
            b"\x83\x79\xAB\x00"          # 71 cmp dword ptr [ecx+30h], 0
            b"\x74\x11"                  # 75 jz short loc_64CC53
            b"\x66\x8B\x48\xAB"          # 77 mov cx, [eax+10h]
            b"\x42"                      # 81 inc edx
            b"\x66\x89\x0F"              # 82 mov [edi], cx
            b"\xC6\x40\xAB\xAB"          # 85 mov byte ptr [eax+14h], 1
            b"\x89\x55\xAB"              # 89 mov [ebp+var_18], edx
            b"\xEB\x0A"                  # 92 jmp short loc_64CC5D
            b"\x83\xC9\xAB"              # 94 or ecx, 0FFFFFFFFh
            b"\x66\x89\x0F"              # 97 mov [edi], cx
            b"\xC6\x40\xAB\xAB"          # 100 mov byte ptr [eax+14h], 0
            b"\x8D\x4D\xAB"              # 104 lea ecx, [ebp+var_1C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 107 call sub_62A3F0
            b"\x8B\x45\xAB"              # 112 mov eax, [ebp+var_1C]
            b"\x83\xC3\xAB"              # 115 add ebx, 4
            b"\x8B\x55\xAB"              # 118 mov edx, [ebp+var_18]
            b"\x83\xC7\xAB"              # 121 add edi, 2
            b"\x3B\xC6"                  # 124 cmp eax, esi
            b"\x75\xC1"                  # 126 jnz short loc_64CC36
            b"\x5F"                      # 128 pop edi
            b"\x85\xD2"                  # 129 test edx, edx
            b"\x74\xAB"                  # 131 jz short loc_64CCBF
            b"\x8D\x45\xAB"              # 133 lea eax, [ebp+Src]
            b"\x50"                      # 136 push eax
            b"\x0F\xBF\x45\xAB"          # 137 movsx eax, [ebp+Src]
            b"\x50"                      # 141 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 142 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 147 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 149 call CRagConnection_GetPacketSiz
            b"\x50"                      # 154 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 155 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 160 mov ecx, eax
            b"\xE8"                      # 162 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 162,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 156,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0C9h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+Src+2], dx
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+Src], ax
            b"\x33\xF6"                  # 19 xor esi, esi
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 21 mov eax, [ebp+Src]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+buf], eax
            b"\x85\xC9"                  # 33 test ecx, ecx
            b"\x7E\x72"                  # 35 jle short loc_8D7C9D
            b"\x8B\xF9"                  # 37 mov edi, ecx
            b"\x0F\x1F\xAB"              # 39 nop dword ptr [eax]
            b"\x56"                      # 42 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 43 lea eax, [ebp+var_DC50]
            b"\xB9\xAB\xAB\xAB\xAB"      # 49 mov ecx, offset g_session
            b"\x50"                      # 54 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 55 call CSession_sub_983D90
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 60 mov [ebp+var_4], 5
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 67 movzx eax, [ebp+var_DC40]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 74 mov word ptr [ebp+Src+2], ax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 81 movzx eax, [ebp+var_DC48]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 88 mov word ptr [ebp+Src], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 95 mov eax, [ebp+Src]
            b"\x89\x84\xB5\xAB\xAB\xAB\xAB"  # 101 mov [ebp+esi*4+var_7C00], ea
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 108 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 115 lea ecx, [ebp+var_DC50]
            b"\xE8\xAB\xAB\xAB\xAB"      # 121 call sub_495E20
            b"\x46"                      # 126 inc esi
            b"\x3B\xF7"                  # 127 cmp esi, edi
            b"\x7C\xA7"                  # 129 jl short loc_8D7C30
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 131 mov ecx, [ebp+var_E6A8+4]
            b"\x85\xC9"                  # 137 test ecx, ecx
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 139 mov edi, [ebp+var_E6AC]
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 145 mov edx, [ebp+Source]
            b"\x74\x20"                  # 151 jz short loc_8D7CBF
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 153 cmp dword ptr [edi+260h], 0
            b"\x75\x17"                  # 160 jnz short loc_8D7CBF
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 162 lea eax, [ebp+buf]
            b"\x50"                      # 168 push eax
            b"\x0F\xBF\xC2"              # 169 movsx eax, dx
            b"\x50"                      # 172 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 173 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 178 mov ecx, eax
            b"\xE8"                      # 180 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 180,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 174,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 7E4h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_E6C0+2]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var_E6C0],
            b"\x33\xF6"                  # 19 xor esi, esi
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 21 mov ecx, [ebp+Source]
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 27 mov dword ptr [ebp+var_E6C0+4], e
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 33 movq xmm0, [ebp+var_E6C0]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 41 movq qword ptr [ebp+var_A
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\x00"  # 49 mov [ebp+var_ABFC
            b"\x85\xFF"                  # 59 test edi, edi
            b"\x7E\x62"                  # 61 jle short loc_8D7DB3
            b"\x56"                      # 63 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 64 lea eax, [ebp+var_DB70]
            b"\xB9\xAB\xAB\xAB\xAB"      # 70 mov ecx, offset g_session
            b"\x50"                      # 75 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 76 call CSession_sub_983D90
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 81 mov [ebp+var_4], 6
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 88 movzx eax, [ebp+var_DB68]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 95 mov word ptr [ebp+var_E6AC],
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 102 movzx eax, [ebp+var_DB60]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 109 mov word ptr [ebp+var_E6AC+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 116 mov eax, [ebp+var_E6AC]
            b"\x89\x84\xB5\xAB\xAB\xAB\xAB"  # 122 mov [ebp+esi*4+var_ABF8], ea
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 129 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 136 lea ecx, [ebp+var_DB70]
            b"\xE8\xAB\xAB\xAB\xAB"      # 142 call sub_495E20
            b"\x46"                      # 147 inc esi
            b"\x3B\xF7"                  # 148 cmp esi, edi
            b"\x7C\xA7"                  # 150 jl short loc_8D7D51
            b"\x8D\x04\xBD\xAB\xAB\xAB\x00"  # 152 lea eax, ds:0Ch[edi*4]
            b"\x85\xFF"                  # 159 test edi, edi
            b"\x74\x15"                  # 161 jz short loc_8D7DCA
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 163 lea ecx, [ebp+var_AC04]
            b"\x98"                      # 169 cwd cwde
            b"\x51"                      # 170 push ecx
            b"\x50"                      # 171 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 172 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 177 mov ecx, eax
            b"\xE8"                      # 179 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 179,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 173,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0C8h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+Src], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+Src]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_7404], eax
            b"\x85\xD2"                  # 24 test edx, edx
            b"\x0F\x8E\x94\x00\x00\x00"  # 26 jle loc_8D7EEF
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 32 lea esi, [ebp+var_7400]
            b"\x33\xFF"                  # 38 xor edi, edi
            b"\x57"                      # 40 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 41 lea eax, [ebp+var_D7A0]
            b"\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset g_session
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CSession_sub_983A50
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 58 mov [ebp+var_4], 7
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 65 movzx eax, [ebp+var_D790]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 72 cmp [ebp+var_D760], 10h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 79 mov word ptr [ebp+var_E6C0+4]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 86 lea eax, [ebp+Str]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 92 cmovnb eax, [ebp+Str]
            b"\x50"                      # 99 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 100 call ds:atoi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 106 mov dword ptr [ebp+var_E6C0+6],
            b"\x83\xC4\x04"              # 112 add esp, 4
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 115 mov eax, dword ptr [ebp+var_E6C0
            b"\x89\x06"                  # 121 mov [esi], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 123 movzx eax, word ptr [ebp+var
            b"\x66\x89\x46\xAB"          # 130 mov [esi+4], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 134 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 141 lea ecx, [ebp+var_D7A0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 147 call sub_495E20
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 152 mov edx, [ebp+var_E6A8+4]
            b"\x8D\x76\xAB"              # 158 lea esi, [esi+6]
            b"\x47"                      # 161 inc edi
            b"\x3B\xFA"                  # 162 cmp edi, edx
            b"\x7C\x82"                  # 164 jl short loc_8D7E63
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 166 mov edi, [ebp+var_E6AC]
            b"\x85\xD2"                  # 172 test edx, edx
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 174 mov ecx, [ebp+Source]
            b"\x74\x20"                  # 180 jz short loc_8D7F11
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 182 cmp dword ptr [edi+260h], 0
            b"\x75\x17"                  # 189 jnz short loc_8D7F11
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 191 lea eax, [ebp+var_7404]
            b"\x50"                      # 197 push eax
            b"\x0F\xBF\xC1"              # 198 movsx eax, cx
            b"\x50"                      # 201 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 202 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 207 mov ecx, eax
            b"\xE8"                      # 209 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 209,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 203,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0AAAh
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+Src], cx
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+Src]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_A404], eax
            b"\x85\xD2"                  # 24 test edx, edx
            b"\x0F\x8E\xAB\xAB\xAB\xAB"  # 26 jle loc_8D82B8
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 32 lea esi, [ebp+Dst]
            b"\x33\xFF"                  # 38 xor edi, edi
            b"\x57"                      # 40 push edi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 41 lea eax, [ebp+var_D0A0]
            b"\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset g_session
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CSession_sub_983A50
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 58 mov [ebp+var_4], 8
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 65 mov eax, [ebp+var_D090]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 71 cmp [ebp+var_D060], 10h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 78 mov dword ptr [ebp+var_E6C0+4], e
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 84 lea eax, [ebp+var_D074]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 90 cmovnb eax, [ebp+var_D074]
            b"\x50"                      # 97 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 98 call ds:atoi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 104 mov dword ptr [ebp+var_E6C0], ea
            b"\x83\xC4\x04"              # 110 add esp, 4
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 113 movq xmm0, [ebp+var_E6C0
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 121 mov ax, [ebp+var_D098]
            b"\x66\x0F\xD6\x06"          # 128 movq qword ptr [esi], xmm0
            b"\x66\x89\x46\xAB"          # 132 mov [esi+8], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 136 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 143 lea ecx, [ebp+var_D0A0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 149 call sub_495E20
            b"\x47"                      # 154 inc edi
            b"\x8D\x76\xAB"              # 155 lea esi, [esi+0Ah]
            b"\x3B\xBD\xAB\xAB\xAB\xAB"  # 158 cmp edi, [ebp+var_E6A8+4]
            b"\x7C\x82"                  # 164 jl short loc_8D81E4
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 166 mov edi, [ebp+var_E6AC]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 172 cmp dword ptr [edi+260h], 0
            b"\x75\x1B"                  # 179 jnz short loc_8D828C
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 181 lea eax, [ebp+var_A404]
            b"\x50"                      # 187 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 188 mov eax, [ebp+Source]
            b"\x98"                      # 194 cwd cwde
            b"\x50"                      # 195 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 196 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 201 mov ecx, eax
            b"\xE8"                      # 203 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 203,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 197,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xBA\xAB\xAB\xAB\x00"      # 0  mov edx, 9D6h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+Src+2], ax
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+Src], dx
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 19 mov eax, [ebp+Src]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_BC04], eax
            b"\x33\xC0"                  # 31 xor eax, eax
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 33 mov [ebp+Src], eax
            b"\x85\xC9"                  # 39 test ecx, ecx
            b"\x0F\x8E\xAB\xAB\xAB\xAB"  # 41 jle loc_8D8441
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 47 lea edi, [ebp+var_BC00]
            b"\x66\x0F\x1F\x84\x00\xAB\xAB\xAB\xAB"  # 53 nop word ptr [eax+eax
            b"\x50"                      # 62 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 63 lea eax, [ebp+var_D420]
            b"\xB9\xAB\xAB\xAB\xAB"      # 69 mov ecx, offset g_session
            b"\x50"                      # 74 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 75 call CSession_sub_983A50
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 80 mov [ebp+var_4], 9
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 87 lea eax, [ebp+var_D3F4]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 93 cmp [ebp+var_D3E0], 10h
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 100 mov esi, [ebp+var_D410]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 106 cmovnb eax, [ebp+var_D3F4]
            b"\x50"                      # 113 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 114 call ds:atoi
            b"\x89\x07"                  # 120 mov [edi], eax
            b"\x83\xC4\x04"              # 122 add esp, 4
            b"\x89\x77\xAB"              # 125 mov [edi+4], esi
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 128 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 135 lea ecx, [ebp+var_D420]
            b"\xE8\xAB\xAB\xAB\xAB"      # 141 call sub_495E20
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 146 mov eax, [ebp+Src]
            b"\x8D\x7F\xAB"              # 152 lea edi, [edi+8]
            b"\x40"                      # 155 inc eax
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 156 mov [ebp+Src], eax
            b"\x3B\x85\xAB\xAB\xAB\xAB"  # 162 cmp eax, [ebp+var_E6A8+4]
            b"\x7C\x94"                  # 168 jl short loc_8D8380
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 170 mov edi, [ebp+var_E6AC]
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 176 cmp dword ptr [edi+260h], 0
            b"\x75\x1B"                  # 183 jnz short loc_8D8416
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 185 lea eax, [ebp+var_BC04]
            b"\x50"                      # 191 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 192 mov eax, [ebp+Source]
            b"\x98"                      # 198 cwd cwde
            b"\x50"                      # 199 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 200 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 205 mov ecx, eax
            b"\xE8"                      # 207 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 207,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 201,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0D5h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_C278],
            b"\x0F\x57\xC0"              # 12 xorps xmm0, xmm0
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 15 movq [ebp+var_C278+7], xm
            b"\x0F\xB7\x41\xAB"          # 23 movzx eax, word ptr [ecx+10h]
            b"\xFF\x71\xAB"              # 27 push dword ptr [ecx+28h]
            b"\x66\x83\xC0\xAB"          # 30 add ax, 0Fh
            b"\x8B\xCE"                  # 34 mov ecx, esi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 36 mov word ptr [ebp+var_C278+2]
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_8E3430
            b"\x50"                      # 48 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 49 lea eax, [ebp+var_C278+7]
            b"\x50"                      # 55 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call memcpy
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 61 mov ecx, [ebp+Source]
            b"\x83\xC4\x0C"              # 67 add esp, 0Ch
            b"\x0F\xB7\x41\xAB"          # 70 movzx eax, word ptr [ecx+38h]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 74 mov word ptr [ebp+var_C278+4]
            b"\x8A\x41\xAB"              # 81 mov al, [ecx+30h]
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 84 mov byte ptr [ebp+var_C278+6], al
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 90 mov eax, [ebp+var_C270]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 96 movq xmm0, [ebp+var_C278]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 104 mov [ebp+var_3FFC], eax
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 110 movzx eax, [ebp+var_C26C]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 117 mov [ebp+var_3FF8], ax
            b"\x8A\x85\xAB\xAB\xAB\xAB"  # 124 mov al, [ebp+var_C26A]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 130 movq qword ptr [ebp+var_
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 138 mov [ebp+var_3FF6], al
            b"\xFF\x71\xAB"              # 144 push dword ptr [ecx+10h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 147 call sub_8E3430
            b"\x50"                      # 152 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 153 lea eax, [ebp+var_3FF5]
            b"\x50"                      # 159 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 160 call memcpy
            b"\x83\xC4\x0C"              # 165 add esp, 0Ch
            b"\x8B\xCF"                  # 168 mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 170 call sub_8D2260
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 175 lea eax, [ebp+var_4004]
            b"\x50"                      # 181 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 182 movsx eax, word ptr [ebp+var
            b"\x50"                      # 189 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 190 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 195 mov ecx, eax
            b"\xE8"                      # 197 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 197,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 191,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 801h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_E6C0],
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+Source]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_E6C0+4], e
            b"\x8D\x0C\xBD\xAB\xAB\xAB\xAB"  # 24 lea ecx, ds:0Ch[edi*4]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 31 mov word ptr [ebp+var_E6C0+2]
            b"\x81\xF9\xAB\xAB\xAB\x00"  # 38 cmp ecx, 800h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 44 ja loc_8D965E
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 50 movq xmm0, [ebp+var_E6C0]
            b"\x33\xF6"                  # 58 xor esi, esi
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 60 mov eax, [ebp+var_E6A8+4]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 66 movq qword ptr [ebp+var_9
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 74 mov [ebp+var_93FC], eax
            b"\x85\xFF"                  # 80 test edi, edi
            b"\x7E\x66"                  # 82 jle short loc_8DD585
            b"\x90"                      # 84 no nop
            b"\x56"                      # 85 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 86 lea eax, [ebp+var_DA90]
            b"\xB9\xAB\xAB\xAB\xAB"      # 92 mov ecx, offset g_session
            b"\x50"                      # 97 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 98 call CSession_sub_982640
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 103 mov [ebp+var_4], 41h
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 110 mov ecx, [ebp+var_DA88]
            b"\x85\xC9"                  # 116 test ecx, ecx
            b"\x74\x22"                  # 118 jz short loc_8DD565
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 120 mov ax, [ebp+var_DA80]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 127 mov word ptr [ebp+var_E6AC],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 134 mov word ptr [ebp+var_E6AC+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 141 mov eax, [ebp+var_E6AC]
            b"\x89\x84\xB5\xAB\xAB\xAB\xAB"  # 147 mov [ebp+esi*4+var_93F8], ea
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 154 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 161 lea ecx, [ebp+var_DA90]
            b"\xE8\xAB\xAB\xAB\xAB"      # 167 call sub_495E20
            b"\x46"                      # 172 inc esi
            b"\x3B\xF7"                  # 173 cmp esi, edi
            b"\x7C\xA4"                  # 175 jl short loc_8DD520
            b"\x8D\x0C\xBD\xAB\xAB\xAB\xAB"  # 177 lea ecx, ds:0Ch[edi*4]
            b"\x85\xFF"                  # 184 test edi, edi
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 186 jz loc_8D965E
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 192 lea eax, [ebp+var_9404]
            b"\x50"                      # 198 push eax
            b"\x0F\xBF\xC1"              # 199 movsx eax, cx
            b"\x50"                      # 202 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 203 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 208 mov ecx, eax
            b"\xE8"                      # 210 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 210,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 204,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xC7\x45\xD8\xAB\xAB\xAB\xAB"  # 0  mov [ebp+Point.y], 7F40B11h
            b"\x50"                      # 7  push eax
            b"\x6A\x04"                  # 8  push 4
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 17,
            "packetId": (3, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2019-04-03
    [
        (
            b"\x6A\xAB"                  # 0  push 64h
            b"\x88\x84\x0D\xAB\xAB\xAB\xAB"  # 2  mov [ebp+ecx+var_6AB], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 9  lea eax, [ebp+name]
            b"\x50"                      # 15 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 16 call ds:gethostname
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 22 lea eax, [ebp+name]
            b"\x50"                      # 28 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 29 call ds:gethostbyname
            b"\x85\xC0"                  # 35 test eax, eax
            b"\x75\x10"                  # 37 jnz short loc_A47314
            b"\x0F\x10\x05\xAB\xAB\xAB\xAB"  # 39 movups xmm0, ds:xmmword_C129D
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 46 movups [ebp+var_667], xmm0
            b"\xEB\x29"                  # 53 jmp short loc_A4733D
            b"\x8B\x40\xAB"              # 55 mov eax, [eax+0Ch]
            b"\x8B\x00"                  # 58 mov eax, [eax]
            b"\xFF\x30"                  # 60 push dword ptr [eax]
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 62 call ds:inet_ntoa
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 68 lea edx, [ebp+var_667]
            b"\x2B\xD0"                  # 74 sub edx, eax
            b"\x0F\x1F\x80\xAB\xAB\xAB\xAB"  # 76 nop dword ptr [eax+00000000h]
            b"\x8A\x08"                  # 83 mov cl, [eax]
            b"\x8D\x40\xAB"              # 85 lea eax, [eax+1]
            b"\x88\x4C\x10\xAB"          # 88 mov [eax+edx-1], cl
            b"\x84\xC9"                  # 92 test cl, cl
            b"\x75\xF3"                  # 94 jnz short loc_A47330
            b"\xBA\xAB\xAB\xAB\xAB"      # 96 mov edx, (offset g_session+503Dh)
            b"\x8D\x4A\xAB"              # 101 lea ecx, [edx+1]
            b"\x8A\x02"                  # 104 mov al, [edx]
            b"\x42"                      # 106 inc edx
            b"\x84\xC0"                  # 107 test al, al
            b"\x75\xF9"                  # 109 jnz short loc_A47345
            b"\x2B\xD1"                  # 111 sub edx, ecx
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 113 lea esi, [ebp+var_6B4]
            b"\x52"                      # 119 push edx
            b"\xB9\xAB\xAB\xAB\x00"      # 120 mov ecx, 17h
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 125 mov [ebp+phkResult], edx
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 131 lea edi, [ebp+var_374]
            b"\x8D\x42\xAB"              # 137 lea eax, [edx+5Ch]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 140 mov [ebp+var_6B2], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 147 lea eax, [ebp+var_318]
            b"\x68\xAB\xAB\xAB\xAB"      # 153 push (offset g_session+503Dh)
            b"\xF3\xA5"                  # 158 rep movsd
            b"\x50"                      # 160 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 161 call memcpy
            b"\x83\xC4\x0C"              # 166 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 169 lea eax, [ebp+var_374]
            b"\x50"                      # 175 push eax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 176 mov eax, [ebp+phkResult]
            b"\x83\xC0\xAB"              # 182 add eax, 5Ch
            b"\x0F\xB7\xC0"              # 185 movzx eax, ax
            b"\x50"                      # 188 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 189 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 194 mov ecx, eax
            b"\xE8"                      # 196 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 196,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 190,
        }
    ],
    # 2019-04-03
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 835h
            b"\x8B\x8E\xAB\xAB\xAB\xAB"  # 5  mov ecx, [esi+0A8h]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 11 mov word ptr [ebp+dstStr+8],
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 18 mov word ptr [ebp+dstStr+0Ah]
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_58B210
            b"\x8B\x1D\xAB\xAB\xAB\xAB"  # 30 mov ebx, ds:atoi
            b"\x50"                      # 36 push eax
            b"\xFF\xD3"                  # 37 call ebx
            b"\x8B\x8E\xAB\xAB\xAB\xAB"  # 39 mov ecx, [esi+0A4h]
            b"\x83\xC4\x04"              # 45 add esp, 4
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 48 mov dword ptr [ebp+dstStr+0Dh], e
            b"\xE8\xAB\xAB\xAB\xAB"      # 54 call sub_58B210
            b"\x50"                      # 59 push eax
            b"\xFF\xD3"                  # 60 call ebx
            b"\x8B\x9D\xAB\xAB\xAB\xAB"  # 62 mov ebx, [ebp+var_86C]
            b"\x83\xC4\x04"              # 68 add esp, 4
            b"\x8A\x8D\xAB\xAB\xAB\xAB"  # 71 mov cl, byte ptr [ebp+var_864]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 77 mov [ebp+dstStr.m_len+1], eax
            b"\x8A\x86\xAB\xAB\xAB\x00"  # 83 mov al, [esi+0C4h]
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 89 mov byte ptr [ebp+dstStr+0Ch], al
            b"\xB8\xAB\xAB\xAB\x00"      # 95 mov eax, 800h
            b"\x88\x9D\xAB\xAB\xAB\xAB"  # 100 mov byte ptr [ebp+dstStr.m_alloc
            b"\x66\x3B\xC7"              # 106 cmp ax, di
            b"\x7D\x0C"                  # 109 jge short loc_65B94D
            b"\xC6\x86\xAB\xAB\xAB\xAB\xAB"  # 111 mov byte ptr [esi+0CCh], 0
            b"\xE9\xA8\x00\x00\x00"      # 118 jmp loc_65B9F5
            b"\x80\xBE\xAB\xAB\xAB\xAB\xAB"  # 123 cmp byte ptr [esi+0CCh], 0
            b"\x0F\x84\x9B\x00\x00\x00"  # 130 jz loc_65B9F5
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 136 mov eax, [ebp+dstStr.m_len]
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 142 mov edi, [ebp+var_870]
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 148 movq xmm0, qword ptr [eb
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 156 mov [ebp+var_808], eax
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 162 mov ax, [ebp-814h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 169 movq [ebp+var_810], xmm0
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 177 mov [ebp+var_804], ax
            b"\x88\x8D\xAB\xAB\xAB\xAB"  # 184 mov [ebp+var_802], cl
            b"\x8B\x07"                  # 190 mov eax, [edi]
            b"\x3B\xC7"                  # 192 cmp eax, edi
            b"\x74\x19"                  # 194 jz short loc_65B9AF
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 196 lea edx, [ebp+var_801]
            b"\x0F\x1F\x40\xAB"          # 202 nop dword ptr [eax+00h]
            b"\x8B\x48\xAB"              # 206 mov ecx, [eax+8]
            b"\x8D\x52\xAB"              # 209 lea edx, [edx+4]
            b"\x89\x4A\xAB"              # 212 mov [edx-4], ecx
            b"\x8B\x00"                  # 215 mov eax, [eax]
            b"\x3B\xC7"                  # 217 cmp eax, edi
            b"\x75\xF1"                  # 219 jnz short loc_65B9A0
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 221 mov edi, [ebp+var_868]
            b"\x8B\x07"                  # 227 mov eax, [edi]
            b"\x3B\xC7"                  # 229 cmp eax, edi
            b"\x74\xAB"                  # 231 jz short loc_65B9D3
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 233 lea edx, [ebp+var_801]
            b"\x8D\x14\x9A"              # 239 lea edx, [edx+ebx*4]
            b"\x8B\x48\xAB"              # 242 mov ecx, [eax+8]
            b"\x8D\x52\xAB"              # 245 lea edx, [edx+4]
            b"\x89\x4A\xAB"              # 248 mov [edx-4], ecx
            b"\x8B\x00"                  # 251 mov eax, [eax]
            b"\x3B\xC7"                  # 253 cmp eax, edi
            b"\x75\xF1"                  # 255 jnz short loc_65B9C4
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 257 mov ecx, [ebp+var_85C]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 263 lea eax, [ebp+var_810]
            b"\x50"                      # 269 push eax
            b"\x8D\x04\x8D\xAB\xAB\xAB\x00"  # 270 lea eax, ds:0Fh[ecx*4]
            b"\x98"                      # 277 cwd cwde
            b"\x50"                      # 278 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 279 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 284 mov ecx, eax
            b"\xE8"                      # 286 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 286,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 280,
        }
    ],
    # 2019-04-03
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0C9h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+Source+2],
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+Source], ax
            b"\x33\xF6"                  # 19 xor esi, esi
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 21 mov eax, [ebp+Source]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 27 mov [ebp+Src], eax
            b"\x85\xC9"                  # 33 test ecx, ecx
            b"\x7E\x79"                  # 35 jle short loc_9D0E0D
            b"\x8B\xF9"                  # 37 mov edi, ecx
            b"\x66\x66\x0F\x1F\x84\xAB\xAB\xAB\xAB\xAB"  # 39 nop word ptr [eax
            b"\x56"                      # 49 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 50 lea eax, [ebp+var_DE7C]
            b"\xB9\xAB\xAB\xAB\xAB"      # 56 mov ecx, offset g_session
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CSession_sub_A7D5D0
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 67 mov [ebp+var_4], 5
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 74 movzx eax, [ebp+var_DE6C]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 81 mov word ptr [ebp+Source+2],
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"  # 88 movzx eax, [ebp+var_DE74]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 95 mov word ptr [ebp+Source], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 102 mov eax, [ebp+Source]
            b"\x89\x84\xB5\xAB\xAB\xAB\xAB"  # 108 mov [ebp+esi*4+var_BF0C], ea
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 115 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 122 lea ecx, [ebp+var_DE7C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 128 call sub_499BC0
            b"\x46"                      # 133 inc esi
            b"\x3B\xF7"                  # 134 cmp esi, edi
            b"\x7C\xA7"                  # 136 jl short loc_9D0DA0
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 138 mov ecx, [ebp+var_E9B4+4]
            b"\x85\xC9"                  # 144 test ecx, ecx
            b"\x8B\xBD\xAB\xAB\xAB\xAB"  # 146 mov edi, [ebp+var_E9B8]
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 152 mov edx, [ebp+var_E9A0]
            b"\x74\x20"                  # 158 jz short loc_9D0E2F
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 160 cmp dword ptr [edi+26Ch], 0
            b"\x75\x17"                  # 167 jnz short loc_9D0E2F
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 169 lea eax, [ebp+Src]
            b"\x50"                      # 175 push eax
            b"\x0F\xBF\xC2"              # 176 movsx eax, dx
            b"\x50"                      # 179 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 180 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 185 mov ecx, eax
            b"\xE8"                      # 187 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 187,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 181,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 801h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_E9CC],
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+var_E9A0]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov dword ptr [ebp+var_E9CC+4], e
            b"\x8D\x0C\xBD\xAB\xAB\xAB\x00"  # 24 lea ecx, ds:0Ch[edi*4]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 31 mov word ptr [ebp+var_E9CC+2]
            b"\x81\xF9\xAB\xAB\xAB\x00"  # 38 cmp ecx, 800h
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 44 ja loc_9D27CE
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 50 movq xmm0, [ebp+var_E9CC]
            b"\x33\xF6"                  # 58 xor esi, esi
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 60 mov eax, [ebp+var_E9B4+4]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 66 movq [ebp+var_7710], xmm0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 74 mov [ebp+var_7708], eax
            b"\x85\xFF"                  # 80 test edi, edi
            b"\x0F\x8E\x65\x00\x00\x00"  # 82 jle loc_9D6848
            b"\x56"                      # 88 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 89 lea eax, [ebp+var_DD9C]
            b"\xB9\xAB\xAB\xAB\xAB"      # 95 mov ecx, offset g_session
            b"\x50"                      # 100 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 101 call CSession_sub_A7BE80
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 106 mov [ebp+var_4], 41h
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 113 mov ecx, [ebp+var_DD94]
            b"\x85\xC9"                  # 119 test ecx, ecx
            b"\x74\x22"                  # 121 jz short loc_9D6828
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 123 mov ax, [ebp+var_DD8C]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 130 mov word ptr [ebp+var_E9B8],
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 137 mov word ptr [ebp+var_E9B8+2
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 144 mov eax, [ebp+var_E9B8]
            b"\x89\x84\xB5\xAB\xAB\xAB\xAB"  # 150 mov [ebp+esi*4+var_7704], ea
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 157 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 164 lea ecx, [ebp+var_DD9C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 170 call sub_499BC0
            b"\x46"                      # 175 inc esi
            b"\x3B\xF7"                  # 176 cmp esi, edi
            b"\x7C\xA4"                  # 178 jl short loc_9D67E3
            b"\x8D\x0C\xBD\xAB\xAB\xAB\x00"  # 180 lea ecx, ds:0Ch[edi*4]
            b"\x85\xFF"                  # 187 test edi, edi
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 189 jz loc_9D27CE
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 195 lea eax, [ebp+var_7710]
            b"\x50"                      # 201 push eax
            b"\x0F\xBF\xC1"              # 202 movsx eax, cx
            b"\x50"                      # 205 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 206 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 211 mov ecx, eax
            b"\xE8"                      # 213 call CRagConnection_SendPacket
        ),
        {
            "fixedOffset": 213,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 207,
        }
    ],
]

#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os


class MaxCharsExtractor:
    def addLine(self, w, text):
        w.write(text + "\n")


    def getScript(self, extractors):
        if len(extractors) == 0:
            return
        subName = os.path.basename(os.path.abspath("."))
        outDir = "output"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        outName = outDir + "/maxchars_{0}.h".format(subName)
        with open(outName, "wt") as w:
            maxChars = 0
            for extractor in extractors:
                if maxChars != extractor.maxChars:
                    maxChars = extractor.maxChars
                    w.write("#if PACKETVER >= {0}\n".format(
                        extractor.packetVersion))
                    w.write("#define MAX_CHARS {0}\n".format(maxChars))
                    w.write("#endif\n".format(extractor.packetVersion))

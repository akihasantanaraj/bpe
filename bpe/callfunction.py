#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


class CallFunction:
    def __init__(self, addr):
        self.callCounter = 0
        self.callsFrom = set()
        self.collectCallsFrom = set()
        self.addr = addr
        # push count = calls
        self.counts = dict()
        self.adjustSp = 0
        self.fixSp = 0

    def addCall(self, asm):
        self.callCounter = self.callCounter + 1
        self.callsFrom.add(asm.pos)
        if asm.pushCounter not in self.counts:
            self.counts[asm.pushCounter] = 0
        self.counts[asm.pushCounter] = self.counts[asm.pushCounter] + 1
        # print("push count before: {0} - {1}".format(
        # hex(self.addr), asm.pushCounter))
        # print(self.counts)
        # esp2 = asm.registers["esp"]
        # esp1 = asm.startEsp - asm.stackAlign
        # print("stack: {0}-{1}={2}".format(esp1, esp2, esp1 - esp2))


    def checkPacket(self, packetId, len1, len2, flag):
        if len1 == 255 and len1 != len2:
            len1 = -1
        elif len1 == 4294967295:
            len1 = -1
            if len2 == 4294967295:
                len2 = -1
        elif len1 == 117440492:
            len1 = -1
            if len2 == 117440492:
                len2 = -1
        return packetId < 0xc00 and packetId > 0 and \
            flag >= -1 and flag <= 1 and \
            len1 >= -1 and len2 >= -1 and \
            len1 != 0 and len2 != 0


    def error(self, asm, text):
        asm.exe.log("{0}: {1}".format(text, self.adjustSp))
        asm.printAll()


    def resetFlags(self, asm):
        if asm.exe.isClientSkipFlag() is True:
            asm.beforeSecondCall = None
            asm.prevPacketId = 0
            asm.cxSet = 0
            if asm.debug is True:
                print("asm.beforeSecondCall = None")
                print("asm.prevPacketId = 0")
                print("asm.cxSet = False")


    def collectArgsSp0(self, asm, offset):
        # skip if function ignored
        if asm.debug is True:
            print("Skip function call with adjustSp={0}".format(
                  self.adjustSp))


    def collectArgsSp4(self, asm, offset):
        data1 = asm.getMemory32(offset)
        if data1 > 0x1000000:
            if asm.exe.isClientTooOld() is True:
                val1 = asm.getMemory32(data1)
                if asm.beforeSecondCall is False or \
                   asm.beforeSecondCall is None:
                    if asm.cxSet is False and \
                       asm.exe.isClientSkipFlag() is False and \
                       asm.prevPacketId != 0 and \
                       asm.beforeSecondCall is not None:
                        print("Cx not set before call1")
                        exit(0)
                    if asm.prevPacketId != 0:
                        print("Double prevPacketId set")
                        exit(0)
                    asm.prevPacketId = val1
                    asm.beforeSecondCall = True
                    asm.cxSet = False
                    self.collectCallsFrom.add(asm.pos)
                    if asm.debug is True:
                        print("asm.prevPacketId = {0}".format(
                            asm.prevPacketId))
                        print("asm.cxSet = False")
                        print("asm.beforeSecondCall = True")
                        print("Use function call with " +
                              "adjustSp={0} (pointer)".
                              format(self.adjustSp))
                    return
                else:
                    if asm.beforeSecondCall is True and \
                       asm.exe.isClientSkipFlag() is False:
                        print("wrong flag1: {0}".format(
                            asm.beforeSecondCall))
                        exit(0)
            if asm.debug is True:
                print("Skip function call with adjustSp={0} (pointer)".
                      format(self.adjustSp))
            return
        else:
            self.error(asm,
                       "Error: unsupported function parameters. AdjustSp")
            exit(1)


    def collectArgsSp8(self, asm, offset):
        asm.altLens = (0, 0)
        if asm.callModeSp12 == 2:
            data1 = asm.getMemory32(offset)
            data2 = asm.getMemory32(offset + 4)
            asm.altLens = (data1, data2)
            asm.altPos = asm.pos
            asm.altAddr = self.addr
            if asm.debug is True:
                print(("Skip function call with adjustSp={0} "
                      "(callModeSp12 == 2)").
                      format(self.adjustSp))
            return
        data1 = asm.getMemory32(offset)
        data2 = asm.getMemory32(offset + 4)
        # both pointers (mode 0)
        if data1 > 0x1000000 and data2 > 0x1000000 and asm.callModeSp8 == 0:
            # data2 is pointer to packet struct
            packetId = asm.getMemory32(data2)
            len1 = asm.getMemory32Sign(data2 + 4)
            len2 = asm.getMemory32Sign(data2 + 8)
            flag = asm.getMemory32(data2 + 12)
            if self.checkPacket(packetId, len1, len2, flag):
                asm.addPacket(packetId, len1, len2, flag)
                self.collectCallsFrom.add(asm.pos)
                self.resetFlags(asm)
                return
            packetId = asm.getMemory32(data1)
            len1 = asm.getMemory32Sign(data2)
            len2 = asm.getMemory32Sign(data2 + 4)
            flag = -1
            if self.checkPacket(packetId, len1, len2, flag):
                asm.addPacket(packetId, len1, len2, flag)
                self.collectCallsFrom.add(asm.pos)
                self.resetFlags(asm)
                return
            if packetId > 0 and len1 == 0:
                if asm.exe.isClientTooOld() is True:
                    if asm.beforeSecondCall is None:
                        # if asm.cxSet is False:
                        #    print("Cx not set before call2")
                        #    exit(0)
                        if asm.prevPacketId != 0:
                            print("Double prevPacketId set")
                            exit(0)
                        asm.prevPacketId = packetId
                        asm.cxSet = False
                        if asm.debug is True:
                            print("asm.prevPacketId = {0}".format(
                                asm.prevPacketId))
                            print("asm.cxSet = False")
                            print("Use function call with adjustSp={0}" +
                                  " (pointers (N, 0) first call)".
                                  format(self.adjustSp))
                        self.collectCallsFrom.add(asm.pos)
                        return
                    else:
                        print("wrong flag2: {0}".format(
                              asm.beforeSecondCall))
                        exit(0)
                if asm.debug is True:
                    print("Skip function call with " +
                          "adjustSp={0} (pointers (N, 0))".
                          format(self.adjustSp))
                return
            if asm.getMemory32(data1) == 0 and asm.getMemory32(data2) == 0:
                if asm.exe.isClientTooOld() is True:
                    if asm.beforeSecondCall is False:
                        asm.beforeSecondCall = True
                        asm.cxSet = False
                        if asm.debug is True:
                            print("asm.beforeSecondCall = True")
                            print("asm.cxSet = False")
                            print("Use function call with adjustSp={0}" +
                                  " (pointers (N, 0) second call)".
                                  format(self.adjustSp))
                        return
                        self.collectCallsFrom.add(asm.pos)
                    else:
                        if asm.beforeSecondCall is True and \
                           asm.exe.isClientSkipFlag() is False:
                            print("wrong flag3: {0}".format(
                                  asm.beforeSecondCall))
                            exit(0)
                if asm.debug is True:
                    print("Skip function call with " +
                          "adjustSp={0} (pointers zero)".
                          format(self.adjustSp))
                return
            if asm.callModeSp12 > 0 and packetId == 0 and len1 != 0 and \
               len1 < 0x1000000:
                if asm.debug is True:
                    print("Skip function call with adjustSp={0} (sp12 pp)".
                          format(self.adjustSp))
                    return
            if asm.debug is True:
                print("Skip function call with adjustSp={0} (pointers)".
                      format(self.adjustSp))
        elif data1 > 0x1000000 and data2 == 0:
            packetId = asm.getMemory32(data1)
            len1 = asm.getMemory32Sign(data1 + 4)
            len2 = asm.getMemory32Sign(data1 + 8)
            flag = asm.getMemory32(data1 + 12)
            if self.checkPacket(packetId, len1, len2, flag):
                asm.callModeSp8 = 1
                if asm.debug is True:
                    print("asm.callModeSp8 = 1")
                asm.addPacket(packetId, len1, len2, flag)
                self.collectCallsFrom.add(asm.pos)
                self.resetFlags(asm)
                return
            if packetId == len1:
                if asm.debug is True:
                    print("Skip function call with " +
                          "adjustSp={0} (len1 + len2)".
                          format(self.adjustSp))
                return
            if asm.callModeSp12 > 0:
                if asm.debug is True:
                    print("Skip function call with adjustSp={0} (sp12 p 0)".
                          format(self.adjustSp))
                    return
#            self.collectCallsFrom.add(asm.pos)
        elif asm.callModeSp8 == 1:
            if asm.debug is True:
                print("Skip function call with " +
                      "adjustSp={0} (asm.callModeSp8 == 1)".
                      format(self.adjustSp))
            return
        else:
            len1 = data1
            len2 = data2
            if len1 >= -1 and len2 >= -1 and \
               len1 != 0 and len2 != 0:
                asm.prevLens = (len1, len2)
                self.collectCallsFrom.add(asm.pos)
                if asm.debug is True:
                    print("Use lens only in function call with " +
                          "adjustSp={0} (lens)".format(self.adjustSp))
                return
        self.error(asm, "Error: unsupported function parameters. AdjustSp")
        exit(1)


    def collectArgsSp12(self, asm, offset, prevLens):
        data1 = asm.getMemory32(offset)
        data2 = asm.getMemory32(offset + 4)
        data3 = asm.getMemory32(offset + 8)
        if data1 > 0x1000000 and data2 > 0x1000000 and data3 > 0x1000000:
            val1 = asm.getMemory32(data1)
            val2 = asm.getMemory32(data2)
            val3 = asm.getMemory32(data3)

            if val1 == 0 and val3 == 0:
                packetId = val2
                len1 = prevLens[0]
                len2 = prevLens[0]
                flag = -1
                # hardcoded detection fix from N, 0, 0, -1
                if self.checkPacket(packetId, len1, len2, flag) is False:
                    ver = asm.exe.client_date
                    if ver >= 20151116 and \
                       len1 == 0 and len2 == 0:
                        len1 = asm.getMemory32(offset + 12)
                        len2 = asm.getMemory32(offset + 16)
                        if len1 != len2:
                            print("Error: possible wrong hardcoded "
                                  "packet: {0}, {1}, {2}".format(hex(packetId),
                                                                 len1,
                                                                 len2))
                            exit(1)
                    # if lens was used before in other function patters
                    elif (asm.callModeSp12 == 2 and
                          ver in (20100817, 20100818, 20100824, 20100825) and
                          len1 == 0 and len2 == 0):
                        if asm.altLens != (0, 0):
                            len1 = asm.altLens[0]
                            len2 = asm.altLens[1]
                            asm.callFunctions[asm.altAddr]. \
                                collectCallsFrom.add(asm.altPos)
                            asm.altLens = (0, 0)
                            asm.altPos = 0
                asm.addPacket(packetId, len1, len2, flag)
                self.collectCallsFrom.add(asm.pos)
                self.resetFlags(asm)
                asm.callModeSp12 = 1
                return
            elif val2 != 0 and val3 != 0:
                val4 = asm.getMemory32(data3 + 4)
                flag = -1
                if self.checkPacket(val2, val3, val4, flag):
                    asm.addPacket(val2, val3, val4, flag)
                    self.collectCallsFrom.add(asm.pos)
                    self.resetFlags(asm)
                    asm.callModeSp12 = 2
                    return
                if asm.debug is True:
                    print("Skip function call with adjustSp={0} (0, N, M)".
                          format(self.adjustSp))
                return
        self.error(asm, "Error: unsupported function parameters. AdjustSp")
        exit(1)


    def collectArgsSp16(self, asm, offset):
        self.collectCallsFrom.add(asm.pos)
        self.resetFlags(asm)
        data1 = asm.getMemory32(offset)
        data2 = asm.getMemory32Sign(offset + 4)
        data3 = asm.getMemory32Sign(offset + 8)
        data4 = asm.getMemory32(offset + 12)
        if data1 > 0x1000000 and \
           data2 == 0 and \
           data3 > 0x1000000 and \
           data4 > 0x1000000:
            if asm.debug is True:
                print("Skip function call with adjustSp={0}".format(
                      self.adjustSp))
            return
        asm.addPacket(
            data1,
            data2,
            data3,
            data4
        )


    def collectArgsSpError(self, asm, offset):
        self.error(asm, "Error: Unsupported adjustSp")
        exit(1)
        for i in range(0, self.adjustSp / 4):
            offset = asm.registers["esp"] + i * 4
            data = asm.getMemory32(offset)
            if data > 0x1000000:
                print("stack pointer data: {0}: {1}".format(
                      hex(offset),
                      hex(data)))
                offset = data
                data = asm.getMemory32(offset)
            print("stack data: {0}: {1}".format(hex(offset), data))


    def collectArgs(self, asm):
        # for 2011-10-05aRagexeRE.exe
        offset = asm.registers["esp"]
        # asm.printAll()
        if asm.debug is True:
            print("AdjustSp {0}".format(self.adjustSp))
        prevLens = asm.prevLens
        asm.prevLens = (0, 0)
        if asm.exe.isClientTooOld() is True:
            if asm.beforeSecondCall is True and asm.cxSet is True:
                print("Set all flags to wrong values")
                exit(0)
        if self.adjustSp == 4:
            self.collectArgsSp4(asm, offset)
        elif self.adjustSp == 8:
            self.collectArgsSp8(asm, offset)
        elif self.adjustSp == 12:
            self.collectArgsSp12(asm, offset, prevLens)
        elif self.adjustSp == 16:
            self.collectArgsSp16(asm, offset)
        elif self.adjustSp == 0:
            self.collectArgsSp0(asm, offset)
        else:
            self.collectArgsSpError(asm, offset)

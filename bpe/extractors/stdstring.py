#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchStdString(self):
    # search in recv_packet_73
    if self.packet73Tmp == 0:
        self.log("Error: packet73 not found")
        exit(1)

    offset, section = self.exe.string(b"server:%s\n")
    if offset is False:
        self.log("failed in search 'server:%s\n'")
        exit(1)
    serverStr = section.rawToVa(offset)

    offset, section = self.exe.string(b"ip:%s\n")
    if offset is False:
        self.log("failed in search 'ip:%s\n'")
        exit(1)
    ipStr = section.rawToVa(offset)

    serverStrHex = self.exe.toHex(serverStr, 4)
    ipStrHex = self.exe.toHex(ipStr, 4)

    # 0  mov esi, fprintf
    # 6  push offset endl
    # 11 push edi
    # 12 call esi ; fprintf
    # 14 cmp g_session_zone_server_name.m_allocated_len, 10h
    # 21 mov eax, offset g_session_zone_server_name
    # 26 cmovnb eax, g_session_zone_server_name.m_cstr
    # 33 push eax
    # 34 push offset aServerS
    # 39 push edi
    # 40 call esi ; fprintf
    # 42 push offset g_zoneServerAddr
    # 47 push offset aIpS
    # 52 push edi
    # 53 call esi ; fprintf
    code = (
        b"\x8B\x35\xAB\xAB\xAB\xAB"        # 0
        b"\x68\xAB\xAB\xAB\xAB"            # 6
        b"\x57"                            # 11
        b"\xFF\xD6"                        # 12
        b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 14
        b"\xB8\xAB\xAB\xAB\xAB"            # 21
        b"\x0F\x43\x05\xAB\xAB\xAB\xAB"    # 26
        b"\x50"                            # 33
        b"\x68" + serverStrHex +           # 34
        b"\x57"                            # 39
        b"\xFF\xD6"                        # 40
        b"\x68\xAB\xAB\xAB\xAB"            # 42
        b"\x68" + ipStrHex +               # 47
        b"\x57"                            # 52
        b"\xFF\xD6"                        # 53
    )
    fprintfOffset = (2, True)
    allocLenOffset = (16, 4)
    minLenOffset = (20, 1)
    nameOffset = (22, 4)
    cStrOffset = (29, 4)
    g_zoneServerAddrOffset = 43
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.packet73Tmp + 10,
                                   self.packet73Tmp + 0x100)

    if offset is False:
        # 2019-02-13
        # 0  push offset endl
        # 5  push esi
        # 6  call fprintf
        # 11 cmp g_session.m_zone_server_name.m_allocated_len, 10h
        # 18 mov eax, offset g_session.m_zone_server_name
        # 23 cmovnb eax, g_session.m_zone_server_name.m_cstr
        # 30 push eax
        # 31 push offset aServerS
        # 36 push esi
        # 37 call fprintf
        # 42 push offset g_zoneServerAddr
        # 47 push offset aIpS
        # 52 push esi
        # 53 call fprintf
        code = (
            b"\x68\xAB\xAB\xAB\xAB"            # 0 push offset Str
            b"\x56"                            # 5 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call fprintf
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 11 cmp g_session.m_zone_server
            b"\xB8\xAB\xAB\xAB\xAB"            # 18 mov eax, offset g_session.m
            b"\x0F\x43\x05\xAB\xAB\xAB\xAB"    # 23 cmovnb eax, g_session.m_zon
            b"\x50"                            # 30 push eax
            b"\x68" + serverStrHex +           # 31 push offset aServerS
            b"\x56"                            # 36 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 37 call fprintf
            b"\x68\xAB\xAB\xAB\xAB"            # 42 push offset g_zoneServerAdd
            b"\x68" + ipStrHex +               # 47 push offset aIpS
            b"\x56"                            # 52 push esi
            b"\xE8"                            # 53 call fprintf
        )

        fprintfOffset = (7, False)
        allocLenOffset = (13, 4)
        minLenOffset = (17, 1)
        nameOffset = (19, 4)
        cStrOffset = (26, 4)
        g_zoneServerAddrOffset = 43
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.packet73Tmp + 10,
                                       self.packet73Tmp + 0x100)

    if offset is False:
        # 2015-01-07
        # 0  mov edi, fprintf
        # 6  push offset _word_9CBA34
        # 11 push esi
        # 12 call edi
        # 14 mov eax, g_session.m_zone_server_name.m_cstr
        # 19 add esp, 0Ch
        # 22 cmp g_session.m_zone_server_name.m_allocated_len, 10h
        # 29 jnb short loc_85E5A0
        # 31 mov eax, offset g_session.m_zone_server_name
        # 36 push eax
        # 37 push offset aServerS
        # 42 push esi
        # 43 call edi
        # 45 push offset g_zoneServerAddr
        # 50 push offset aIpS
        # 55 push esi
        # 56 call edi
        code = (
            b"\x8B\x3D\xAB\xAB\xAB\xAB"        # 0
            b"\x68\xAB\xAB\xAB\xAB"            # 6
            b"\x56"                            # 11
            b"\xFF\xD7"                        # 12
            b"\xA1\xAB\xAB\xAB\xAB"            # 14
            b"\x83\xC4\x0C"                    # 19
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 22
            b"\x73\xAB"                        # 29
            b"\xB8\xAB\xAB\xAB\xAB"            # 31
            b"\x50"                            # 36
            b"\x68" + serverStrHex +           # 37
            b"\x56"                            # 42
            b"\xFF\xD7"                        # 43
            b"\x68\xAB\xAB\xAB\xAB"            # 45
            b"\x68" + ipStrHex +               # 50
            b"\x56"                            # 55
            b"\xFF\xD7"                        # 56
        )
        fprintfOffset = (2, True)
        allocLenOffset = (24, 4)
        minLenOffset = (28, 1)
        nameOffset = (32, 4)
        cStrOffset = (15, 4)
        g_zoneServerAddrOffset = 46
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.packet73Tmp + 10,
                                       self.packet73Tmp + 0x100)

    if offset is False:
        # 0  call _fprintf
        # 5  mov eax, g_session_zone_server_name.m_cstr
        # 10 add esp, 0Ch
        # 13 cmp g_session_zone_server_name.m_allocated_len, 10h
        # 20 jnb short loc_5ECEAB
        # 22 mov eax, offset g_session_zone_server_name
        # 27 push eax
        # 28 push offset aServerS
        # 33 push esi
        # 34 call _fprintf
        # 39 push offset g_zoneServerAddr
        # 44 push offset aIpS
        # 49 push esi
        # 50 call _fprintf
        code = (
            b"\xE8\xAB\xAB\xAB\xAB"            # 0 call _fprintf
            b"\xA1\xAB\xAB\xAB\xAB"            # 5 mov eax, g_session_zone_serv
            b"\x83\xC4\x0C"                    # 10 add esp, 0Ch
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 13 cmp g_session_zone_server_n
            b"\x73\x05"                        # 20 jnb short loc_5ECEAB
            b"\xB8\xAB\xAB\xAB\xAB"            # 22 mov eax, offset g_session_z
            b"\x50"                            # 27 push eax
            b"\x68" + serverStrHex +           # 28 push offset aServerS
            b"\x56"                            # 33 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 34 call _fprintf
            b"\x68\xAB\xAB\xAB\xAB"            # 39 push offset g_zoneServerAdd
            b"\x68" + ipStrHex +               # 44 push offset aIpS
            b"\x56"                            # 49 push esi
            b"\xE8"                            # 50 call _fprintf
        )
        fprintfOffset = (1, False)
        allocLenOffset = (15, 4)
        minLenOffset = (19, 1)
        nameOffset = (23, 4)
        cStrOffset = (6, 4)
        g_zoneServerAddrOffset = 40
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.packet73Tmp + 10,
                                       self.packet73Tmp + 0x100)

    if offset is False:
        self.log("failed in search std::string.")
        if self.packetVersion < "20110000" or \
           (self.clientType == "iro" and self.packetVersion < "20120000"):
            return
        exit(1)

    if fprintfOffset[1] is True:
        self.fprintf = self.exe.readUInt(offset + fprintfOffset[0])
        self.addVaFunc("fprintf", self.fprintf)
    else:
        self.fprintf = self.getAddr(offset,
                                    fprintfOffset[0],
                                    fprintfOffset[0] + 4)
        self.addRawFunc("fprintf", self.fprintf)
    self.g_zoneServerAddr = self.exe.readUInt(offset + g_zoneServerAddrOffset)
    self.addVaVar("g_zoneServerAddr", self.g_zoneServerAddr)

    name = self.getVarAddr(offset, nameOffset)
    allocLen = self.getVarAddr(offset, allocLenOffset) - name
    cStr = self.getVarAddr(offset, cStrOffset) - name
    minLen = self.getVarAddr(offset, minLenOffset)
    if cStr != 0:
        self.log("Error: wrong c_str offset found: {0}".format(cStr))
        exit(1)
    if allocLen != minLen + 4:
        self.log("Error: wrong m_allocated_len in std::string found: {0}, {1}".
                 format(allocLen, minLen))
        exit(1)
    self.stdStringAllocLen = allocLen
    self.stdstringSize = allocLen + 4

    debug = True
    self.addStruct("struct_std_string")
    self.addStructMember("m_cstr", 0, 4, debug)
    self.addStructMember("m_len", minLen, 4, debug)
    self.addStructMember("m_allocated_len", allocLen, 4, debug)
    self.setStructMemberType(0, "char*")
    self.setStructComment("Size {0}".format(hex(self.stdstringSize)))
    self.stdString = (0, minLen, allocLen)

    self.addStruct("CSession")
    offset = name - self.session
    self.addStructMember("m_zone_server_name",
                         offset,
                         self.stdstringSize,
                         True)
    self.setStructMemberType(offset, "struct_std_string")

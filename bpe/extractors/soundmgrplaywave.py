#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSoundMgrPlayWave(self):
    offset, section = self.exe.halfString(b"_heal_effect.wav")
    if offset is False:
        self.log("failed in search _heal_effect.wav")
        exit(1)
    strAddr = section.rawToVa(offset)
    strHex = self.exe.toHex(strAddr, 4)
    soundMgrHex = self.exe.toHex(self.soundMgr, 4)

    # search in recv_packet_13E_7FB
    # 0  mov ecx, g_soundMgr
    # 6  mov dword ptr [esp+1B8h+var_1C4+0Ch], 3F800000h
    # 13 push 28h
    # 15 push 0FAh
    # 20 sub esp, 0Ch
    # 23 mov dword ptr [esp+1CCh+var_1C4], 0
    # 31 mov [esp+1CCh+y], 0
    # 39 mov [esp+1CCh+x], 0
    # 46 push offset a_heal_effect_w
    # 51 call CSoundMgr_PlayWave
    # 56 sub esp, 10h
    code = (
        b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
        b"\xC7\x04\xAB\xAB\xAB\xAB\xAB"    # 6 mov dword ptr [esp+1B8h+var_1C4+
        b"\x6A\xAB"                        # 13 push 28h
        b"\x68\xAB\xAB\xAB\x00"            # 15 push 0FAh
        b"\x83\xEC\x0C"                    # 20 sub esp, 0Ch
        b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 23 mov dword ptr [esp+1CCh+var_1
        b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 31 mov [esp+1CCh+y], 0
        b"\xC7\x04\xAB\x00\x00\x00\x00"    # 39 mov [esp+1CCh+x], 0
        b"\x68" + strHex +                 # 46 push offset a_heal_effect_w
        b"\xE8\xAB\xAB\xAB\xAB"            # 51 call CSoundMgr_PlayWave
        b"\x83\xEC\x10"                    # 56 sub esp, 10h
    )
    CSoundMgr_PlayWaveOffset = 52
    PlayWaveOffset = 0
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov ecx, g_soundMgr
        # 6  fstp [esp+1D0h+vFactor]
        # 9  fld fldz
        # 11 push 28h
        # 13 push 0FAh
        # 18 sub esp, 0Ch
        # 21 fst [esp+1E4h+z]
        # 25 fst [esp+1E4h+y]
        # 29 fstp [esp+1E4h+x]
        # 32 push offset a_heal_effect_w
        # 37 call CSountMgr_PlayWave
        # 42 fld fldz
        # 44 sub esp, 10h
        code = (
            b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
            b"\xD9\x1C\xAB"                    # 6 fstp [esp+1D0h+vFactor]
            b"\xD9\xEE"                        # 9 fld fldz
            b"\x6A\x28"                        # 11 push 28h
            b"\x68\xFA\x00\x00\x00"            # 13 push 0FAh
            b"\x83\xEC\x0C"                    # 18 sub esp, 0Ch
            b"\xD9\x54\x24\xAB"                # 21 fst [esp+1E4h+z]
            b"\xD9\x54\x24\xAB"                # 25 fst [esp+1E4h+y]
            b"\xD9\x1C\xAB"                    # 29 fstp [esp+1E4h+x]
            b"\x68" + strHex +                 # 32 push offset a_heal_effect_w
            b"\xE8\xAB\xAB\xAB\xAB"            # 37 call CSountMgr_PlayWave
            b"\xD9\xEE"                        # 42 fld fldz
            b"\x83\xEC\x10"                    # 44 sub esp, 10h
        )
        CSoundMgr_PlayWaveOffset = 38
        PlayWaveOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  fstp [esp+1D4h+vFactor]
        # 3  fld fldz
        # 5  push 28h
        # 7  push 0FAh
        # 12 sub esp, 0Ch
        # 15 fst [esp+1E8h+z]
        # 19 fst [esp+1E8h+y]
        # 23 fstp [esp+1E8h+x]
        # 26 push offset a_heal_effect_w
        # 31 call CSoundMgr_PlayWave
        code = (
            b"\xD9\x1C\xAB"                    # 0 fstp [esp+1D4h+vFactor]
            b"\xD9\xEE"                        # 3 fld fldz
            b"\x6A\x28"                        # 5 push 28h
            b"\x68\xFA\x00\x00\x00"            # 7 push 0FAh
            b"\x83\xEC\x0C"                    # 12 sub esp, 0Ch
            b"\xD9\x54\x24\xAB"                # 15 fst [esp+1E8h+z]
            b"\xD9\x54\x24\xAB"                # 19 fst [esp+1E8h+y]
            b"\xD9\x1C\xAB"                    # 23 fstp [esp+1E8h+x]
            b"\x68" + strHex +                 # 26 push offset a_heal_effect_w
            b"\xE8\xAB\xAB\xAB\xAB" +          # 31 call CSoundMgr_PlayWave
            b"\xD9\xEE"                        # 36 fldz
        )
        CSoundMgr_PlayWaveOffset = 0
        PlayWaveOffset = 32
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov ecx, g_soundMgr
        # 6  xorps xmm0, xmm0
        # 9  mov [esp+38h+var_38], 3F800000h
        # 16 push 28h
        # 18 push 0FAh
        # 23 sub esp, 0Ch
        # 26 movq [ebp+var_1C+4], xmm0
        # 31 mov [esp+4Ch+var_44], 0
        # 39 mov [esp+4Ch+var_48], 0
        # 47 mov [esp+4Ch+var_4C], 0
        # 54 push offset a_heal_effect_w
        # 59 call CSoundMgr_PlayWave
        # 64 movq xmm0, [ebp+var_1C+4]
        # 69 sub esp, 10h
        code = (
            b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
            b"\x0F\x57\xC0"                    # 6 xorps xmm0, xmm0
            b"\xC7\x04\xAB\x00\x00\x80\x3F"    # 9 mov [esp+38h+var_38], 3F8000
            b"\x6A\x28"                        # 16 push 28h
            b"\x68\xFA\x00\x00\x00"            # 18 push 0FAh
            b"\x83\xEC\x0C"                    # 23 sub esp, 0Ch
            b"\x66\x0F\xD6\x45\xAB"            # 26 movq [ebp+var_1C+4], xmm0
            b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 31 mov [esp+4Ch+var_44], 0
            b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 39 mov [esp+4Ch+var_48], 0
            b"\xC7\x04\xAB\x00\x00\x00\x00"    # 47 mov [esp+4Ch+var_4C], 0
            b"\x68" + strHex +                 # 54 push offset a_heal_effect_w
            b"\xE8\xAB\xAB\xAB\xAB"            # 59 call CSoundMgr_PlayWave
            b"\xF3\x0F\x7E\x45\xAB"            # 64 movq xmm0, [ebp+var_1C+4]
            b"\x83\xEC\x10"                    # 69 sub esp, 10h
        )
        CSoundMgr_PlayWaveOffset = 60
        PlayWaveOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov ecx, g_soundMgr
        # 6  xorps xmm0, xmm0
        # 9  mov [esp+28h+var_28], 3F800000h
        # 16 push 28h
        # 18 push 0FAh
        # 23 sub esp, 0Ch
        # 26 movq [ebp+var_C+4], xmm0
        # 31 mov [esp+3Ch+var_34], eax
        # 35 mov dword ptr [esp+3Ch+var_3C+4], eax
        # 39 mov dword ptr [esp+3Ch+var_3C], eax
        # 42 push offset a_heal_effect_w
        # 47 call CSoundMgr_PlayWave
        # 52 movq xmm0, [ebp+var_C+4]
        # 57 sub esp, 10h
        code = (
            b"\x8B\x0D" + soundMgrHex +        # 0 mov ecx, g_soundMgr
            b"\x0F\x57\xC0"                    # 6 xorps xmm0, xmm0
            b"\xC7\x04\xAB\x00\x00\x80\x3F"    # 9 mov [esp+28h+var_28], 3F8000
            b"\x6A\x28"                        # 16 push 28h
            b"\x68\xFA\x00\x00\x00"            # 18 push 0FAh
            b"\x83\xEC\x0C"                    # 23 sub esp, 0Ch
            b"\x66\x0F\xD6\x45\xAB"            # 26 movq [ebp+var_C+4], xmm0
            b"\x89\x44\x24\xAB"                # 31 mov [esp+3Ch+var_34], eax
            b"\x89\x44\x24\xAB"                # 35 mov dword ptr [esp+3Ch+var_
            b"\x89\x04\x24"                    # 39 mov dword ptr [esp+3Ch+var_
            b"\x68" + strHex +                 # 42 push offset a_heal_effect_w
            b"\xE8\xAB\xAB\xAB\xAB"            # 47 call CSoundMgr_PlayWave
            b"\xF3\x0F\x7E\x45\xAB"            # 52 movq xmm0, [ebp+var_C+4]
            b"\x83\xEC\x10"                    # 57 sub esp, 10h
        )
        CSoundMgr_PlayWaveOffset = 48
        PlayWaveOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("Error: CSoundMgr::PlayWave not found")
        if self.packetVersion < "20110000":
            # in 2010 or older search possible but bit complex
            return
        exit(1)

    offset1 = offset
    self.log("offset1: 0x{0:X}".format(self.exe.rawToVa(offset1)))

    if CSoundMgr_PlayWaveOffset != 0:
        self.CSoundMgr_PlayWave = self.getAddr(offset,
                                               CSoundMgr_PlayWaveOffset,
                                               CSoundMgr_PlayWaveOffset + 4)
        self.addRawFunc("CSoundMgr::PlayWave", self.CSoundMgr_PlayWave)
    if PlayWaveOffset != 0:
        self.PlayWave = self.getAddr(offset,
                                     PlayWaveOffset,
                                     PlayWaveOffset + 4)
        self.addRawFunc("PlayWave", self.PlayWave)

    # 0  push 0D8h
    # 5  mov ecx, edi
    # 7  call CAbleToMakeEffect_LaunchEffect
    code = (
        b"\x68\xD8\x00\x00\x00"  # 0 push 0D8h
        b"\x8B\xCF"              # 5 mov ecx, edi
        b"\xE8"                  # 7 call CAbleToMakeEffect_LaunchEffect
    )
    launchEffectOffset = 8
    offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x90)

    if offset is False:
        # 0  push 0D8h
        # 5  mov ecx, esi
        # 7  call CAbleToMakeEffect_LaunchEffect
        code = (
            b"\x68\xD8\x00\x00\x00"  # 0 push 0D8h
            b"\x8B\xCE"              # 5 mov ecx, esi
            b"\xE8"                  # 7 call CAbleToMakeEffect_LaunchEffect
        )
        launchEffectOffset = 8
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x90)

    if offset is False:
        # 0  mov ecx, esi
        # 2  push 0D8h
        # 7  call CAbleToMakeEffect_LaunchEffect
        code = (
            b"\x8B\xCE"                        # 0 mov ecx, esi
            b"\x68\xD8\x00\x00\x00"            # 2 push 0D8h
            b"\xE8"                            # 7 call CAbleToMakeEffect_Launc
        )
        launchEffectOffset = 8
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x90)

    if offset is False:
        # 0  mov ecx, edi
        # 2  push 0D8h
        # 7  call CAbleToMakeEffect_Launch
        code = (
            b"\x8B\xCF"                        # 0 mov ecx, edi
            b"\x68\xD8\x00\x00\x00"            # 2 push 0D8h
            b"\xE8"                            # 7 call CAbleToMakeEffect_Launc
        )
        launchEffectOffset = 8
        offset = self.exe.codeWildcard(code, b"\xAB", offset1, offset1 + 0x90)

    if offset is False:
        self.log("Error: CAbleToMakeEffect::LaunchEffect not found")
        exit(1)

    self.CAbleToMakeEffect_LaunchEffect = self.getAddr(offset,
                                                       launchEffectOffset,
                                                       launchEffectOffset + 4)
    self.addRawFunc("CAbleToMakeEffect::LaunchEffect",
                    self.CAbleToMakeEffect_LaunchEffect)

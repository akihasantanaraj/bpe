#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSendPacketOld(self, errorExit):
    # 1. search for 20080311 and older clients
    if self.getPacketSizeFunction != 0 or self.packetVersion > "20080311":
        return
    if self.instanceR == 0:
        return
    if self.loginPollAddr == 0:
        self.log("Error: CLoginMode::PollNetworkStatus "
                 "not found before searchSendPacketOld")
        if errorExit is True:
            exit(1)
        return

    # 2008-03-11
    # 0  push 1BFh
    # 5  mov [ebp+offset1], 1
    # 9  call CRagConnection::instanceR
    # 14 mov ecx, eax
    # 16 call CRagConnection::GetPacketSize
    # 21 push eax
    # 22 call CRagConnection::instanceR
    # 27 mov ecx, eax
    # 29 call CRagConnection::SendPacket
    # 34 jmp addr1
    instanceOffset1 = 10
    getPacketSizeOffset = 17
    instanceOffset2 = 23
    sendOffset = 30
    code = (
        b"\x68\xBF\x01\x00\x00" +
        b"\xC6\x45\xAB\x01" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x50" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\xE9")
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.loginPollAddr,
                                   self.loginPollAddr + 0x500)
    if offset is False:
        self.log("failed searchSendPacketOld in step 1.")
        if errorExit is True:
            exit(1)
        return
    # compare is CRagConnection::instanceR real functions
    instanceR1 = self.getAddr(offset,
                              instanceOffset1,
                              instanceOffset1 + 4)
    instanceR2 = self.getAddr(offset,
                              instanceOffset2,
                              instanceOffset2 + 4)
    if instanceR1 != instanceR2:
        self.log("Error: different CRagConnection::instanceR "
                 "found in old searchSendPacket.")
        if errorExit is True:
            exit(1)
        return
    if self.instanceR != 0:
        if instanceR1 != self.instanceR:
            self.log("Error: wrong CRagConnection::instanceR "
                     "found in old searchSendPacket.")
            if errorExit is True:
                exit(1)
            return False
    else:
        self.instanceR = instanceR1
        self.addRawFunc("CRagConnection::instanceR",
                        self.instanceR)
    sendPacket = self.getAddr(offset, sendOffset, sendOffset + 4)
    if self.sendPacket != 0:
        if sendPacket != self.sendPacket:
            self.log("Error: CRagConnection::SendPacket validation failed")
            exit(1)
    else:
        self.sendPacket = sendPacket
        self.sendPacketVa = self.exe.rawToVa(self.sendPacket)
        self.addRawFunc("CRagConnection::SendPacket",
                        self.sendPacket)
    self.getPacketSizeFunction = self.getAddr(offset,
                                              getPacketSizeOffset,
                                              getPacketSizeOffset + 4)
    self.addRawFunc("CRagConnection::GetPacketSize",
                    self.getPacketSizeFunction)
    return True

#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.calls import searchCallsToCall
from bpe.blocks.blocks import searchBlocks, blocksToLabels, showBlocksInfo


def searchGetPacketSizes(self):
    showInfo = False
    getPacketSizeVa = self.exe.rawToVa(self.getPacketSizeFunction)
    offsets = searchCallsToCall(self, getPacketSizeVa)
    if self.instanceR != 0:
        from bpe.blocks.getpacketsizenew import blocks
        relVars = {"instanceR": self.instanceR}
    else:
        from bpe.blocks.getpacketsizeold import blocks
        relVars = {"g_instanceR": self.g_instanceR}
    found, errors = searchBlocks(self,
                                 "getPacketSizes",
                                 offsets,
                                 blocks,
                                 relVars,
                                 "packetId")
    self.getPacketSizeBlocks = found
    self.getPacketSizeStats = (len(found), len(errors))
    if showInfo is True:
        showBlocksInfo(self, offsets, found, errors)


def addGetPacketSizeLabels(self):
    name = "CRagConnection_GetPacketSize_{0}"
    blocksToLabels(self, self.getPacketSizeBlocks, name)

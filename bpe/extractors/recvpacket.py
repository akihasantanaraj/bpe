#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchRecvPacket(self):
    # search CRagConnection_RecvPacket usage in CGameMode_PollNetworkStatus
    # 1. search for call pattern
    # 2017-11-01
    # 0  push eax
    # 1  push offet packet_buf
    # 6  call CRagConnection_instanceR
    # 11 mov ecx, eax
    # 13 call CRagConnection_RecvPacket
    # 18 mov byte ptr [ebp + buf + 3], al
    # 21 test al, al
    bufOffset = 2
    instanceOffset = 7
    recvOffset = 14
    hook1Offset = 11
    callRecvPacketOffset = 13
    hook2Offset = 18
    code = (
        b"\x50" +
        b"\x68\xAB\xAB\xAB\x01" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x88\x45\x8B" +
        b"\x84\xC0")
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.gamePollAddr,
                                   self.gamePollAddr + 0x400)
    if offset is False:
        # 2017-11-01
        # 0  push eax
        # 1  push offet packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov byte ptr [ebp + buf + 3], al
        # 21 test al, al
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        code = (
            b"\x50" +
            b"\x68\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x88\x45\x8B" +
            b"\x84\xC0")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x400)
    if offset is False:
        # 2018-07-04
        # 0  push eax
        # 1  push offset packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov bl, al
        # 20 test bl, bl
        # 22 jz short A
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        code = (
            b"\x50"                            # 0
            b"\x68\xAB\xAB\xAB\xAB"            # 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 6
            b"\x8B\xC8"                        # 11
            b"\xE8\xAB\xAB\xAB\xAB"            # 13
            b"\x8A\xD8"                        # 18
            b"\x84\xDB"                        # 20
            b"\x74"                            # 22
        )
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x300)
    if offset is False:
        # 2019-02-13
        # 0  push eax
        # 1  push offset packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov [ebp+var_2E95], al
        # 24 test al, al
        code = (
            b"\x50"                            # 0 push eax
            b"\x68\xAB\xAB\xAB\xAB"            # 1 push offset packet_buf
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call CRagConnection_instance
            b"\x8B\xC8"                        # 11 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 13 call CRagConnection_RecvPac
            b"\x88\x85\xAB\xAB\xAB\xAB"        # 18 mov [ebp+var_2E95], al
            b"\x84\xC0"                        # 24 test al, al
        )
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x300)
    if offset is False:
        # 2018-06-xx mro
        # 0  push offset packet_buf
        # 5  call CRagConnection_instanceR
        # 10 mov ecx, eax
        # 12 call CRagConnection_RecvPacket
        # 17 test al, al
        # 19 jz loc_88A2A9
        # 25 push ebx
        bufOffset = 1
        instanceOffset = 6
        recvOffset = 13
        hook1Offset = 10
        callRecvPacketOffset = 12
        hook2Offset = 17
        code = (
            b"\x68\xAB\xAB\xAB\xAB"            # 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 5
            b"\x8B\xC8"                        # 10
            b"\xE8\xAB\xAB\xAB\xAB"            # 12
            b"\x84\xC0"                        # 17
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 19
            b"\x53"                            # 25
        )
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x400)
    if offset is False:
        # aro
        # 0  push offset buf
        # 5  call CRagConnection_instanceR
        # 10 mov ecx, eax
        # 12 call CRagConnection_RecvPacket
        # 17 test al, al
        # 19 jz loc_6F9825
        # 25 jmp short loc_6F7340
        bufOffset = 1
        instanceOffset = 6
        recvOffset = 13
        hook1Offset = 10
        callRecvPacketOffset = 12
        hook2Offset = 17
        code = (
            b"\x68\xE0\xBC\x8E\x00"            # 0
            b"\xE8\x97\x69\xF6\xFF"            # 5
            b"\x8B\xC8"                        # 10
            b"\xE8\x00\x69\xF6\xFF"            # 12
            b"\x84\xC0"                        # 17
            b"\x0F\x84\xED\x24\x00\x00"        # 19
            b"\xEB"                            # 25
        )
    if offset is False and (self.packetVersion < "20160000" or
                            self.clientType == "iro"):
        # 2015-01-07
        # 0  push ecx
        # 1  push offet packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov bl, al
        # 21 test bl, bl
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        code = (
            b"\x51" +
            b"\x68\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8A\xAB" +
            b"\x84\xAB")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x400)
        if offset is False:
            # 2011-01-04
            # 0  push offet packet_buf
            # 5  call CRagConnection_instanceR
            # 10 mov ecx, eax
            # 12 call CRagConnection_RecvPacket
            # 17 test al, al
            bufOffset = 1
            instanceOffset = 6
            recvOffset = 13
            hook1Offset = 10
            callRecvPacketOffset = 12
            hook2Offset = 17
            code = (
                b"\x68\xAB\xAB\xAB\x00" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x8B\xC8" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x84\xAB")
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
        if offset is False:
            # 2010-01-05
            # 0  lea edx, [ebp+packet_buf]
            # 6  push edx
            # 7  call CRagConnection_instanceR
            # 12 mov ecx, eax
            # 14 call CRagConnection_RecvPacket
            # 19 test al, al
            # 21 jz loc_5E1C3F
            bufOffset = 0
            instanceOffset = 8
            recvOffset = 15
            hook1Offset = 12
            callRecvPacketOffset = 14
            hook2Offset = 19
            code = (
                b"\x8D\x95\xAB\xAB\xAB\xAB"        # 0
                b"\x52"                            # 6
                b"\xE8\xAB\xAB\xAB\xAB"            # 7
                b"\x8B\xC8"                        # 12
                b"\xE8\xAB\xAB\xAB\xAB"            # 14
                b"\x84\xC0"                        # 19
                b"\x0F\x84"                        # 21
            )
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
        if offset is False:
            # 2009-01-13
            # 0  lea edx, [esp+51DCh+packet_buf]
            # 7  push edx
            # 8  call CRagConnection_instanceR
            # 13 mov ecx, eax
            # 15 call CRagConnection_RecvPacket
            # 20 test al, al
            # 22 jz loc_5EB043
            bufOffset = 0
            instanceOffset = 9
            recvOffset = 16
            hook1Offset = 13
            callRecvPacketOffset = 15
            hook2Offset = 20
            code = (
                b"\x8D\x94\x24\xAB\xAB\xAB\x00"    # 0
                b"\x52"                            # 7
                b"\xE8\xAB\xAB\xAB\xAB"            # 8
                b"\x8B\xC8"                        # 13
                b"\xE8\xAB\xAB\xAB\xAB"            # 15
                b"\x84\xC0"                        # 20
                b"\x0F\x84"                        # 22
            )
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
        if offset is False:
            # 2008-01-02
            # 0  lea ecx, [ebp+packet_buf]
            # 6  push ecx
            # 7  call CRagConnection_instanceR
            # 12 mov ecx, eax
            # 14 call CRagConnection_RecvPacket
            # 19 test al, al
            # 21 jz loc_5B77BE
            bufOffset = 0
            instanceOffset = 8
            recvOffset = 15
            hook1Offset = 12
            callRecvPacketOffset = 14
            hook2Offset = 19
            code = (
                b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 0
                b"\x51"                            # 6
                b"\xE8\xAB\xAB\xAB\xAB"            # 7
                b"\x8B\xC8"                        # 12
                b"\xE8\xAB\xAB\xAB\xAB"            # 14
                b"\x84\xC0"                        # 19
                b"\x0F\x84"                        # 21
            )
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
    if offset is False and self.packetVersion < "20050000":
        if self.g_instanceR == 0:
            self.log("Error: g_instanceR not detected")
            exit(1)
        g_instanceRHex = self.exe.toHex(self.g_instanceR, 4)
        if offset is False:
            # 2004-01-07
            # 0  lea edx, [ebp+var_5458]
            # 6  mov ecx, offset g_instanceR
            # 11 push edx
            # 12 call CRagConnection_RecvPacket
            # 17 test al, al
            # 19 jz loc_51D113
            bufOffset = 0
            instanceOffset = 0
            recvOffset = 13
            hook1Offset = 11
            callRecvPacketOffset = 12
            hook2Offset = 17
            code = (
                b"\x8D\x95\xAB\xAB\xAB\xAB"        # 0
                b"\xB9" + g_instanceRHex +         # 6
                b"\x52"                            # 11
                b"\xE8\xAB\xAB\xAB\xAB"            # 12
                b"\x84\xC0"                        # 17
                b"\x0F\x84"                        # 19
            )
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
        if offset is False:
            # 2003-10-28
            # 0  lea ecx, [ebp+var_5458]
            # 6  push ecx
            # 7  mov ecx, offset g_instanceR
            # 12 call CRagConnection_RecvPacket
            # 17 test al, al
            # 19 jz loc_51E625
            bufOffset = 0
            instanceOffset = 0
            recvOffset = 13
            hook1Offset = 7
            callRecvPacketOffset = 12
            hook2Offset = 17
            code = (
                b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 0
                b"\x51"                            # 6
                b"\xB9" + g_instanceRHex +         # 7
                b"\xE8\xAB\xAB\xAB\xAB"            # 12
                b"\x84\xC0"                        # 17
                b"\x0F\x84"                        # 19
            )
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
    if offset is False:
        self.log("failed searchRecvPacket in step 1.")
        exit(1)
    # 2. compare is first call is really CRagConnection::instanceR
    if self.instanceR != 0:
        instanceR = self.getAddr(offset,
                                 instanceOffset,
                                 instanceOffset + 4)
        if instanceR != self.instanceR:
            self.log("Error: failed searchRecvPacket, "
                     "found different instanceR.")
            exit(1)
    self.recvPacket = self.getAddr(offset, recvOffset, recvOffset + 4)
    self.recvPacketVa = self.exe.rawToVa(
        self.getAddr(offset, recvOffset, recvOffset + 4))
    self.addRawFunc("CRagConnection::RecvPacket",
                    self.recvPacket)
    if hook1Offset == 0 or hook2Offset == 0:
        if self.packetVersion >= "20180315":
            self.log("hooks for CRagConnection::RecvPacket not found")
            exit(1)
    else:
        data = self.exe.readData(offset + hook1Offset, 3)
        if data != b"\x8B\xC8\xE8" and self.packetVersion > "20100000":
            print("Error: found wrong data on hook1Offset: "
                  "{0}".format(data.encode("hex")))
            exit(1)
        self.hookRecvPacketStartMapRaw = offset + hook1Offset
        self.hookRecvPacketStartMap = self.exe.rawToVa(offset) + hook1Offset
        self.hookRecvPacketExitMap = self.exe.rawToVa(offset) + hook2Offset
        self.showVaAddr("hookRecvPacketStartMap",
                        self.hookRecvPacketStartMap)
        self.showVaAddr("hookRecvPacketExitMap",
                        self.hookRecvPacketExitMap)
    if bufOffset != 0:
        packetBuf = self.exe.readUInt(offset + bufOffset)
        if self.packetBuf != 0:
            if self.packetBuf != packetBuf:
                self.log("Error: detected wrong packet_buf.")
                exit(1)
        else:
            self.packetBuf = packetBuf
    if callRecvPacketOffset != 0:
        self.callRecvPacket1 = offset + callRecvPacketOffset
    return True

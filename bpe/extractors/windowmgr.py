#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchWindowMgr(self, errorExit):
    # 1. search NUMACCOUNT
    offset, section = self.exe.string(b"NUMACCOUNT")
    if offset is False:
        self.log("failed in search NUMACCOUNT")
        exit(1)
        return
    self.numAccount = section.rawToVa(offset)
    # 2. search UIWindowMgr::MakeWindow
    # 0  mov ecx, g_windowMgr
    # 5  call UIWindowMgr::MakeWindow
    # 10 push 0
    # 12 push 0
    # 14 push "NUMACCOUNT"
    g_windowMgrOffset = 1
    makeWindowOffset = 6
    code = (
        b"\xB9\xAB\xAB\xAB\x00" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x6A\x00" +
        b"\x6A\x00" +
        b"\x68" + self.exe.toHex(self.numAccount, 4))
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov ecx, g_windowMgr
        # 5  call UIWindowMgr::MakeWindow
        # 10 push 0
        # 12 mov edi, eax
        # 14 push 0
        # 16 push "NUMACCOUNT"
        g_windowMgrOffset = 1
        makeWindowOffset = 6
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x6A\x00" +
            b"\xAB\xAB" +
            b"\x6A\x00" +
            b"\x68" + self.exe.toHex(self.numAccount, 4))
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov ecx, g_windowMgr
        # 5  call UIWindowMgr::MakeWindow
        # 10 mov edi, eax
        # 12 push 0
        # 14 push "NUMACCOUNT"
        g_windowMgrOffset = 1
        makeWindowOffset = 6
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\xAB\xAB" +
            b"\x6A\x00" +
            b"\x68" + self.exe.toHex(self.numAccount, 4))
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in search WindowMgr step 2.")
        if errorExit is True:
            exit(1)
        return
    offset1 = offset
    self.gWindowMgr = self.exe.read(offset + g_windowMgrOffset, 4, "V")
    self.UIWindowMgrMakeWindow = self.getAddr(offset,
                                              makeWindowOffset,
                                              makeWindowOffset + 4)
    self.UIWindowMgrMakeWindowVa = self.exe.rawToVa(self.UIWindowMgrMakeWindow)
    self.addVaVar("g_windowMgr", self.gWindowMgr)
    self.addRawFunc("UIWindowMgr::MakeWindow",
                    self.UIWindowMgrMakeWindow)
    # 3. search MsgStr
    # push A
    # push B
    # mov ecx, edi
    # call UIWindow_Create
    # push C
    # call MsgStr
    code = (
        b"\x68\xAB\x00\x00\x00" +
        b"\x68\xAB\x00\x00\x00" +
        b"\x8B\xAB" +
        b"\xE8\xAB\xAB\xAB\xFF" +
        b"\x68\xAB\xAB\x00\x00" +
        b"\xE8\xAB\xAB\xAB\x00")
    windowCreateOffset = 13
    msgStrOffset = 23
    sendMsgOffset = 0
    g_windowMgrOffset = 0
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.UIWindowMgrMakeWindow,
                                   self.UIWindowMgrMakeWindow + 0x10000)
    if offset is False:
        # 2010-09-28
        # 0  push A
        # 5  add edx, B
        # 11 push C
        # 16 mov ecx, ebx
        # 18 mov [ebp+var_20], edx
        # 21 call UIWindow_Create
        # 26 mov eax, [ebp+var_20]
        # 29 push eax
        # 30 call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x81\xAB\xAB\xAB\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\x89\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8B\xAB\xAB" +
            b"\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 22
        msgStrOffset = 31
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2011-01-04
        # push A
        # push B
        # mov ecx, edi
        # call UIWindow_Create
        # lea eax, [ebx + D]
        # push eax
        # call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8D\xAB\xAB\xAB\x00\x00" +
            b"\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 13
        msgStrOffset = 25
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2011-12-07
        # push A
        # push B
        # mov ecx, edi
        # lea ebp, [ebx + D]
        # call UIWindow_Create
        # push ebp
        # call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\x8D\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 19
        msgStrOffset = 25
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2013-01-15
        # push A
        # push B
        # mov ecx, edi
        # call UIWindow_Create
        # mov eax, [ebp + C]
        # add eax, D
        # push eax
        # call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x8B\xAB\xAB" +
            b"\x05\xAB\xAB\x00\x00" +
            b"\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 13
        msgStrOffset = 27
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2015-01-21
        # push A
        # push B
        # mov ecx, edi
        # add esi, C
        # call UIWindow_Create
        # push esi
        # call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\x81\xAB\xAB\xAB\x00\x00" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 19
        msgStrOffset = 25
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2017-09-27
        # push A
        # push B
        # mov ecx, edi
        # call UIWindow_Create
        # push [ebp + C]
        # call MsgStr
        code = (
            b"\x68\xAB\x00\x00\x00" +
            b"\x68\xAB\x00\x00\x00" +
            b"\x8B\xAB" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\xFF\xAB\xAB" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        windowCreateOffset = 13
        msgStrOffset = 21
        sendMsgOffset = 0
        g_windowMgrOffset = 0
    if offset is False:
        # 2009-06-10
        # 0  mov ecx, offset g_windowMgr
        # 5  call UIWindowMgr::SendMsg
        # 10 push 0
        # 12 push 0
        # 14 push 0FFh
        # 19 push 495h
        # 24 call MsgStr
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x6A\x00" +
            b"\x6A\x00" +
            b"\x68\xFF\x00\x00\x00" +
            b"\x68\xAB\x04\x00\x00" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        g_windowMgrOffset = 1
        sendMsgOffset = 6
        msgStrOffset = 25
        windowCreateOffset = 0
    if offset is False:
        # 2009-01-13
        # 0  mov ecx, offset g_windowMgr
        # 5  call UIWindowMgr::SendMsg
        # 10 push ebp
        # 11 push ebp
        # 12 push 0FFh
        # 17 push 495h
        # 22 call MsgStr
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x55" +
            b"\x55" +
            b"\x68\xFF\x00\x00\x00" +
            b"\x68\xAB\x04\x00\x00" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        g_windowMgrOffset = 1
        sendMsgOffset = 6
        msgStrOffset = 23
        windowCreateOffset = 0
    if offset is False:
        # 2007-06-19
        # 0  mov ecx, offset g_windowMgr
        # 5  call UIWindowMgr::SendMsg
        # 10 push 0
        # 12 push 0FFh
        # 17 push 495h
        # 22 call MsgStr
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x6A\x00" +
            b"\x68\xFF\x00\x00\x00" +
            b"\x68\xAB\x04\x00\x00" +
            b"\xE8\xAB\xAB\xAB\x00")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x10000)
        g_windowMgrOffset = 1
        sendMsgOffset = 6
        msgStrOffset = 23
        windowCreateOffset = 0
    if offset is False and self.packetVersion >= "20190213":
        # 0  mov ecx, offset g_windowMgr
        # 5  mov [edi+1E4h], eax
        # 11 call UIWindowMgr_MakeWindow
        # 16 push 1
        # 18 push 10h
        # 20 push 7Ah
        # 22 mov [edi+69C4h], eax
        # 28 call MsgStr
        code = (
            b"\xB9\xAB\xAB\xAB\xAB"            # 0 mov ecx, offset g_windowMgr
            b"\x89\x87\xAB\xAB\xAB\x00"        # 5 mov [edi+1E4h], eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call UIWindowMgr_MakeWindow
            b"\x6A\x01"                        # 16 push 1
            b"\x6A\x10"                        # 18 push 10h
            b"\x6A\x7A"                        # 20 push 7Ah
            b"\x89\x87\xAB\xAB\xAB\x00"        # 22 mov [edi+69C4h], eax
            b"\xE8"                            # 28 call MsgStr
        )
        g_windowMgrOffset = 1
        sendMsgOffset = 0
        msgStrOffset = 29
        windowCreateOffset = 0
        makeWindowOffset = 12
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       offset1,
                                       offset1 + 0x400)

        if offset is not False:
            self.UIWindowMgrMakeWindow = self.getAddr(offset,
                                                      makeWindowOffset,
                                                      makeWindowOffset + 4)
            self.addRawFunc("UIWindowMgr::MakeWindow",
                            self.UIWindowMgrMakeWindow)

    if offset is False and self.packetVersion > "20070619":
        self.log("failed in search MsgStr.")
        if errorExit is True:
            exit(1)
        return

    if offset is False:
        # 2006-02-27
        # 0  push 0
        # 2  push 0
        # 4  push 1
        # 6  push 0
        # 8  push 9
        # 10 call MsgStr
        # 15 add esp, 4
        # 18 mov ecx, offset g_windowMgr
        code = (
            b"\x6A\x00" +
            b"\x6A\x00" +
            b"\x6A\x01" +
            b"\x6A\x00" +
            b"\x6A\x09" +
            b"\xE8\xAB\xAB\xAB\xFF" +
            b"\x83\xAB\xAB" +
            b"\xB9" + self.exe.toHex(self.gWindowMgr, 4))
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.loginPollAddr,
                                       self.loginPollAddr + 0x500)
        msgStrOffset = 11
        sendMsgOffset = 0
        windowCreateOffset = 0
        g_windowMgrOffset = 0

    if g_windowMgrOffset != 0:
        gWindowMgr = self.exe.read(offset + g_windowMgrOffset, 4, "V")
        if self.gWindowMgr != 0 and self.gWindowMgr != gWindowMgr:
            self.log("Error: found different g_windowMgr.")
            if errorExit is True:
                exit(1)
            return

    if windowCreateOffset == 0:
        # here can be addition search for UIWindow::Create
        pass

    if windowCreateOffset != 0:
        self.UIWindowCreate = self.getAddr(offset,
                                           windowCreateOffset,
                                           windowCreateOffset + 4)
        self.addRawFunc("UIWindow::Create", self.UIWindowCreate)
    if sendMsgOffset != 0:
        self.UIWindowMgrSendMsg = self.getAddr(offset,
                                               sendMsgOffset,
                                               sendMsgOffset + 4)
        self.addRawFunc("UIWindowMgr::SendMsg", self.UIWindowMgrSendMsg)
    self.MsgStr = self.getAddr(offset, msgStrOffset, msgStrOffset + 4)
    self.addRawFunc("MsgStr", self.MsgStr)

    # validate MsgStr
    if self.packetVersion > "20040615":
        offset, section = self.exe.string(b"NO MSG")
        if offset is False:
            offset, section = self.exe.string(b"NO MSG : %d")
        if offset is False:
            self.log("failed in search 'NO MSG'")
            exit(1)
            return
        self.NOMSG = section.rawToVa(offset)
        code = (
            self.exe.toHex(self.NOMSG, 4))
        offset = self.exe.code(code,
                               self.MsgStr,
                               self.MsgStr + 0x300)
        if offset is False:
            self.log("failed in MsgStr validation.")
            exit(1)
            return

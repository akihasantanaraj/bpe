#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchVersion(self):
    offset, section = self.exe.string(b"build : %s - %s")
    if offset is False:
        self.log("failed in search 'build : %s - %s'")
        if self.packetVersion < "20120000" or self.clientType == "iro":
            return
        exit(1)
    buildHex = self.exe.toHex(section.rawToVa(offset), 4)

    offset, section = self.exe.string(b"ver : %d.%d.%d")
    if offset is False:
        self.log("failed in search 'ver : %d.%d.%d'")
        exit(1)
    verHex = self.exe.toHex(section.rawToVa(offset), 4)

    offset, section = self.exe.halfString(b"s/n : %s")
    if offset is False:
        offset, section = self.exe.halfString(b"serial : %s")
    if offset is False:
        self.log("failed in search 's/n : %s'")
        exit(1)
    snHex = self.exe.toHex(section.rawToVa(offset), 4)

    gWindowMgrHex = self.exe.toHex(self.gWindowMgr, 4)

    # 0  mov esi, sprintf_s
    # 6  push offset a113155
    # 11 push offset aOct312017
    # 16 push offset aBuildSS
    # 21 lea eax, [ebp+var_52C]
    # 27 push 400h
    # 32 push eax
    # 33 call esi ; sprintf_s
    # 35 add esp, 20h
    # 38 lea eax, [ebp+var_52C]
    # 44 push 0
    # 46 push 0
    # 48 push 0FFFFh
    # 53 push eax
    # 54 push 1
    # 56 mov ecx, offset g_windowMgr
    # 61 call UIWindowMgr_SendMsg
    # 66 push 3
    # 68 push 2
    # 70 push 0Eh
    # 72 push offset aVerD_D_D
    # 77 lea eax, [ebp+var_52C]
    # 83 push 400h
    # 88 push eax
    # 89 call esi ; sprintf_s
    # 91 add esp, 18h
    # 94 lea eax, [ebp+var_52C]
    # 100 push 0
    # 102 push 0
    # 104 push 0FFFFh
    # 109 push eax
    # 110 push 1
    # 112 mov ecx, offset g_windowMgr
    # 117 call UIWindowMgr_SendMsg
    # 122 push offset aT9mx8utS35wzas
    # 127 push offset aSNS
    # 132 lea eax, [ebp+var_52C]
    # 138 push 400h
    # 143 push eax
    # 144 call esi ; sprintf_s
    code = (
        b"\x8B\x35\xAB\xAB\xAB\xAB"        # 0 mov esi, sprintf_s
        b"\x68\xAB\xAB\xAB\xAB"            # 6 push offset a113155
        b"\x68\xAB\xAB\xAB\xAB"            # 11 push offset aOct312017
        b"\x68" + buildHex +               # 16 push offset aBuildSS
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 21 lea eax, [ebp+var_52C]
        b"\x68\x00\x04\x00\x00"            # 27 push 400h
        b"\x50"                            # 32 push eax
        b"\xFF\xD6"                        # 33 call esi ; sprintf_s
        b"\x83\xC4\x20"                    # 35 add esp, 20h
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 38 lea eax, [ebp+var_52C]
        b"\x6A\x00"                        # 44 push 0
        b"\x6A\x00"                        # 46 push 0
        b"\x68\xFF\xFF\x00\x00"            # 48 push 0FFFFh
        b"\x50"                            # 53 push eax
        b"\x6A\x01"                        # 54 push 1
        b"\xB9" + gWindowMgrHex +          # 56 mov ecx, offset g_windowMgr
        b"\xE8\xAB\xAB\xAB\xAB"            # 61 call UIWindowMgr_SendMsg
        b"\x6A\xAB"                        # 66 push 3
        b"\x6A\xAB"                        # 68 push 2
        b"\x6A\xAB"                        # 70 push 0Eh
        b"\x68" + verHex +                 # 72 push offset aVerD_D_D
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 77 lea eax, [ebp+var_52C]
        b"\x68\x00\x04\x00\x00"            # 83 push 400h
        b"\x50"                            # 88 push eax
        b"\xFF\xD6"                        # 89 call esi ; sprintf_s
        b"\x83\xC4\x18"                    # 91 add esp, 18h
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 94 lea eax, [ebp+var_52C]
        b"\x6A\x00"                        # 100 push 0
        b"\x6A\x00"                        # 102 push 0
        b"\x68\xFF\xFF\x00\x00"            # 104 push 0FFFFh
        b"\x50"                            # 109 push eax
        b"\x6A\x01"                        # 110 push 1
        b"\xB9" + gWindowMgrHex +          # 112 mov ecx, offset g_windowMgr
        b"\xE8\xAB\xAB\xAB\xAB"            # 117 call UIWindowMgr_SendMsg
        b"\x68\xAB\xAB\xAB\xAB"            # 122 push offset aT9mx8utS35wzas
        b"\x68" + snHex +                  # 127 push offset aSNS
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 132 lea eax, [ebp+var_52C]
        b"\x68\x00\x04\x00\x00"            # 138 push 400h
        b"\x50"                            # 143 push eax
        b"\xFF\xD6"                        # 144 call esi ; sprintf_s
    )
    sprintfOffset = (2, True)
    timeOffset = 7
    dateOffset = 12
    verOffset1 = 71
    verOffset2 = 69
    verOffset3 = 67
    serialOffset = 123
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push offset a093151
        # 5  push offset aFeb182019
        # 10 push offset aBuildSS
        # 15 lea eax, [ebp+Dst]
        # 21 push 400h
        # 26 push eax
        # 27 call sprintf_s
        # 32 add esp, 20h
        # 35 lea eax, [ebp+Dst]
        # 41 mov ecx, offset g_windowMgr
        # 46 push 0
        # 48 push 0
        # 50 push 0FFFFh
        # 55 push eax
        # 56 push 1
        # 58 call UIWindowMgr_SendMsg
        # 63 push 3
        # 65 push 2
        # 67 push 0Eh
        # 69 push offset aVerD_D_D
        # 74 lea eax, [ebp+Dst]
        # 80 push 400h
        # 85 push eax
        # 86 call sprintf_s
        # 91 add esp, 18h
        # 94 lea eax, [ebp+Dst]
        # 100 mov ecx, offset g_windowMgr
        # 105 push 0
        # 107 push 0
        # 109 push 0FFFFh
        # 114 push eax
        # 115 push 1
        # 117 call UIWindowMgr_SendMsg
        # 122 push offset aXxxxxxxXxxxxxx
        # 127 push offset aSNS
        # 132 lea eax, [ebp+Dst]
        # 138 push 400h
        # 143 push eax
        # 144 call sprintf_s
        # 149 add esp, 10h
        code = (
            b"\x68\xAB\xAB\xAB\xAB"            # 0 push offset a093151
            b"\x68\xAB\xAB\xAB\xAB"            # 5 push offset aFeb182019
            b"\x68" + buildHex +               # 10 push offset aBuildSS
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 15 lea eax, [ebp+Dst]
            b"\x68\x00\x04\x00\x00"            # 21 push 400h
            b"\x50"                            # 26 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 27 call sprintf_s
            b"\x83\xC4\x20"                    # 32 add esp, 20h
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 35 lea eax, [ebp+Dst]
            b"\xB9" + gWindowMgrHex +          # 41 mov ecx, offset g_windowMgr
            b"\x6A\x00"                        # 46 push 0
            b"\x6A\x00"                        # 48 push 0
            b"\x68\xFF\xFF\x00\x00"            # 50 push 0FFFFh
            b"\x50"                            # 55 push eax
            b"\x6A\x01"                        # 56 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 58 call UIWindowMgr_SendMsg
            b"\x6A\xAB"                        # 63 push 3
            b"\x6A\xAB"                        # 65 push 2
            b"\x6A\xAB"                        # 67 push 0Eh
            b"\x68" + verHex +                 # 69 push offset aVerD_D_D
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 74 lea eax, [ebp+Dst]
            b"\x68\x00\x04\x00\x00"            # 80 push 400h
            b"\x50"                            # 85 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 86 call sprintf_s
            b"\x83\xC4\x18"                    # 91 add esp, 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 94 lea eax, [ebp+Dst]
            b"\xB9" + gWindowMgrHex +          # 100 mov ecx, offset g_windowMg
            b"\x6A\x00"                        # 105 push 0
            b"\x6A\x00"                        # 107 push 0
            b"\x68\xFF\xFF\x00\x00"            # 109 push 0FFFFh
            b"\x50"                            # 114 push eax
            b"\x6A\x01"                        # 115 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 117 call UIWindowMgr_SendMsg
            b"\x68\xAB\xAB\xAB\xAB"            # 122 push offset aXxxxxxxXxxxxx
            b"\x68" + snHex +                  # 127 push offset aSNS
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 132 lea eax, [ebp+Dst]
            b"\x68\x00\x04\x00\x00"            # 138 push 400h
            b"\x50"                            # 143 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 144 call sprintf_s
            b"\x83\xC4\x10"                    # 149 add esp, 10h
        )
        sprintfOffset = (28, False)
        timeOffset = 1
        dateOffset = 6
        verOffset1 = 68
        verOffset2 = 66
        verOffset3 = 64
        serialOffset = 123
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov esi, sprintf_s
        # 6  push offset a102046
        # 11 push offset aJan62015
        # 16 push offset aBuildSS
        # 21 lea ecx, [ebp+var_624]
        # 27 push 400h
        # 32 push ecx
        # 33 call esi
        # 35 add esp, 20h
        # 38 push 0
        # 40 push 0
        # 42 push 0FFFFh
        # 47 lea edx, [ebp+var_624]
        # 53 push edx
        # 54 push 1
        # 56 mov ecx, offset g_windowMgr
        # 61 call UIWindowMgr_sub_5DE4B0
        # 66 push 3
        # 68 push 2
        # 70 push 0Eh
        # 72 push offset aVerD_D_D
        # 77 lea eax, [ebp+var_624]
        # 83 push 400h
        # 88 push eax
        # 89 call esi
        # 91 add esp, 18h
        # 94 push 0
        # 96 push 0
        # 98 push 0FFFFh
        # 103 lea ecx, [ebp+var_624]
        # 109 push ecx
        # 110 push 1
        # 112 mov ecx, offset g_windowMgr
        # 117 call UIWindowMgr_sub_5DE4B0
        # 122 push offset a1jyx8ut5cgwzas
        # 127 push offset aSNS
        # 132 lea edx, [ebp+var_624]
        # 138 push 400h
        # 143 push edx
        # 144 call esi
        # 146 add esp, 10h
        code = (
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 0 mov esi, sprintf_s
            b"\x68\xAB\xAB\xAB\xAB"            # 6 push offset a102046
            b"\x68\xAB\xAB\xAB\xAB"            # 11 push offset aJan62015
            b"\x68" + buildHex +               # 16 push offset aBuildSS
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 21 lea ecx, [ebp+var_624]
            b"\x68\x00\x04\x00\x00"            # 27 push 400h
            b"\x51"                            # 32 push ecx
            b"\xFF\xD6"                        # 33 call esi
            b"\x83\xC4\x20"                    # 35 add esp, 20h
            b"\x6A\x00"                        # 38 push 0
            b"\x6A\x00"                        # 40 push 0
            b"\x68\xFF\xFF\x00\x00"            # 42 push 0FFFFh
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 47 lea edx, [ebp+var_624]
            b"\x52"                            # 53 push edx
            b"\x6A\x01"                        # 54 push 1
            b"\xB9" + gWindowMgrHex +          # 56 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 61 call UIWindowMgr_SendMsg
            b"\x6A\xAB"                        # 66 push 3
            b"\x6A\xAB"                        # 68 push 2
            b"\x6A\xAB"                        # 70 push 0Eh
            b"\x68" + verHex +                 # 72 push offset aVerD_D_D
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 77 lea eax, [ebp+var_624]
            b"\x68\x00\x04\x00\x00"            # 83 push 400h
            b"\x50"                            # 88 push eax
            b"\xFF\xD6"                        # 89 call esi
            b"\x83\xC4\x18"                    # 91 add esp, 18h
            b"\x6A\x00"                        # 94 push 0
            b"\x6A\x00"                        # 96 push 0
            b"\x68\xFF\xFF\x00\x00"            # 98 push 0FFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 103 lea ecx, [ebp+var_624]
            b"\x51"                            # 109 push ecx
            b"\x6A\x01"                        # 110 push 1
            b"\xB9" + gWindowMgrHex +          # 112 mov ecx, offset g_windowMg
            b"\xE8\xAB\xAB\xAB\xAB"            # 117 call UIWindowMgr_SendMsg
            b"\x68\xAB\xAB\xAB\xAB"            # 122 push offset a1jyx8ut5cgwza
            b"\x68" + snHex +                  # 127 push offset aSNS
            b"\x8D\x95\xAB\xAB\xAB\xAB"        # 132 lea edx, [ebp+var_624]
            b"\x68\x00\x04\x00\x00"            # 138 push 400h
            b"\x52"                            # 143 push edx
            b"\xFF\xD6"                        # 144 call esi
            b"\x83\xC4\x10"                    # 146 add esp, 10h
        )
        sprintfOffset = (2, True)
        timeOffset = 7
        dateOffset = 12
        verOffset1 = 71
        verOffset2 = 69
        verOffset3 = 67
        serialOffset = 123
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov esi, sprintf_s
        # 6  push offset a093416
        # 11 push offset aJan32013
        # 16 push offset aBuildSS
        # 21 lea ecx, [esp+6E0h+var_410]
        # 28 push 400h
        # 33 push ecx
        # 34 call esi ; sprintf_s
        # 36 add esp, 20h
        # 39 push 0
        # 41 push 0
        # 43 push 0FFFFh
        # 48 lea edx, [esp+6D4h+var_410]
        # 55 push edx
        # 56 push 1
        # 58 mov ecx, offset g_windowMgr
        # 63 call UIWindowMgr_SendMsg
        # 68 push 9
        # 70 push 2
        # 72 push 0Eh
        # 74 push offset aVerD_D_D
        # 79 lea eax, [esp+6D8h+var_410]
        # 86 push 400h
        # 91 push eax
        # 92 call esi ; sprintf_s
        # 94 add esp, 18h
        # 97 push 0
        # 99 push 0
        # 101 push 0FFFFh
        # 106 lea ecx, [esp+6D4h+var_410]
        # 113 push ecx
        # 114 push 1
        # 116 mov ecx, offset g_windowMgr
        # 121 call UIWindowMgr_SendMsg
        # 126 push offset aHvax8utK5cwzas
        # 131 push offset aSNS
        # 136 lea edx, [esp+6D0h+var_410]
        # 143 push 400h
        # 148 push edx
        # 149 call esi ; sprintf_s
        # 151 add esp, 10h
        code = (
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 0 mov esi, sprintf_s
            b"\x68\xAB\xAB\xAB\xAB"            # 6 push offset a093416
            b"\x68\xAB\xAB\xAB\xAB"            # 11 push offset aJan32013
            b"\x68" + buildHex +               # 16 push offset aBuildSS
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"    # 21 lea ecx, [esp+6E0h+var_410]
            b"\x68\x00\x04\x00\x00"            # 28 push 400h
            b"\x51"                            # 33 push ecx
            b"\xFF\xD6"                        # 34 call esi ; sprintf_s
            b"\x83\xC4\x20"                    # 36 add esp, 20h
            b"\x6A\x00"                        # 39 push 0
            b"\x6A\x00"                        # 41 push 0
            b"\x68\xFF\xFF\x00\x00"            # 43 push 0FFFFh
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 48 lea edx, [esp+6D4h+var_410]
            b"\x52"                            # 55 push edx
            b"\x6A\x01"                        # 56 push 1
            b"\xB9" + gWindowMgrHex +          # 58 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 63 call UIWindowMgr_SendMsg
            b"\x6A\xAB"                        # 68 push 9
            b"\x6A\xAB"                        # 70 push 2
            b"\x6A\xAB"                        # 72 push 0Eh
            b"\x68" + verHex +                 # 74 push offset aVerD_D_D
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 79 lea eax, [esp+6D8h+var_410]
            b"\x68\x00\x04\x00\x00"            # 86 push 400h
            b"\x50"                            # 91 push eax
            b"\xFF\xD6"                        # 92 call esi ; sprintf_s
            b"\x83\xC4\x18"                    # 94 add esp, 18h
            b"\x6A\x00"                        # 97 push 0
            b"\x6A\x00"                        # 99 push 0
            b"\x68\xFF\xFF\x00\x00"            # 101 push 0FFFFh
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"    # 106 lea ecx, [esp+6D4h+var_410
            b"\x51"                            # 113 push ecx
            b"\x6A\x01"                        # 114 push 1
            b"\xB9" + gWindowMgrHex +          # 116 mov ecx, offset g_windowMg
            b"\xE8\xAB\xAB\xAB\xAB"            # 121 call UIWindowMgr_SendMsg
            b"\x68\xAB\xAB\xAB\xAB"            # 126 push offset aHvax8utK5cwza
            b"\x68" + snHex +                  # 131 push offset aSNS
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 136 lea edx, [esp+6D0h+var_410
            b"\x68\x00\x04\x00\x00"            # 143 push 400h
            b"\x52"                            # 148 push edx
            b"\xFF\xD6"                        # 149 call esi ; sprintf_s
            b"\x83\xC4\x10"                    # 151 add esp, 10h
        )
        sprintfOffset = (2, True)
        timeOffset = 7
        dateOffset = 12
        verOffset1 = 73
        verOffset2 = 71
        verOffset3 = 69
        serialOffset = 127
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov esi, ds:sprintf_s
        # 6  push offset a131121
        # 11 push offset aFeb292012
        # 16 push offset aBuildSS
        # 21 lea eax, [esp+6D4h+DstBuf]
        # 28 push 400h
        # 33 push eax
        # 34 call esi
        # 36 add esp, 20h
        # 39 push 0
        # 41 push 0
        # 43 push 0FFFFh
        # 48 lea ecx, [esp+6C8h+DstBuf]
        # 55 push ecx
        # 56 push 1
        # 58 mov ecx, offset g_windowMgr
        # 63 call UIWindowMgr_SendMsg
        # 68 push 1
        # 70 push 2
        # 72 push 0Eh
        # 74 push offset aVerD_D_D
        # 79 lea edx, [esp+6CCh+DstBuf]
        # 86 push 400h
        # 91 push edx
        # 92 call esi
        # 94 add esp, 18h
        # 97 push 0
        # 99 push 0
        # 101 push 0FFFFh
        # 106 lea eax, [esp+6C8h+DstBuf]
        # 113 push eax
        # 114 push 1
        # 116 mov ecx, offset g_windowMgr
        # 121 call UIWindowMgr_SendMsg
        # 126 push offset String
        # 131 push offset aSNS
        # 136 lea ecx, [esp+6C4h+DstBuf]
        # 143 push 400h
        # 148 push ecx
        # 149 call esi
        # 151 add esp, 10h
        code = (
            b"\x8B\x35\xAB\xAB\xAB\xAB"        # 0 mov esi, ds:sprintf_s
            b"\x68\xAB\xAB\xAB\xAB"            # 6 push offset a131121
            b"\x68\xAB\xAB\xAB\xAB"            # 11 push offset aFeb292012
            b"\x68" + buildHex +               # 16 push offset aBuildSS
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 21 lea eax, [esp+6D4h+DstBuf]
            b"\x68\x00\x04\x00\x00"            # 28 push 400h
            b"\x50"                            # 33 push eax
            b"\xFF\xD6"                        # 34 call esi
            b"\x83\xC4\x20"                    # 36 add esp, 20h
            b"\x6A\x00"                        # 39 push 0
            b"\x6A\x00"                        # 41 push 0
            b"\x68\xFF\xFF\x00\x00"            # 43 push 0FFFFh
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"    # 48 lea ecx, [esp+6C8h+DstBuf]
            b"\x51"                            # 55 push ecx
            b"\x6A\x01"                        # 56 push 1
            b"\xB9" + gWindowMgrHex +          # 58 mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 63 call UIWindowMgr_SendMsg
            b"\x6A\xAB"                        # 68 push 1
            b"\x6A\xAB"                        # 70 push 2
            b"\x6A\xAB"                        # 72 push 0Eh
            b"\x68" + verHex +                 # 74 push offset aVerD_D_D
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 79 lea edx, [esp+6CCh+DstBuf]
            b"\x68\x00\x04\x00\x00"            # 86 push 400h
            b"\x52"                            # 91 push edx
            b"\xFF\xD6"                        # 92 call esi
            b"\x83\xC4\x18"                    # 94 add esp, 18h
            b"\x6A\x00"                        # 97 push 0
            b"\x6A\x00"                        # 99 push 0
            b"\x68\xFF\xFF\x00\x00"            # 101 push 0FFFFh
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 106 lea eax, [esp+6C8h+DstBuf]
            b"\x50"                            # 113 push eax
            b"\x6A\x01"                        # 114 push 1
            b"\xB9" + gWindowMgrHex +          # 116 mov ecx, offset g_windowMg
            b"\xE8\xAB\xAB\xAB\xAB"            # 121 call UIWindowMgr_SendMsg
            b"\x68\xAB\xAB\xAB\xAB"            # 126 push offset String
            b"\x68" + snHex +                  # 131 push offset aSNS
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"    # 136 lea ecx, [esp+6C4h+DstBuf]
            b"\x68\x00\x04\x00\x00"            # 143 push 400h
            b"\x51"                            # 148 push ecx
            b"\xFF\xD6"                        # 149 call esi
            b"\x83\xC4\x10"                    # 151 add esp, 10h
        )
        sprintfOffset = (2, True)
        timeOffset = 7
        dateOffset = 12
        verOffset1 = 73
        verOffset2 = 71
        verOffset3 = 69
        serialOffset = 127
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search version.")
        exit(1)

    if sprintfOffset != 0:
        if sprintfOffset[1] is True:
            self.sprintf_s = self.exe.readUInt(offset + sprintfOffset[0])
            self.addVaFunc("sprintf_s", self.sprintf_s)
        else:
            self.sprintf_s = self.getAddr(offset,
                                          sprintfOffset[0],
                                          sprintfOffset[0] + 4)
            self.addRawFunc("sprintf_s", self.sprintf_s)

    self.buildDate = self.exe.readUInt(offset + dateOffset)
    self.addVaVarStr("buildDate", self.buildDate)
    self.buildTime = self.exe.readUInt(offset + timeOffset)
    self.addVaVarStr("buildTime", self.buildTime)
    self.version1 = self.exe.readUByte(offset + verOffset1)
    self.version2 = self.exe.readUByte(offset + verOffset2)
    self.version3 = self.exe.readUByte(offset + verOffset3)
    self.serialNumber = self.exe.readUInt(offset + serialOffset)
    self.addVaVarStr("serialNumber", self.serialNumber)
    self.log("version: {0}.{1}.{2}".format(self.version1,
                                           self.version2,
                                           self.version3))

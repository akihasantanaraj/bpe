#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchMaxChars(self, errorExit):
    # 1. search CHARACTER_INFO Cnt Over %d / %d
    offset, section = self.exe.string(b"CHARACTER_INFO Cnt Over %d / %d")
    if offset is False:
        self.log("failed in search CHARACTER_INFO Cnt Over %d / %d")
        if errorExit is True:
            exit(1)
        return
    self.characterInfoCntOver = section.rawToVa(offset)
    # 2. search for string usage
    # 2017-11-01
    # 0  call memset
    # 5  push MaxChars
    # 7  push edi
    # 8  push "CHARACTER_INFO Cnt Over %d / %d"
    # 13 push 0FFFFFFFFh
    code = (
        b"\xE8\xAB\xAB\xAB\x00" +
        b"\x6A\xAB" +
        b"\xAB" +
        b"\x68" + self.exe.toHex(self.characterInfoCntOver, 4) +
        b"\x6A\xFF")
    offset = self.exe.codeWildcard(code, b"\xAB")
    sizeOffset = 6
    if offset is False:
        # 2013-01-15
        # 0  call memset
        # 5  mov eax, [ebp+addr]
        # 11 push MaxChars
        # 13 push edi
        # 14 push "CHARACTER_INFO Cnt Over %d / %d"
        # 19 push 0FFFFFFFFh
        code = (
            b"\xE8\xAB\xAB\xAB\x00" +
            b"\x8B\xAB\xAB\xAB\xAB\xFF" +
            b"\x6A\xAB" +
            b"\xAB" +
            b"\x68" + self.exe.toHex(self.characterInfoCntOver, 4) +
            b"\x6A\xFF")
        offset = self.exe.codeWildcard(code, b"\xAB")
        sizeOffset = 12
    if offset is False:
        self.log("failed in search MAX_CHARS")
        if errorExit is True:
            exit(1)
        return
    self.maxChars = self.exe.readUByte(offset + sizeOffset)
    self.showVaAddr("MAX_CHARS", self.maxChars)

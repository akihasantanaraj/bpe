#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchModeMgr(self):
    if self.getGameModeAddrVa == 0:
        self.log("Error: skip search g_modeMgr")
        exit(1)
    # 0  mov ecx, offset g_modeMgr
    # 5  call CModeMgr::GetGameMode
    # 10 mov ecx, eax
    code = (
        b"\xB9\xAB\xAB\xAB\xAB" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8")
    offsets = self.exe.codesWildcard(code, b"\xAB", -1)
    if offsets is False:
        self.log("Error: failed in seach g_modeMgr")
        exit(1)
        return
    g_modeMgr = 0
    errorCount = 0
    for offset in offsets:
        getGameMode = self.getAddr(offset, 6, 10)
        if getGameMode == self.getGameModeAddr:
            g_modeMgr = self.exe.readUInt(offset + 1)
            if self.g_modeMgr != 0:
                if g_modeMgr != self.g_modeMgr:
                    errorCount = errorCount + 1
            else:
                self.g_modeMgr = g_modeMgr
    if g_modeMgr == 0:
        self.log("Error: cant find g_modeMgr reference")
        exit(1)
    # some number of errors can happend because too small search block
    if errorCount > 2:
        self.log("Error: found different g_modeMgr")
        exit(1)
    self.addVaVar("g_modeMgr", self.g_modeMgr)

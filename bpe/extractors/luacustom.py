#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchLuaCustom(self, errorExit):
    offset, _ = self.exe.string(b"Lua 5.1")
    if offset is False:
        if errorExit is True and self.packetVersion >= "20120110":
            exit(1)
        return

    # search lua_getfield + lua_pcall
    offset, section = self.exe.string(b"main")
    if offset is False:
        self.log("Error: string 'main' not found")
        exit(1)
    mainStr = section.rawToVa(offset)
    # 0  push offset aMain
    # 5  push 0FFFFD8EEh
    # 10 push [ebp+var_18.L]
    # 13 call lua_getfield
    # 18 push 0
    # 20 push 2
    # 22 push 0
    # 24 push [ebp+var_18.L]
    # 27 call lua_pcall
    # 32 add esp, 1Ch
    code = (
        b"\x68" + self.exe.toHex(mainStr, 4) +  # 0
        b"\x68\xAB\xAB\xAB\xAB"            # 5
        b"\xFF\x75\xAB"                    # 10
        b"\xE8\xAB\xAB\xAB\xAB"            # 13
        b"\x6A\x00"                        # 18
        b"\x6A\x02"                        # 20
        b"\x6A\x00"                        # 22
        b"\xFF\x75\xAB"                    # 24
        b"\xE8\xAB\xAB\xAB\xAB"            # 27
        b"\x83\xC4\x1C"                    # 32
    )
    lua_getfieldOffset = 14
    lua_pcallOffset = 28
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov eax, [ebp+var_14]
        # 3  push offset aMain
        # 8  push 0FFFFD8EEh
        # 13 push eax
        # 14 call lua_getfield
        # 19 mov ecx, [ebp+var_14]
        # 22 push 0
        # 24 push 2
        # 26 push 0
        # 28 push ecx
        # 29 call lua_pcall
        # 34 add esp, 1Ch
        code = (
            b"\x8B\x45\xAB"                    # 0
            b"\x68" + self.exe.toHex(mainStr, 4) +  # 3
            b"\x68\xAB\xAB\xAB\xAB"            # 8
            b"\x50"                            # 13
            b"\xE8\xAB\xAB\xAB\xAB"            # 14
            b"\x8B\x4D\xAB"                    # 19
            b"\x6A\x00"                        # 22
            b"\x6A\x02"                        # 24
            b"\x6A\x00"                        # 26
            b"\x51"                            # 28
            b"\xE8\xAB\xAB\xAB\xAB"            # 29
            b"\x83\xC4\x1C"                    # 34
        )
        lua_getfieldOffset = 15
        lua_pcallOffset = 30
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov edx, [ebp+var_6C]
        # 3  push offset aMain
        # 8  push 0FFFFD8EEh
        # 13 push edx
        # 14 call lua_getfield
        # 19 mov eax, [ebp+var_6C]
        # 22 push 0
        # 24 push 2
        # 26 push 0
        # 28 push eax
        # 29 call lua_pcall
        # 34 add esp, 1Ch
        code = (
            b"\x8B\x55\x94"                    # 0
            b"\x68" + self.exe.toHex(mainStr, 4) +  # 3
            b"\x68\xAB\xAB\xAB\xAB"            # 8
            b"\x52"                            # 13
            b"\xE8\xAB\xAB\xAB\xAB"            # 14
            b"\x8B\x45\xAB"                    # 19
            b"\x6A\x00"                        # 22
            b"\x6A\x02"                        # 24
            b"\x6A\x00"                        # 26
            b"\x50"                            # 28
            b"\xE8\xAB\xAB\xAB\xAB"            # 29
            b"\x83\xC4\x1C"                    # 34
        )
        lua_getfieldOffset = 15
        lua_pcallOffset = 30
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov edx, [esp+78h+var_6C]
        # 4  push offset aMain
        # 9  push 0FFFFD8EEh
        # 14 push edx
        # 15 call lua_getfield
        # 20 mov eax, [esp+84h+var_6C]
        # 24 push 0
        # 26 push 2
        # 28 push 0
        # 30 push eax
        # 31 call lua_pcall
        # 36 add esp, 1Ch
        code = (
            b"\x8B\x54\x24\xAB"                # 0
            b"\x68" + self.exe.toHex(mainStr, 4) +  # 4
            b"\x68\xAB\xAB\xAB\xAB"            # 9
            b"\x52"                            # 14
            b"\xE8\xAB\xAB\xAB\xAB"            # 15
            b"\x8B\x44\x24\xAB"                # 20
            b"\x6A\x00"                        # 24
            b"\x6A\x02"                        # 26
            b"\x6A\x00"                        # 28
            b"\x50"                            # 30
            b"\xE8\xAB\xAB\xAB\xAB"            # 31
            b"\x83\xC4\x1C"                    # 36
        )
        lua_getfieldOffset = 16
        lua_pcallOffset = 32
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  mov ecx, [esp+78h+var_6C]
        # 4  push offset aMain
        # 9  push 0FFFFD8EEh
        # 14 push ecx
        # 15 call lua_getfield
        # 20 mov edx, [esp+84h+var_6C]
        # 24 push 0
        # 26 push 2
        # 28 push 0
        # 30 push edx
        # 31 call lua_pcall
        # 36 add esp, 1Ch
        code = (
            b"\x8B\x4C\x24\xAB"                # 0
            b"\x68" + self.exe.toHex(mainStr, 4) +  # 4
            b"\x68\xAB\xAB\xAB\xAB"            # 9
            b"\x51"                            # 14
            b"\xE8\xAB\xAB\xAB\xAB"            # 15
            b"\x8B\x54\x24\xAB"                # 20
            b"\x6A\x00"                        # 24
            b"\x6A\x02"                        # 26
            b"\x6A\x00"                        # 28
            b"\x52"                            # 30
            b"\xE8\xAB\xAB\xAB\xAB"            # 31
            b"\x83\xC4\x1C"                    # 36
        )
        lua_getfieldOffset = 16
        lua_pcallOffset = 32
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("Error: lua_getfield + lua_pcall call not found")
        if self.packetVersion >= "20120328" and \
           self.clientType not in ("mro", "ruro", "bro", "tro"):
            exit(1)
        return
    # need for quick convert raw to va
    codeSection = self.exe.codeSection
    lua_getfield = self.getAddr(offset,
                                lua_getfieldOffset,
                                lua_getfieldOffset + 4) - \
        codeSection.rawVaDiff
    if self.lua_getfield == 0:
        self.lua_getfield = lua_getfield
        self.addVaFuncType("lua_getfield",
                           self.lua_getfield,
                           "void __cdecl lua_getfield(lua_State *L, int idx, "
                           "const char *k)")
    elif self.lua_getfield != lua_getfield:
        self.log("Error: found different lua_getfield")
        exit(1)
    self.lua_pcall = self.getAddr(offset,
                                  lua_pcallOffset,
                                  lua_pcallOffset + 4) - \
        codeSection.rawVaDiff
    self.addVaFuncType("lua_pcall",
                       self.lua_pcall,
                       "int __cdecl lua_pcall(lua_State *L, int nargs, "
                       "int nresults, int errfunc)")

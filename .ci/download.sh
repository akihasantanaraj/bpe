#!/usr/bin/env bash

mkdir clients || true
cd clients || exit 1
wget http://nemo.herc.ws/${DOWNLOADER} || exit 1
python2 ./${DOWNLOADER} ${CLIENT} || exit 1
cd ..
